class UrlMappings {

    static mappings = {

        "/qrcode/text"(controller: "qrcode", action: "text")
        "/my_account"(controller: "users", action: "my_account")
        "/my_account/edit"(controller: "users", action: "edit_me")

        "/themes"(controller: "theme", action: "list")
        "/themes/list"(controller: "theme", action: "list")
        "/themes/delete/$t_id"(controller: "theme", action: "delete")

        "/users"(controller: "users", action: "list")
        "/users/add"(controller: "users", action: "add")
        //        "/users/test"(controller: "users", action: "test")
        "/users/do_add"(controller: "users", action: "do_add")
        "/users/$user_id/edit"(controller: "users", action: "edit")
        "/users/$user_id/do_edit"(controller: "users", action: "do_edit")
        "/users/$user_id/enable"(controller: "users", action: "enable")
        "/users/$user_id/disable"(controller: "users", action: "disable")
        "/users/$user_id/delete"(controller: "users", action: "delete")

        "/parcours"(controller: "parcours", action: "list")
        "/parcours/add"(controller: "parcours", action: "add")
        "/parcours/do_add"(controller: "parcours", action: "do_add")

        //POIS

        "/parcours/$p_id/enable"(controller: "parcours", action: "enable")
        "/parcours/$p_id/disable"(controller: "parcours", action: "disable")

        "/parcours/$p_id/edit"(controller: "parcours", action: "edit")
        "/parcours/$p_id/delete"(controller: "parcours", action: "delete")
        "/parcours/$p_id/do_info_edit"(controller: "parcours", action: "do_info_edit")
        "/parcours/$p_id/pois"(controller: "pois", action: "list")
        "/parcours/$p_id/pois/add"(controller: "pois", action: "add")
        "/parcours/$p_id/pois/do_add"(controller: "pois", action: "do_add")
        "/parcours/$p_id/pois/$poi_id/edit"(controller: "pois", action: "edit")
        "/parcours/$p_id/pois/$poi_id/do_edit"(controller: "pois", action: "do_edit")
        "/parcours/$p_id/pois/$poi_id/move_to/$index"(controller: "pois", action: "move")
        "/parcours/$p_id/pois/$poi_id/delete"(controller: "pois", action: "delete")

        //        "/parcours/$p_id/comments" ( controller: "comments", action:"list" )

        "/parcours/$p_id/medias"(controller: "medias", action: "list")
        "/parcours/$p_id/medias/get_json_img_list"(controller: "medias", action: "get_json_img_list")
        "/parcours/$p_id/medias/upload"(controller: "medias", action: "upload")

        "/parcours/$p_id/moderators"(controller: "moderators", action: "list")
        "/parcours/$p_id/moderators/add"(controller: "moderators", action: "add")
        "/parcours/$p_id/moderators/do_add/$user_id"(controller: "moderators", action: "do_add")
        "/parcours/$p_id/moderators/do_remove/$user_id"(controller: "moderators", action: "do_remove")

        "/parcours/$p_id/sections"(controller: "sections", action: "list")
        "/parcours/$p_id/sections/do_add"(controller: "sections", action: "do_add")
        "/parcours/$p_id/sections/add"(controller: "sections", action: "add")
        "/parcours/$p_id/sections/$s_id/delete"(controller: "sections", action: "delete")
        "/parcours/$p_id/sections/$s_id/edit"(controller: "sections", action: "edit")
        "/parcours/$p_id/sections/$s_id/move_to/$index"(controller: "sections", action: "move")
        "/parcours/$p_id/sections/$s_id/do_info_edit"(controller: "sections", action: "do_info_edit")

        "/api/parcours/$p_id/pois/$poi_id"(controller: "pois", action: "api_get")
        "/api/parcours/$p_id"(controller: "parcours", action: "api_get")

        "/api/parcours/*-$p_id" {
            controller = "parcours"
            action = "api_get_from_title"
            constraints {
                p_id(matches: /\d+/)
            }
        }

        //            Ajouts 23/03/2017
        "/api/parcours/get_all"
                {
                    controller = "parcours"
                    action = "api_get_all"
                }

        "/api/parcours/$p_id/get_all_pois"
                {
                    controller = "parcours"
                    action = "api_get_all_pois"
                    constraints {
                        p_id(matches: /\d+/)
                    }
                }
        "/api/pois/get_all"
                {
                    controller = "pois"
                    action = "api_get_all"
                }
        "/api/pois/$poi_id"
                {
                    controller = "pois"
                    action = "api_get_generic"
                    constraints {
                        poi_id(matches: /\d+/)
                    }
                }
        "/api/pois/lifi/$lifi_id"
                {
                    controller = "pois"
                    action = "api_get_lifi_generic"
                }

        //            Ajouts 23/03/2017

//        Ajouts 02/08/2018

        "/api/themes/get_all"
                {
                    controller = "parcours"
                    action = "api_get_all_themes"
                }
        "/api/parcours/theme/$t_id"
                {
                    controller = "parcours"
                    action = "api_get_parcours_by_theme"
                }
        "/api/parcours/get_all_by_themes"
                {
                    controller = "parcours"
                    action = "api_get_all_parcours_by_theme"
                }
//        Ajouts 02/08/2018

        //        "/auth"(controller:"Login",action:"auth_fail")
        "/auth"(controller: "Login", action: "auth")

        "/"(controller: "parcours", action: "list")
        "500"(view: '/error')
        "404"(view: '/page_not_found')
        "403"(view: '/page_forbidden')
    }
}

