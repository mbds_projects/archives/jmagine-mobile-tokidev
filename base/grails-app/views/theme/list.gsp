<%@ page import="fr.mbds.tokidev.jmagine.User" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="backend"/>
    <title></title>
</head>

<body>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><g:message code="jmagine.themes.list"/></h1>

    <div class="row theme_list">
        <div class="col-xs-12 col-sm-12">
            <g:each in="${themes}" var="theme">
                <div class="theme" style="margin-bottom: 4px">
                    <div class="body" style="margin-bottom: 5px">
                        <div class="name" data-theme-id="${theme.id}">
                            ${ theme.name +" ( "+theme.parcours.size()+" )"}

                            <g:if test="${theme.parcours.size() == 0}">
                                <g:link controller="theme" action="delete" params="${[t_id:theme.id]}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <g:message code="jmagine.button.delete"/></g:link>
                            </g:if>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>
    </div>
</div>

</body>
</html>