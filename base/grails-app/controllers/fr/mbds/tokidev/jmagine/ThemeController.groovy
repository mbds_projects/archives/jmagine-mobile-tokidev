package fr.mbds.tokidev.jmagine

import grails.plugin.springsecurity.SpringSecurityService

class ThemeController {
    SpringSecurityService springSecurityService

    def list() {
        User me = springSecurityService.getCurrentUser()
        render(view: '/theme/list', model: [me: me, themes: Theme.list()])
    }

    def delete(Long t_id)
    {
        def themeInstance = Theme.get(t_id)
        if (themeInstance)
        {
            if (themeInstance.parcours.size() == 0)
                themeInstance.delete(flush: true)
        }
        redirect (action: 'list')
    }
}

