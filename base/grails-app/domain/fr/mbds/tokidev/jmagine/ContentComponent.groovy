/*
 * Copyright (c) 2016.
 * Tokidev S.A.S.
 */

package fr.mbds.tokidev.jmagine

class ContentComponent {

    String              title
    FileContainer       backgroundPic
    String              content

    Date                dateCreated
    Date                lastUpdated

    static belongsTo = [Parcours]

    static constraints =
            {
                title           nullable: false
                backgroundPic   nullable: false
                content         blank: true, nullable: true
            }

    static mapping =
            {
                content type: 'text'
            }
}
