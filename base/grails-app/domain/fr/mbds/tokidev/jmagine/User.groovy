/*
 * Copyright (c) 2016.
 * Tokidev S.A.S.
 */

package fr.mbds.tokidev.jmagine

class User {

	transient springSecurityService

	String username
	String password
	String mail
	Thumbnail thumbnail
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
	Date	dateCreated
	Date 	lastUpdated

	static transients = ['springSecurityService']
	static mappedBy = [ moderatedParcours: 'moderators']
	static hasMany = [ moderatedParcours:Parcours ]

	static constraints = {
		username 	blank: false, unique: true
		password 	blank: false
		mail     	blank: false, email: true
		thumbnail	nullable: true
	}

	static mapping = {
		password 	column: '`password`'
		thumbnail 	cascade: 'all-delete-orphan'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}
}
