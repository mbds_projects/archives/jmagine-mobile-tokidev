package fr.mbds.tokidev.jmagine

class Theme {

    String name

    static hasMany = [parcours: Parcours]

    static belongsTo = Parcours

    static constraints = {
        name unique: true, nullable: false, blank: false
    }
}
