/*
 * Copyright (c) 2016.
 * Tokidev S.A.S.
 */

package fr.mbds.tokidev.jmagine

/**
 * A POI is a step in a "Parcours", contains many informations about the spot itself, as well as many "components"
 * for mobile display handling
 * We should never store an "address", but correlate the lat / lng with reverse geocoding
 * Future improvement : allow the "linking" between many POIs, a same place could be part of many "Parcours" and we
 * should offer the users the possibility to switch from one "Parcours" to another at these meeting points
 */
class POI
{
    String              title
    FileContainer       backgroundPic
    Double              lat
    Double              lng

    Date                dateCreated
    Date                lastUpdated
    String              address
    String              content

    Boolean             isNFCEnabled = Boolean.FALSE
    Boolean             isQREnabled = Boolean.FALSE
    Boolean             isSNSEnabled = Boolean.FALSE
    Boolean             isGeolocEnabled = Boolean.FALSE
    Boolean             isLifiEnabled = Boolean.FALSE

    String              targetUrl
    String              lifiId

    static hasMany = [ comments:Comment ]

    static belongsTo = [ parcours:Parcours ]

    static constraints =
    {
        title           blank: false
        backgroundPic   nullable: true
        lat             nullable: false
        lng             nullable: false
        content         nullable: true, blank: true
        isNFCEnabled    nullable: false
        isQREnabled     nullable: false
        isSNSEnabled    nullable: false
        isGeolocEnabled nullable: false
        isLifiEnabled   nullable: false
        targetUrl       nullable: true, blank: true
        lifiId          nullable: true, blank: true
    }

    static mapping = {
        address type: 'text'
        content type: 'text'
    }
}
