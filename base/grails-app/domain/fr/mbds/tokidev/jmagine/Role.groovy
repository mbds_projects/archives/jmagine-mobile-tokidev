/*
 * Copyright (c) 2016.
 * Tokidev S.A.S.
 */

package fr.mbds.tokidev.jmagine

class Role {

	String 		authority
	Integer		level

	static mapping = {
		cache true
	}

	static constraints = {
		authority 	blank: false, unique: true
		level		unique: true
	}
}
