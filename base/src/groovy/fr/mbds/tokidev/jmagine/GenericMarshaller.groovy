package fr.mbds.tokidev.jmagine
/**
 * Created with IntelliJ IDEA.
 * User: Lionel
 * Date: 18/08/14
 * Time: 18:48
 * To change this template use File | Settings | File Templates.
 */
import grails.converters.JSON
import grails.converters.XML
import grails.util.Holders
import org.codehaus.groovy.grails.web.converters.marshaller.NameAwareMarshaller
import org.codehaus.groovy.grails.web.converters.marshaller.ObjectMarshaller
import org.hibernate.collection.internal.PersistentList

class GenericMarshaller implements ObjectMarshaller<XML>, NameAwareMarshaller {

    @Override
    boolean supports(Object object) {
        if( object instanceof Date )
            return false
        else if( object instanceof ArrayList )
            return false
        else if( object instanceof PersistentList )
            return false
        else return true
    }

    @Override
    String getElementName(Object object) {
        if( object instanceof ContentComponent )
            return "component"
        else if( object instanceof Parcours )
            return 'parcours'
        else if( object instanceof POI )
            return 'poi'
        else if( object instanceof Theme )
            return 'theme'
        else if( object instanceof ThemeAndParcours )
            return 'theme'
        else return object.class.name
    }

    @Override
    void marshalObject(Object object, XML converter) {
        if( object instanceof Parcours ) {
            converter.build {
                id( object.id )
                title( object.title )
                backgroundPic( object.backgroundPic?Holders.config.grails.serverURL + Holders.config.grails.assetspath.relative_images + object.backgroundPic.filename:'' )
                components( object.components )
                if( object.pois.size() ) {
                    first_poi( object.pois[0].id )
                }
                else first_poi( '' )
            }
        }

        else if( object instanceof ContentComponent ) {
            converter.build {
                title( object.title )
                backgroundPic( object.backgroundPic?(Holders.config.grails.serverURL+Holders.config.grails.assetspath.relative_images+object.backgroundPic.filename):'')
                content( "<![CDATA["+object.content+"]]>" )
            }
        }

        else if( object instanceof POI ) {
            converter.build {
                id (object.id)
                title( object.title )
                backgroundPic( object.backgroundPic?Holders.config.grails.serverURL + Holders.config.grails.assetspath.relative_images + object.backgroundPic.filename:'' )
                lat( object.lat )
                lng( object.lng )
                address( object.address )
                if( object.content ) content( "<![CDATA["+object.content+"]]>" )
                else content('')
                def index = object.parcours.pois.indexOf( object )
                isNFCEnabled(object.isNFCEnabled)
                isQREnabled(object.isQREnabled)
                isSNSEnabled(object.isSNSEnabled)
                isGeolocEnabled(object.isGeolocEnabled)
                idLifiEnabled(object.isLifiEnabled)
                lifiId(object.lifiId)
                targetUrl(object.targetUrl)


                if( (index != -1) && ( (index+1) < object.parcours.pois.size() ) )
                    next_poi( object.parcours.pois[index+1].id )
                else
                    next_poi( object.parcours.pois[0].id )
            }
        }

        else if (object instanceof Theme)
        {
            converter.build {
                id (object.id)
                name (object.name)
            }
        }

        else if (object instanceof ThemeAndParcours)
        {
            converter.build {
                id (object.theme.id)
                name (object.theme.name)
                parcours (object.parcours)
            }
        }
    }
}