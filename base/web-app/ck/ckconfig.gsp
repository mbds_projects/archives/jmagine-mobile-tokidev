<%@ page contentType="text/javascript"%>

// Add a reference to the new plugin
CKEDITOR.plugins.addExternal('youtube', '${request.contextPath}/ck/plugins/youtube/');
CKEDITOR.plugins.addExternal('slideshow', '${request.contextPath}/ck/plugins/SlideShow/');
CKEDITOR.plugins.addExternal('imagebrowser', '${request.contextPath}/ck/plugins/imagebrowser/');

CKEDITOR.editorConfig = function( config )
{
    // Declare the additional plugin
    config.extraPlugins = 'youtube,slideshow,imagebrowser'; // Additional plugin

    config.resize_enabled = false;
    config.enterMode = CKEDITOR.ENTER_BR;
    config.allowedContent = true;

    // EXAMPLE TOOLBAR USING THE NEW PLUGIN
    config.toolbar_custom = [[
        'Source','Save','NewPage','DocProps','-',
        'Templates','-','Cut','Copy','Paste','-',
        'Image','Table','HorizontalRule','SpecialChar','-',
        'Maximize','-',
        'Bold','Italic','Underline','Strike','Subscript','Superscript','-',
        'NumberedList','BulletedList','-',
        'Undo','Redo','-','FontSize','Styles','Format','Font','-',
        'TextColor','BGColor','-',
        'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-',
        'ShowBlocks','-',
        'Youtube','Slideshow','-',
        'Link', 'Unlink']
    ]

};