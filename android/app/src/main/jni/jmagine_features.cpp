/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_features.cpp - Header features detection module. Uses features descriptors to match
                         against known triggers.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine_features.hpp"
#include "jmagine_features_filtering.hpp"
#include "jmagine_utils.hpp"
#include "jmagine_parameters.hpp"
#ifdef JMAGINE_TESTING
#include "jmagine_data_triggers_testing.hpp"
#else
#include "jmagine_data_triggers.hpp"
#endif
#ifdef TKDV_DEBUG
#include "jmagine_debug.hpp"
#endif
#include "jmagine_order32.hpp"




// Initializes a features library handle. Returns 0 on success, something else on failure.
int jmagine_features_init(jmagine_features_handle_t& handle, const char* base_path, int db_id)
{
  int rv;

#ifdef JMAGINE_SURF
  initModule_nonfree();
#endif

  if(!jmagine_features_are_algorithms_supported(JMAGINE_ALGORITHMS_TO_USE, JMAGINE_NB_ALGORITHMS_TO_USE))
    return(10);
  if(!JMAGINE_FEATURES_CROSSCHECK_FILTERING)   // Not supported right now.
    return(11);

  if((db_id<0) || (db_id>=JMAGINE_NB_DATABASES))
    return(12);

  // Create the features detectors/descriptors extractors.
  if((rv=jmagine_features_create_detectors_extractors(handle))!=0)
    return(rv);

  // Create the matchers.
  if((rv=jmagine_features_create_matchers(handle))!=0)
    return(rv);

  if(jmagine_utils_check_supported_len_datatypes())
  {
    LOGE("Unsupported data types lengths ! On the current architecture, the lengths are: %lu/%lu/%lu/%lu/%lu.", sizeof(char), sizeof(int),
         sizeof(short), sizeof(double), sizeof(float));
    return(12);
  }
  if(jmagine_utils_check_supported_endianness())
  {
    LOGE("Unsupported endian-ness ! On the current architecture, the hex key has a value of: %08X.", SNS_O32_HOST_ORDER);
    return(13);
  }
#ifdef TKDV_DEBUG
  if(jmagine_utils_is_big_endian())
  {
    LOGD("Architecture is big-endian.");
  }
  else
  {
    LOGD("Architecture is little-endian.");
  }
#endif

  // Load the triggers.
  if((rv=jmagine_features_load_triggers(handle, base_path, db_id))!=0)
    return(rv);

#ifdef TKDV_DEBUG
  jmagine_debug_output_triggers(handle);
#endif

  return(0);
}




// Entry point for the features analysis. This will call the jmagine_perform_features_detection function for the specified query for each
// supported features detection algorithm. Those will return a list of likely matches. Then, this function will apply processing in
// order to present the results to the calling layer as if there was only one algorithm being used - in other words, it will combine
// the results of the various features detection algorithms.
// This is a hack due to the recent addition (post branch prototype) of the multi-detector support; it will eventually need to be
// replaced by something much cleaner than this.
// Returns 0 on success, an error code on failure.
int jmagine_features_detection(jmagine_features_handle_t& hnd, const Mat& query, vector<jmagine_candidate_match_t>& matches)
{
  int i, rv;

  for(i=0; i<JMAGINE_NB_ALGORITHMS_TO_USE; ++i)
  {
    if(hnd.triggers[i].size()==0)
    {
#ifdef TKDV_DEBUG
      LOGD("Algorithm 0x%02X has no trigger, ignoring.", JMAGINE_ALGORITHMS_TO_USE[i]);
#endif
      return(0);
    }
    vector<jmagine_candidate_match_t> algorithm_matches;
#ifdef TKDV_DEBUG
    LOGD("Performing analysis for algorithm 0x%02X...", JMAGINE_ALGORITHMS_TO_USE[i]);
    double t=(double)getTickCount();
#endif
    if((rv=jmagine_perform_features_detection(hnd, query, algorithm_matches, i))!=0)
      return(rv);
#ifdef TKDV_DEBUG
    t=((double)getTickCount() - t)/getTickFrequency();
    LOGD("Performed analysis for algorithm 0x%02X successfully in %.04f seconds. %lu candidates:", JMAGINE_ALGORITHMS_TO_USE[i], t, algorithm_matches.size());
    for(unsigned int j=0; j<algorithm_matches.size(); ++j)
    {
      LOGD("  #%d: trigger '%s'/element '%d'. Score: %.02f, matches: %d.", j+1, hnd.triggers[i][algorithm_matches[j].trigger_id].name.c_str(),
           algorithm_matches[j].element_id, algorithm_matches[j].score, algorithm_matches[j].nb_matches);
    }
#endif
    if(i==0)   // #### MULTI DETECTOR IMPLEM: TEMPORARY FOR REGRESSION TESTING
      matches=algorithm_matches;
  }

  return(0);
}




// The main features analysis function. This takes the specified image, generate its keypoints and descriptors for the specified algorithm,
// matches it against the library of known model, and then apply a large amount of filtering/other matching
// operations according to the algorithms params. Finally, it sorts through the filtered matches and attributes
// a score to each of those probable matches, putting them into the specified vector.
// If max_matches is >0, then no more than max_matches will be returned (the max_matches best results).
// Returns 0 on success, something else on failure.
int jmagine_perform_features_detection(jmagine_features_handle_t& hnd, const Mat& query, vector<jmagine_candidate_match_t>& matches, int algorithm_id)
{
  int rv;
  size_t i;
  Mat query_img;
  vector<KeyPoint> query_features;
  Mat query_descriptors;
  vector<vector<DMatch> > knnmatches_triggers;
  vector<vector<DMatch> > knnmatches_q2m, knnmatches_q2m_ratio;
  vector<vector<DMatch> > knnmatches_m2q, knnmatches_m2q_ratio;
  vector<jmagine_candidate_match_t> matches_all_triggers;
  vector<vector<DMatch> > filtered_matches;
  vector<vector<DMatch> > matches_per_trigger;
  jmagine_trigger_t* trigger;
  double dist_min, dist_max, dist_min_all, dist_max_all;
  vector<DMatch> top_matches;
  Mat homography;
  double min=0, max=0;
  int rows, cols;
  int nb_steps=0;
  int fast_thresh=JMAGINE_FEATURES_FAST_THRESHOLD;
  double surf_thresh=JMAGINE_FEATURES_SURF_HTHRESHOLD;
  Ptr<FeatureDetector> dynamic_detector;

  if(JMAGINE_FEATURES_NORMALIZE_QUERY)
    jmagine_utils_normalize_img(query, query_img);
  else
    query_img=query.clone();
  if(JMAGINE_FEATURES_BLUR_QUERY)
  {
    GaussianBlur(query_img, query_img, cv::Size(JMAGINE_FEATURES_BLUR_QUERY_KERNEL, JMAGINE_FEATURES_BLUR_QUERY_KERNEL), 0,
                 0, BORDER_DEFAULT);
  }

  // We now have a normalized query image, let's detect its features and extract its descriptors.
  hnd.detectors[algorithm_id]->detect(query_img, query_features);
  if(query_features.size()==0)   // No features found.
    return(0);
  if((JMAGINE_FEATURES_DYNAMIC_DETECTION[algorithm_id]) && (JMAGINE_ALGORITHMS_TO_USE[algorithm_id]==JMAGINE_ALGORITHM_FAST_FREAK))
  {
    if(query_features.size()>JMAGINE_FEATURES_DETECTION_MAX_FEATURES[algorithm_id])
    {
      nb_steps=0;
      while(query_features.size()>JMAGINE_FEATURES_DETECTION_MAX_FEATURES[algorithm_id])
      {
        fast_thresh+=JMAGINE_FEATURES_DETECTION_STEP_THRESH_FAST;
        if((fast_thresh<1) || (fast_thresh>60))
          break;
        dynamic_detector=new FastFeatureDetector(fast_thresh, true);
        query_features.clear();
        dynamic_detector->detect(query_img, query_features);
        dynamic_detector.release();
        ++nb_steps;
        if(nb_steps>=JMAGINE_FEATURES_DETECTION_MAX_STEPS_FAST)
          break;
      }
    }
    else if(query_features.size()<JMAGINE_FEATURES_DETECTION_MIN_FEATURES[algorithm_id])
    {
      nb_steps=0;
      while(query_features.size()<JMAGINE_FEATURES_DETECTION_MAX_FEATURES[algorithm_id])
      {
        fast_thresh-=JMAGINE_FEATURES_DETECTION_STEP_THRESH_FAST;
        if((fast_thresh<1) || (fast_thresh>60))
          break;
        dynamic_detector=new FastFeatureDetector(fast_thresh, true);
        query_features.clear();
        dynamic_detector->detect(query_img, query_features);
        dynamic_detector.release();
        ++nb_steps;
        if(nb_steps>=JMAGINE_FEATURES_DETECTION_MAX_STEPS_FAST)
          break;
      }
    }
    if(query_features.size()==0)   // No features found.
      return(0);
  }
  else if((JMAGINE_FEATURES_DYNAMIC_DETECTION[algorithm_id]) && (JMAGINE_ALGORITHMS_TO_USE[algorithm_id]==JMAGINE_ALGORITHM_SURF_SURF))
  {
    if(query_features.size()>JMAGINE_FEATURES_DETECTION_MAX_FEATURES[algorithm_id])
    {
      nb_steps=0;
      while(query_features.size()>JMAGINE_FEATURES_DETECTION_MAX_FEATURES[algorithm_id])
      {
        surf_thresh+=JMAGINE_FEATURES_DETECTION_STEP_THRESH_SURF;
        if((surf_thresh<10.) || (surf_thresh>1000.))
          break;
        dynamic_detector=new SURF(surf_thresh, JMAGINE_FEATURES_SURF_NB_OCTAVES, JMAGINE_FEATURES_SURF_NB_OCTAVE_LAYERS,
                                   JMAGINE_FEATURES_SURF_EXTENDED, JMAGINE_FEATURES_SURF_UPRIGHT);
        query_features.clear();
        dynamic_detector->detect(query_img, query_features);
        dynamic_detector.release();
        ++nb_steps;
        if(nb_steps>=JMAGINE_FEATURES_DETECTION_MAX_STEPS_SURF)
          break;
      }
    }
    else if(query_features.size()<JMAGINE_FEATURES_DETECTION_MIN_FEATURES[algorithm_id])
    {
      nb_steps=0;
      while(query_features.size()<JMAGINE_FEATURES_DETECTION_MAX_FEATURES[algorithm_id])
      {
        surf_thresh-=JMAGINE_FEATURES_DETECTION_STEP_THRESH_SURF;
        if((surf_thresh<1.) || (surf_thresh>1000.))
          break;
        dynamic_detector=new SURF(surf_thresh, JMAGINE_FEATURES_SURF_NB_OCTAVES, JMAGINE_FEATURES_SURF_NB_OCTAVE_LAYERS,
                                   JMAGINE_FEATURES_SURF_EXTENDED, JMAGINE_FEATURES_SURF_UPRIGHT);
        query_features.clear();
        dynamic_detector->detect(query_img, query_features);
        dynamic_detector.release();
        ++nb_steps;
        if(nb_steps>=JMAGINE_FEATURES_DETECTION_MAX_STEPS_SURF)
          break;
      }
    }
    if(query_features.size()==0)   // No features found.
      return(0);
  }
#ifdef TKDV_DEBUG
  LOGD("Algorithm 0x%02X - got %lu features.", JMAGINE_ALGORITHMS_TO_USE[algorithm_id], query_features.size());
#endif

  hnd.extractors[algorithm_id]->compute(query_img, query_features, query_descriptors);

  // Now, we try to match the query descriptors to our known library of descriptors through the flann
  // matcher. We take n matches, to make sure too-general descriptors in the library won't prevent the detection
  // of a proper match.
  if(JMAGINE_FEATURES_MATCHING_USE_FLANN[algorithm_id])
  {
    hnd.matchers[algorithm_id]->knnMatch(query_descriptors, knnmatches_triggers, JMAGINE_FEATURES_MATCHING_KNN_TRIGGERS[algorithm_id]);
  }
  else
  {
    hnd.matchers_bruteforce[algorithm_id]->knnMatch(query_descriptors, knnmatches_triggers, JMAGINE_FEATURES_MATCHING_KNN_TRIGGERS[algorithm_id]);
  }
  if(knnmatches_triggers.size()==0)   // No matches.
    return(0);
  // We filter those to obtain the n most likely match candidates.

  if(JMAGINE_FEATURES_MATCHING_MODE_SCORE[algorithm_id]==1)
  {
    rv=jmagine_features_filter_top_candidates_knn_score(hnd, knnmatches_triggers, matches_all_triggers,
                                                        JMAGINE_FEATURES_FLANN_FILTERING_NB_BEST[algorithm_id], algorithm_id);
  }
  else
  {
    rv=jmagine_features_filter_top_candidates_knn_occurences(hnd, knnmatches_triggers, matches_all_triggers,
                                                             JMAGINE_FEATURES_FLANN_FILTERING_NB_BEST[algorithm_id], algorithm_id);
  }
  if(rv)   // Error occured.
    return(rv);
  if(matches_all_triggers.size()==0)  // No matches.
    return(0);
  knnmatches_triggers.clear();

#ifdef TKDV_DEBUG
  string model_name;
  short element_id;
  LOGD("  - Features analysis, algorithm 0x%02X, best candidates, first step (general flann index):", JMAGINE_ALGORITHMS_TO_USE[algorithm_id]);
  for(i=0; i<matches_all_triggers.size(); ++i)
  {
    model_name=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].name;
    element_id=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].element_id;
    LOGD("    #%lu for model '%s'/element correspondance id '%d' with %d matches, score %.02f.", i+1, model_name.c_str(), element_id,
         matches_all_triggers[i].nb_matches, matches_all_triggers[i].score);
  }
#endif

  filtered_matches.resize(matches_all_triggers.size());
  // Now, we'll first perform a crosscheck bruteforce match for each of the candidates.
  // This will allow to get a much better analysis for each of those potential trigger matches.
#ifdef TKDV_DEBUG
  LOGD("  - Features analysis, algorithm 0x%02X, best candidates, crosscheck-filtered:", JMAGINE_ALGORITHMS_TO_USE[algorithm_id]);
#endif
  for(i=0; i<matches_all_triggers.size(); ++i)
  {
    trigger=&(hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id]);
    // query2model
    hnd.matchers_crosscheck[algorithm_id]->knnMatch(query_descriptors, trigger->descriptors, knnmatches_q2m,
                                                    JMAGINE_FEATURES_MATCHING_KNN_CROSSCHECK);
    // model2query
    hnd.matchers_crosscheck[algorithm_id]->knnMatch(trigger->descriptors, query_descriptors, knnmatches_m2q,
                                                    JMAGINE_FEATURES_MATCHING_KNN_CROSSCHECK);

    // If enabled: perform Lowe's ratio filtering before doing crosscheck filtering.
    // This helps remove uncertain points.
    if(JMAGINE_FEATURES_RATIO_FILTERING[algorithm_id])
    {
#ifdef TKDV_DEBUG
      size_t nb_q2m=knnmatches_q2m.size();
      size_t nb_m2q=knnmatches_m2q.size();
      model_name=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].name;
      element_id=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].element_id;
#endif
      filter_matches_ratio_knn(knnmatches_q2m, knnmatches_q2m_ratio, JMAGINE_FEATURES_RATIO[algorithm_id]);
      filter_matches_ratio_knn(knnmatches_m2q, knnmatches_m2q_ratio, JMAGINE_FEATURES_RATIO[algorithm_id]);
      knnmatches_q2m.swap(knnmatches_q2m_ratio);
      knnmatches_m2q.swap(knnmatches_m2q_ratio);
      knnmatches_q2m_ratio.clear();
      knnmatches_m2q_ratio.clear();
#ifdef TKDV_DEBUG
      LOGD("    Number #%lu - '%s'/'%d' went through ratio filtering. Before: %lu q2m, %lu m2q. After: %lu q2m, %lu m2q.", i+1,
           model_name.c_str(), element_id, nb_q2m, nb_m2q, knnmatches_q2m.size(), knnmatches_m2q.size());
#endif
    }
    // Let's apply crosscheck filtering.
    rv=jmagine_features_filter_crosscheck(knnmatches_q2m, knnmatches_m2q, filtered_matches[i]);
    if(rv)   // Error occured.
      return(rv);
    knnmatches_q2m.clear();
    knnmatches_m2q.clear();
    if((rv=jmagine_features_update_candidate(matches_all_triggers[i], filtered_matches[i], algorithm_id))!=0)
      return(rv);
#ifdef TKDV_DEBUG
    model_name=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].name;
    element_id=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].element_id;
    LOGD("    Number #%lu - model '%s'/'%d': %d crosscheck-filtered matches, score %.02f.", i+1, model_name.c_str(), element_id,
         matches_all_triggers[i].nb_matches, matches_all_triggers[i].score);
#endif
  }
  matches_per_trigger.swap(filtered_matches);
  filtered_matches.clear();


  // If simple or adjusted threshold filtering is enabled, we'll possibly need the min/max values of all candidates
  // for below.
  if((JMAGINE_FEATURES_STHRESHOLD_FILTERING[algorithm_id]) || (JMAGINE_FEATURES_ATHRESHOLD_FILTERING[algorithm_id]))
  {
    if((JMAGINE_FEATURES_STHRESHOLD_LOCAL[algorithm_id]) && (JMAGINE_FEATURES_ATHRESHOLD_LOCAL[algorithm_id]))
    {
      dist_min_all=-1;
      dist_max_all=-1;
    }
    else
    {
      min=4242424;
      max=-4242424;
      for(i=0; i<matches_per_trigger.size(); ++i)
      {
        if(matches_per_trigger[i].size()>0)
        {
          rv=jmagine_utils_get_min_max_dist_matches(matches_per_trigger[i], &dist_min, &dist_max);
          if(rv)
            return(rv);
          if(dist_min<min)
            min=dist_min;
          if(dist_max>max)
            max=dist_max;
        }
      }
      if(min==4242424)
      {
        dist_min_all=-1;
        dist_max_all=-1;
      }
      else
      {
        dist_min_all=min;
        dist_max_all=max;
      }
    }
  }

  // If enabled, we now perform a simple threshold filtering.
  if(JMAGINE_FEATURES_STHRESHOLD_FILTERING[algorithm_id])
  {
    if(JMAGINE_FEATURES_STHRESHOLD_LOCAL[algorithm_id])   // Compare with the min/max distance for *this* candidate only.
    {
      dist_min=-1;
      dist_max=-1;
    }
    else   // Compare with the min/max distance of all candidates.
    {
      dist_min=dist_min_all;
      dist_max=dist_max_all;
    }
    filtered_matches.resize(matches_per_trigger.size());
#ifdef TKDV_DEBUG
    LOGD("  - Features analysis, algorithm 0x%02X, best candidates, simple threshold-filtered:", JMAGINE_ALGORITHMS_TO_USE[algorithm_id]);
#endif
    for(i=0; i<matches_per_trigger.size(); ++i)
    {
#ifdef TKDV_DEBUG
      size_t nb_before=matches_per_trigger[i].size();
#endif
      rv=jmagine_features_filter_simple_threshold(matches_per_trigger[i], filtered_matches[i],
                                                  JMAGINE_FEATURES_STHRESHOLD[algorithm_id], dist_min);
      if(rv)
        return(rv);
      if((rv=jmagine_features_update_candidate(matches_all_triggers[i], filtered_matches[i], algorithm_id))!=0)
        return(rv);
#ifdef TKDV_DEBUG
      model_name=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].name;
      element_id=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].element_id;
      LOGD("    Number #%lu - model '%s'/'%d': %d s-threshold-filtered matches (%lu before), score %.02f.", i+1,
           model_name.c_str(), element_id, matches_all_triggers[i].nb_matches, nb_before, matches_all_triggers[i].score);
#endif
    }
    matches_per_trigger.swap(filtered_matches);
    filtered_matches.clear();
  }

  // If enabled, we now perform an adjusted threshold filtering.
  if(JMAGINE_FEATURES_ATHRESHOLD_FILTERING[algorithm_id])
  {
    if(JMAGINE_FEATURES_ATHRESHOLD_LOCAL[algorithm_id])   // Compare with the min/max distance for *this* candidate only.
    {
      dist_min=-1;
      dist_max=-1;
    }
    else   // Compare with the min/max distance of all candidates.
    {
      dist_min=dist_min_all;
      dist_max=dist_max_all;
    }
    filtered_matches.resize(matches_per_trigger.size());
#ifdef TKDV_DEBUG
    LOGD("  - Features analysis, algorithm 0x%02X, best candidates, adjusted threshold-filtered:", JMAGINE_ALGORITHMS_TO_USE[algorithm_id]);
#endif
    for(i=0; i<matches_per_trigger.size(); ++i)
    {
#ifdef TKDV_DEBUG
      size_t nb_before=matches_per_trigger[i].size();
#endif
      rv=jmagine_features_filter_adjusted_threshold(matches_per_trigger[i], filtered_matches[i],
                                                    JMAGINE_FEATURES_ATHRESHOLD[algorithm_id], dist_min, dist_max);
      if(rv)
        return(rv);
      if((rv=jmagine_features_update_candidate(matches_all_triggers[i], filtered_matches[i], algorithm_id))!=0)
        return(rv);
#ifdef TKDV_DEBUG
      model_name=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].name;
      element_id=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].element_id;
      LOGD("    Number #%lu - model '%s'/'%d': %d a-threshold-filtered matches (%lu before), score %.02f.", i+1,
           model_name.c_str(), element_id, matches_all_triggers[i].nb_matches, nb_before, matches_all_triggers[i].score);
#endif
    }
    matches_per_trigger.swap(filtered_matches);
    filtered_matches.clear();
  }

  // If enabled, we now perform filtering through the use of RANSAC/LMEDS by trying to find a coherent homography for the
  // match. This can take a while if there are a lot of filtered matches. Thus, it is advised to set a positive value
  // to JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_NBMAX, in which case we'll use the top n matches.
  if(JMAGINE_FEATURES_HOMOGRAPHY_FILTERING[algorithm_id])
  {
    filtered_matches.resize(matches_per_trigger.size());
#ifdef TKDV_DEBUG
    LOGD("  - Features analysis, best candidates, homography-filtered:");
#endif
    for(i=0; i<matches_per_trigger.size(); ++i)
    {
#ifdef TKDV_DEBUG
      size_t nb_before=matches_per_trigger[i].size();
#endif
      if(matches_per_trigger[i].size()<JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_NBMIN[algorithm_id])
      {
#ifdef TKDV_DEBUG
        model_name=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].name;
        element_id=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].element_id;
        LOGD("    Number #%lu - model '%s'/'%d': %d matches, no filtering applied (not enough matches).", i+1,
             model_name.c_str(), element_id, matches_all_triggers[i].nb_matches);
#endif
        filtered_matches[i]=matches_per_trigger[i];
        continue;
      }
      homography.release();
      // Too many matches: take the top n.
      if((JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_NBMAX[algorithm_id]>0) &&
         (matches_per_trigger[i].size()>JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_NBMAX[algorithm_id]))
      {
        top_matches.clear();
        rv=jmagine_utils_get_top_matches(matches_per_trigger[i], top_matches, JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_NBMAX[algorithm_id]);
        if(rv)
          return(rv);
        homography.release();
        rv=jmagine_features_filter_homography(top_matches, query_features,
                                              hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].keypoints,
                                              JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_METHOD[algorithm_id], filtered_matches[i],
                                              homography);
      }
      else
      {
        rv=jmagine_features_filter_homography(matches_per_trigger[i], query_features,
                                              hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].keypoints,
                                              JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_METHOD[algorithm_id], filtered_matches[i],
                                              homography);
      }
      if(rv)
        return(rv);

      // We now have the homography-filtered matches for this candidate.
      // If enabled, we'll get some information about a possible shape of the model in the image.
      if(JMAGINE_FEATURES_HOMOGRAPHY_STORE_TOPOLOGY)
      {
        cols=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].len_x;
        rows=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].len_y;
        rv=jmagine_features_get_topology_homography(matches_all_triggers[i], homography, cols, rows,
                                                    JMAGINE_FEATURES_HOMOGRAPHY_APPROX_TOPOLOGY);
        if(rv)
          return(rv);
      }

      if((rv=jmagine_features_update_candidate(matches_all_triggers[i], filtered_matches[i], algorithm_id))!=0)
        return(rv);
#ifdef TKDV_DEBUG
      model_name=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].name;
      element_id=hnd.triggers[algorithm_id][matches_all_triggers[i].trigger_id].element_id;
      LOGD("    Number #%lu - model '%s'/'%d': %d homography-filtered matches (%lu before), score %.02f.", i+1,
           model_name.c_str(), element_id, matches_all_triggers[i].nb_matches, nb_before, matches_all_triggers[i].score);
#endif
    }
    matches_per_trigger.swap(filtered_matches);
    filtered_matches.clear();
  }

  // We've know applied a large amount of filtering to the matches of our n best candidates.
  // For each of them, we have various information. We fill the final matches vector with
  // all candidates who survived the filtering steps.
  for(i=0; i<matches_all_triggers.size(); ++i)
  {
    if(matches_per_trigger[i].size()==0)   // No matches passed the test, skip this candidate.
      continue;
    matches.push_back(matches_all_triggers[i]);
  }

  return(0);
}




// Creates the features detector/descriptors extractor according to the params in the specified handle.
// Returns 0 on success, something else on failure.
int jmagine_features_create_detectors_extractors(jmagine_features_handle_t& hnd)
{
  int i, j, algorithm_id;

  hnd.detectors.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  hnd.extractors.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  for(i=0; i<JMAGINE_NB_ALGORITHMS_TO_USE; ++i)
  {
    algorithm_id=JMAGINE_ALGORITHMS_TO_USE[i];

    if(algorithm_id==JMAGINE_ALGORITHM_FAST_FREAK)
    {
      hnd.detectors[i]=new FastFeatureDetector(JMAGINE_FEATURES_FAST_THRESHOLD, true);
      if(hnd.detectors[i]==NULL)
      {
        for(j=0; j<i; ++i)
        {
          hnd.detectors[j].release();
          hnd.extractors[j].release();
        }
        return(1);
      }
      hnd.extractors[i]=new FREAK(JMAGINE_FEATURES_FREAK_NORM_ORIENTATION, JMAGINE_FEATURES_FREAK_NORM_SCALE, JMAGINE_FEATURES_FREAK_PSCALE,
                                  JMAGINE_FEATURES_FREAK_N_OCTAVES);
      if(hnd.extractors[i]==NULL)
      {
        for(j=0; j<i; ++i)
        {
          hnd.detectors[j].release();
          hnd.extractors[j].release();
        }
        return(2);
      }
    }
    else if(algorithm_id==JMAGINE_ALGORITHM_SURF_SURF)
    {
      hnd.detectors[i]= new SURF(JMAGINE_FEATURES_SURF_HTHRESHOLD, JMAGINE_FEATURES_SURF_NB_OCTAVES, JMAGINE_FEATURES_SURF_NB_OCTAVE_LAYERS,
                                 JMAGINE_FEATURES_SURF_EXTENDED, JMAGINE_FEATURES_SURF_UPRIGHT);
      if(hnd.detectors[i]==NULL)
      {
        for(j=0; j<i; ++i)
        {
          hnd.detectors[j].release();
          hnd.extractors[j].release();
        }
        return(1);
      }
      hnd.extractors[i]=new SURF(JMAGINE_FEATURES_SURF_HTHRESHOLD, JMAGINE_FEATURES_SURF_NB_OCTAVES, JMAGINE_FEATURES_SURF_NB_OCTAVE_LAYERS,
                                 JMAGINE_FEATURES_SURF_EXTENDED, JMAGINE_FEATURES_SURF_UPRIGHT);
      if(hnd.extractors[i]==NULL)
      {
        for(j=0; j<i; ++i)
        {
          hnd.detectors[j].release();
          hnd.extractors[j].release();
        }
        return(2);
      }
    }
    else
    {
      for(j=0; j<i; ++i)
      {
        hnd.detectors[j].release();
        hnd.extractors[j].release();
      }
      return(3);
    }
  }
  return(0);
}




// Creates the descriptors matchers according to the params in the specified handle.
// Returns 0 on success, something else on failure.
int jmagine_features_create_matchers(jmagine_features_handle_t& hnd)
{
  int i;

  hnd.matchers_lsh_params.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  hnd.matchers_kdtree_params.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  hnd.matchers_search_params.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  hnd.matchers.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  hnd.matchers_bruteforce.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  hnd.matchers_crosscheck.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  for(i=0; i<JMAGINE_NB_ALGORITHMS_TO_USE; ++i)
  {
    if(JMAGINE_FEATURES_MATCHING_USE_FLANN[i])
    {
      hnd.matchers_lsh_params[i]=new flann::LshIndexParams(JMAGINE_FEATURES_LSH_NB_TABLES[i], JMAGINE_FEATURES_LSH_KEY_SIZE[i],
                                                              JMAGINE_FEATURES_LSH_MULTI_PROBE_LEVEL[i]);
      hnd.matchers_kdtree_params[i]=new flann::KDTreeIndexParams(JMAGINE_FEATURES_KDTREE_NB_TREES[i]);
      hnd.matchers_search_params[i]=new flann::SearchParams(JMAGINE_FEATURES_FLANN_SEARCH_NB_CHECKS[i]);
      if(JMAGINE_FEATURES_FLANN_TYPE[i]==JMAGINE_FLANN_TYPE_KDTREE)
        hnd.matchers[i]=new FlannBasedMatcher(hnd.matchers_kdtree_params[i], hnd.matchers_search_params[i]);
      else
        hnd.matchers[i]=new FlannBasedMatcher(hnd.matchers_lsh_params[i], hnd.matchers_search_params[i]);
    }
    else
    {
      hnd.matchers_bruteforce[i]=new BFMatcher(JMAGINE_FEATURES_MATCHING_BRUTEFORCE_NORM[i], false);
    }
    hnd.matchers_crosscheck[i]=new BFMatcher(JMAGINE_FEATURES_CROSSCHECK_NORM[i], false);
  }

  return(0);
}




// Load all triggers into the specified handle. This function will also train all relevant matchers accordingly.
// Returns 0 on success, an error code on failure.
int jmagine_features_load_triggers(jmagine_features_handle_t& hnd, const char* base_path, int db_id)
{
  int i, algorithm_pos, rv;
  vector<vector<Mat> > all_descriptors;
  const string* triggers;

  all_descriptors.resize(JMAGINE_NB_ALGORITHMS_TO_USE);
  hnd.triggers.resize(JMAGINE_NB_ALGORITHMS_TO_USE);

  triggers=JMAGINE_TRIGGERS[db_id];
  // Load available triggers.
  for(i=0; i<JMAGINE_NB_TRIGGERS[db_id]; ++i)
  {
    Mat desc;
    jmagine_trigger_t ntrigger;
    char tmp_path[3000];
#ifdef JMAGINE_HTTPD
    LOGD("Loading trigger '%s'...", triggers[i].c_str());
#endif
    if(base_path[0]!='\0')
      snprintf(tmp_path, 3000, "%s/%s", base_path, triggers[i].c_str());
    else
      snprintf(tmp_path, 3000, "%s", triggers[i].c_str());
    if((rv=jmagine_utils_load_trigger_binary(tmp_path, ntrigger))!=0)
      return(rv);

    algorithm_pos=jmagine_features_get_algorithm_pos_from_id(ntrigger.algorithm_id);
    if((algorithm_pos<0) || (algorithm_pos>=JMAGINE_NB_ALGORITHMS_TO_USE))
    {
      LOGE("Unknown algorithm ID '%d' in file '%s'.", ntrigger.algorithm_id, triggers[i].c_str());
      return(-1);
    }

    all_descriptors[algorithm_pos].push_back(ntrigger.descriptors);
    hnd.triggers[algorithm_pos].push_back(ntrigger);
  }

  // Now train each matcher with the corresponding database of descriptors.
  for(i=0; i<JMAGINE_NB_ALGORITHMS_TO_USE; ++i)
  {
#ifdef TKDV_DEBUG
    LOGD("Loaded %lu triggers for algorithm 0x%02X.", all_descriptors[i].size(), JMAGINE_ALGORITHMS_TO_USE[i]);
#endif
    if(hnd.triggers[i].size()==0)
    {
#ifdef TKDV_DEBUG
      LOGD("No triggers for algorithm 0x%02X, not training matchers.", JMAGINE_ALGORITHMS_TO_USE[i]);
#endif
      continue;
    }
#ifdef TKDV_DEBUG
    LOGD("Algorithm 0x%02X, training general matcher (using flann: %d)...", JMAGINE_ALGORITHMS_TO_USE[i], JMAGINE_FEATURES_MATCHING_USE_FLANN[i]);
    double t=(double)getTickCount();
#endif
    if(JMAGINE_FEATURES_MATCHING_USE_FLANN[i])
    {
      hnd.matchers[i]->add(all_descriptors[i]);
      // Train the matcher.
      hnd.matchers[i]->train();
    }
    else
    {
      hnd.matchers_bruteforce[i]->add(all_descriptors[i]);
      // Train the matcher.
      hnd.matchers_bruteforce[i]->train();
    }
#ifdef TKDV_DEBUG
    t=((double)getTickCount() - t)/getTickFrequency();
    LOGD("Trained general matcher for algorithm 0x%02X, time taken: %.04f seconds.", JMAGINE_ALGORITHMS_TO_USE[i], t);
#endif
    all_descriptors[i].clear();
  }

  return(0);
}




// Updates the score of the specified candidate based on the specified matches.
// The score is the mean value of the distance for the matches.
// Returns 0 on success, -1 on failure.
int jmagine_features_update_candidate(jmagine_candidate_match_t& candidate, const vector<DMatch>& matches, int algorithm_id)
{
  size_t i;
  double score=0;
  double dist_thr=-1.;
  double max_dists=-4242424.;
  double min_dists=4242424.;

  // If enabled, we'll compute the min dist and max dist of all the triggers.
  // This will allow to use only part of the dists in the mean computation below.
  if(JMAGINE_FEATURES_MATCHING_FILTER_MEAN_DIST[algorithm_id])
  {
    for(i=0; i<matches.size(); ++i)
    {
      if(matches[i].distance>max_dists)
        max_dists=matches[i].distance;
      else if(matches[i].distance<min_dists)
        min_dists=matches[i].distance;
    }
    if((max_dists==-4242424) || (min_dists==4242424) || (min_dists==max_dists) ||
       (max_dists<0) || (min_dists<0))
      dist_thr=-1.;
    else
      dist_thr=(max_dists-min_dists)/JMAGINE_FEATURES_MATCHING_DIST_ATHRESHOLD[algorithm_id];
  }

  score=0.;
  int nb_total=0;
  for(i=0; i<matches.size(); ++i)
  {
    if((JMAGINE_FEATURES_MATCHING_FILTER_MEAN_DIST[algorithm_id]) && (dist_thr!=-1.))
    {
      if(matches[i].distance<(min_dists+dist_thr))
      {
          nb_total++;
          score+=matches[i].distance;
      }
    }
    else
    {
      score+=matches[i].distance;
      nb_total++;
    }
  }
  if(nb_total==0)   // Filtering removes all matches, do not filter...
  {
    score=0.;
    nb_total=0;
    for(i=0; i<matches.size(); ++i)
    {
      score+=matches[i].distance;
      nb_total++;
    }
  }
  score=score/double(nb_total);
  candidate.nb_matches=matches.size();
  candidate.score=score;
  candidate.nb_matches_used_for_score=nb_total;

  return(0);
}




// From the specified homography, try to pinpoint a shape for the matched model. Information about the shape
// (its points, its area, convexity, etc.) will be stored in the specified candidate.
// cols is the length of the model image, rows is its width.
// If approx is true, then the computed shape will be poly-approxed before extracting the infos.
// Returns 0 on success, something else on failure.
int jmagine_features_get_topology_homography(jmagine_candidate_match_t& candidate, const Mat& homography, int cols,
                                             int rows, bool approx)
{
  size_t i;
  vector<Point2f> model_corners(4);  // Our "origin" points.
  vector<Point2f> corners(4);   // The same after the perspective transform from the homography.
  vector<cv::Point> corners_contour;   // Contour formed by the four corners.
  Mat ctr;
  vector<cv::Point> corners_contour_approxed;   // Approxed contour (if needed).
  bool convex=false;
  double cosine, irregularity;

  // Origin: the four corners of the model image.
  model_corners[0]=Point2f(0,0);
  model_corners[1]=Point2f(cols, 0);
  model_corners[2]=Point2f(cols, rows);
  model_corners[3]=Point2f(0, rows);

  // Compute transformed points.
  perspectiveTransform(model_corners, corners, homography);
  // Create contour.
  for(i=0; i<4; ++i)
    corners_contour.push_back(cv::Point(corners[i].x, corners[i].y));

  // If needed: approx contour.
  if(approx)
  {
    ctr=Mat(corners_contour);
    if(JMAGINE_FEATURES_TOPOLOGY_APPROX_ARCLENGTH)
      approxPolyDP(ctr, corners_contour_approxed, arcLength(ctr, true)*JMAGINE_FEATURES_TOPOLOGY_APPROX_PONDERATION, true);
    else
      approxPolyDP(ctr, corners_contour_approxed, JMAGINE_FEATURES_TOPOLOGY_APPROX_EPSILON, true);
    corners_contour.swap(corners_contour_approxed);
    corners_contour_approxed.clear();
  }

  // Now, compute the various info about the projected shape (area, convexity, etc.).
  candidate.shape_contour.swap(corners_contour);
  // Area.
  candidate.shape_area=contourArea(candidate.shape_contour);
  // Convexity (if enabled).
  if(JMAGINE_FEATURES_TOPOLOGY_COMPUTE_CONVEXITY)
  {
    convex=isContourConvex(corners_contour);
    if(convex)
      candidate.shape_is_convex=1;
    else
      candidate.shape_is_convex=0;
  }
  else
    candidate.shape_is_convex=-1;

  // Number of faces.
  if(candidate.shape_contour.size()==1)
    candidate.shape_nb_faces=0;
  if(candidate.shape_contour.size()==2)
    candidate.shape_nb_faces=1;
  else
    candidate.shape_nb_faces=candidate.shape_contour.size();

  // Rectangle: compute irregularity.
  if(candidate.shape_nb_faces==4)
  {
    irregularity=0;
    for(i=2; i<5; ++i)
    {
      // Find the maximum cosine of the angle between joint edges.
      cosine=fabs(jmagine_utils_angle_vectors(candidate.shape_contour[i%4], candidate.shape_contour[i-2],
                                              candidate.shape_contour[i-1]));
      irregularity+=cosine;  // Sum of all the abs(cos) - so basically the most ~90° angles, the less the value.
    }
  }
  else
    irregularity=-1;
  candidate.shape_irregularity=irregularity;

  return(0);
}




// Returns 0 if any of the specified algorithms are not supported. This used the algorithms ID as defined in jmagine_parameters.hpp.
// If all of them are supported, returns !=0.
int jmagine_features_are_algorithms_supported(const int* algorithms, int length)
{
  int i;
  for(i=0; i<length; ++i)
  {
    if((algorithms[i]!=JMAGINE_ALGORITHM_SURF_SURF) && (algorithms[i]!=JMAGINE_ALGORITHM_FAST_FREAK))
      return(0);
  }
  return(1);
}




// From an algorithm ID, this returns the position of the same in the global parameters table of algorithms to use.
// If the specified ID is not found, returns -1.
int jmagine_features_get_algorithm_pos_from_id(int algorithm_id)
{
  int i;
  for(i=0; i<JMAGINE_NB_ALGORITHMS_TO_USE; ++i)
  {
    if(algorithm_id==JMAGINE_ALGORITHMS_TO_USE[i])
      return(i);
  }
  return(-1);
}
