/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_geoloc.hpp - Geoloc module header (right now, only coords distance calculations and so on).
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __JMAGINE_GEOLOC_HPP
#define __JMAGINE_GEOLOC_HPP


#define JMAGINE_EARTH_RADIUS    6371009       // in meters (mean over the surface)


// Returns the distance, in meters, between the two specified points. Uses the haversine formula.
// Returns -1 on error.
double jmagine_get_dist_gps_coords_meters(double lat_1, double lon_1, double lat_2, double lon_2);


#endif //__JMAGINE_GEOLOC_HPP
