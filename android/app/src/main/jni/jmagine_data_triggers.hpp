/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_data_triggers.hpp - Data / Elements to be recognized.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_DATA_TRIGGERS_HPP
#define __TOKIDEV_JMAGINE_DATA_TRIGGERS_HPP

#include "jmagine_parameters.hpp"


const string JMAGINE_TRIGGERS_OLD_NICE[JMAGINE_NB_TRIGGERS_OLD_NICE]={ "49697d9b34ad0a4c282208ffb55ed2bf.sns", "71991e98f3d1d56b4c21b5629b29a966.sns",
                                                                       "7729dcc4f2d08a715a6e16ec1e478013.sns", "c754182660bdddc9ea08abf19257e6ad.sns",
                                                                       "dd4682164fffcf5ed44d76d2fad34e52.sns", "f558bd0ee0b3734db99b695251a6ee5d.sns",
                                                                       "fc4d567a16ce0152e67cfe7b41fffd1c.sns", "fd79dadfbf1fc1d484e208c989a668c0.sns",
                                                                       "51824906914fb2b3573e01e23121ee42.sns", "8261322d4755cfffd61b36918be072b8.sns",
                                                                       "a9f703905bb39108290ccddd733235e3.sns", "ae40fbcfb520bc9bf1ee1d5d558dc6fa.sns",
                                                                       "ba45a356240198d5db55911ee49cb377.sns", "5e17c5c0bef2b060134b00fdcda44a1f.sns",
                                                                       "f7d34fe9c9eb306b8c4e608cf9800499.sns", "5052882bb263c4df71f3419a1692b277.sns",
                                                                       "921ddc83bfb5a95fa85422c2d758f4e1.sns", "f76d7ff4534ffc461c576b95ef430040.sns",
                                                                       "91172fc6e12109ad9a0e58123be0dab1.sns", "a2f3bb689b9c45d7db91fe736d3315e0.sns",
                                                                       "1d83ba0c18d5d966ffb0cb08a1adc788.sns", "24e5aa870272f7e04dbe023d87b7605f.sns",
                                                                       "89ccb939ebbf6589846cca398b1c8c23.sns", "daaa218fba64e4fd9dceb945876443e5.sns",
                                                                       "9d9d8c4699d23f2634049bf2510de068.sns", "9ac956b6a612fb90f2210cbf1358981f.sns",
                                                                       "865afdd200eb70074c6b8e97ecc29b3d.sns", "a8271c195dac0157a3616187b013950a.sns",
                                                                       "f7e58f511e101cdff29f9f2017d9dc9d.sns", "b6fb3c280ea3bfb216b905957ee4b90c.sns",
                                                                       "f9e2b708d09173d80f69d406043f3e04.sns", "8270e40172167d798b0c3413022e94f3.sns",
                                                                       "45b9e1b5c8445d5256980a1a6bc0cb23.sns", "9f0ac5ac4d1583a3fdf7220abb69bdbc.sns",
                                                                       "52eded364848201e10d9bea88a2fd9c8.sns", "6425d85beff23680e5ab7de01099c77c.sns",
                                                                       "0c8d5112de1220adf9e9013a20c2ec17.sns", "a713b8a61d8e48dc3cefc0ad727ffcf1.sns",
                                                                       "fd49f81f24aecbcc88d70bc99c057304.sns", "47445db3a9cf1d2aa4dc11aa33761e59.sns",
                                                                       "8ced663955add181fe81e66093d0f199.sns", "18a0d500796bb7640869d925570de682.sns",
                                                                       "d169aae3efe576ec4610ff2ff9ece6f0.sns", "bda598d0252b4920ccdb88a6cff8a45c.sns",
                                                                       "30c2db6a33cf7dd0767411ee468f6cf9.sns", "1bed5e457b8c6ae79ec2b9582348714a.sns",
                                                                       "fccc7ef96fe456c50cfae751e46301d3.sns", "70dc459a8aaea32df6040a15ff5d3c26.sns",
                                                                       "fae4e83c98d46041aec9175006abe6b1.sns", "08d2891bfc507c7a3891d4e3eed866f9.sns",
                                                                       "dbc7c934e06a97f6a1d21a2b842da236.sns", "8bd5d4fa20b3b8f19b5cbef7dc2d8c23.sns",
                                                                       "046f0732a3b82e95955b6232c240e141.sns", "2e75b151a534e3ecffee3a729cf32f50.sns",
                                                                       "0b5074b236bee8d6818aaaf20dd65626.sns", "360b2bcf16f3cb7139f6635e50f92b24.sns",
                                                                       "75a25b00844debc1a8818fca03eaabc7.sns", "ccae43e5471d7ea6f2b1e75d022710cd.sns",
                                                                       "af9083aaaa5b48ef6fb7acda3cd0ccf8.sns", "9ce1800335aca1503a1dcc91d1edccf0.sns",
                                                                       "45143e356fba15da593a824cbfb36f1e.sns", "1e443bb19302392f89bbb1a22f54b261.sns",
                                                                       "e498ee6884ed3c31db15fa97a24aec63.sns", "7586e059598151bc2df9c0cd42d291e9.sns",
                                                                       "efd276f55f7916bbab713273294cdef2.sns", "b9a4ffd6831527eef0e952ad0a27d56c.sns",
                                                                       "57739751565b41fbfc08697e32d2e163.sns", "46cb6a7b98a591b37c026b9d189cf922.sns",
                                                                       "31e2dbdb9a9d6ed97ee196c0b3b859cc.sns", "210e892dd86acaf522251900f6f46f4f.sns",
                                                                       "5540f2f2c161d31ba0e2aa934f179f41.sns", "5aefe7a5bb3c9cf85525cd1f7593639e.sns",
                                                                       "89afd1e0a095029e6c81e345c2ed3515.sns", "ffcac9e48a882bb811835852763ffed6.sns",
                                                                       "6ab8019b0dfd1af2a822f24732ebb63b.sns", "1fbfc9a990775da4d13211f5d89a65d7.sns",
                                                                       "a09ffacb270338ffc872fd775bc2e720.sns", "21b24f7b8cd0b3100b10cf307558c27b.sns",
                                                                       "a4c743831a723ffa336b696f0dabd59f.sns", "683fdbb44da3f4703cff1ee7d4124e9d.sns",
                                                                       "b542e931eee34859697de1b0647bc393.sns", "bf666adb794276dbbeb4c31f4cb443dd.sns",
                                                                       "c93845bc88c30050292106e3040204f3.sns", "7a04495fdd6c95e1e66a42c4c1df09ac.sns",
                                                                       "ff1f54810a8042639455b5ef5f924448.sns", "a313d3dfdc6a97d00617276e295f5171.sns",
                                                                       "cc1cf27854847eb8be1dbb969a86ca9c.sns", "cd2eaf1afea946f87d1bca4774cdb8cc.sns",
                                                                       "e5976106fa8ab1b625101393bdc5b1cc.sns", "1d0ecf88d60ddcb58ec621f7c2c6ddd9.sns",
                                                                       "81d968adf4a2cfd4f5cc2df908137a18.sns", "6dbd0dc4f9b0afde2a6506632d69e6f0.sns",
                                                                       "2f7c0200968bd85db52b73d74a7e7b8d.sns", "181de8e38b566f9bb5f9a6e9a9176b56.sns",
                                                                       "8270c4fb5d853a8a315888ffb6f5397b.sns", "5dfdaf43f078901ad388b205061de801.sns",
                                                                       "540e91cec7af20d25d56622ed2613c66.sns", "4fd0f26abc30f1f897c7f64141e8ee2a.sns",
                                                                       "c020c64b9f073d1ca1557d063244aee8.sns", "cdcd605093dade3cd72d174c66c88f80.sns",
                                                                       "5918d725b41219a2bf6c7f3afdaa8162.sns", "66d5c6a30fd134a7eabad6528ab5a139.sns" };

const string* JMAGINE_TRIGGERS[JMAGINE_NB_DATABASES]={ JMAGINE_TRIGGERS_OLD_NICE };
const int JMAGINE_NB_TRIGGERS[JMAGINE_NB_DATABASES]={ JMAGINE_NB_TRIGGERS_OLD_NICE };


#endif //__TOKIDEV_JMAGINE_DATA_TRIGGERS_HPP
