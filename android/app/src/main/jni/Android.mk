# This needs to be set to the path of OpenCV Android SDK.
OPENCV_SDK_PATH=../../../../../../psa/ocv-sdk
OPENCV_SDK_MK_PATH=$(OPENCV_SDK_PATH)/native/jni/OpenCV.mk
LOCAL_PATH := $(call my-dir)

OPENCV_CAMERA_MODULES:=off
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=SHARED

include $(CLEAR_VARS)
LOCAL_MODULE := tkdv_ocv_nf_prebuilt
LOCAL_SRC_FILES := ../jni/ocvnf/$(TARGET_ARCH_ABI)/libocvnf.so
LOCAL_EXPORT_C_INCLUDES := $(OPENCV_SDK_PATH)/native/jni/include
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
include $(OPENCV_SDK_MK_PATH)
LOCAL_LDLIBS := -llog -ljnigraphics -lm
LOCAL_CFLAGS    := -DTKDV_ANDROID
LOCAL_MODULE    := tkdv_jmagine_jni
LOCAL_SHARED_LIBRARIES += tkdv_ocv_nf_prebuilt
LOCAL_SRC_FILES := jmagine_android.cpp jmagine_android.hpp \
                                                                         jmagine_parameters.hpp \
                                                                   jmagine_recognition.cpp jmagine_recognition.hpp \
                                                                   jmagine_features.cpp jmagine_features.hpp \
                                                                   jmagine_features_filtering.cpp jmagine_features_filtering.hpp \
                                                                   jmagine_utils.cpp jmagine_utils.hpp jmagine_geoloc.cpp jmagine_geoloc.hpp \
                                                                         jmagine_order32.hpp
include $(BUILD_SHARED_LIBRARY)
