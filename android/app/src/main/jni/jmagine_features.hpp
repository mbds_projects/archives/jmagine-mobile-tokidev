/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_features.hpp - Header features detection module. Uses features descriptors to match
                         against known triggers.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __JMAGINE_FEATURES_HPP
#define __JMAGINE_FEATURES_HPP

#ifdef  TKDV_ANDROID
#include "jmagine_android.hpp"
#else
#include "jmagine.hpp"
#endif
#include "jmagine_parameters.hpp"


// Defines a model (a trigger).
// The image itself is not stored.
struct jmagine_trigger_t
{
  Mat descriptors;
  vector<KeyPoint> keypoints;
  size_t len_x, len_y;
  string name;
  int algorithm_id;
  short element_id;
};

// Library handle for the features module (see jmagine_reco.hh).
struct jmagine_features_handle_t
{
  vector<vector<jmagine_trigger_t> > triggers;
  vector<flann::LshIndexParams*> matchers_lsh_params;
  vector<flann::KDTreeIndexParams*> matchers_kdtree_params;
  vector<flann::SearchParams*> matchers_search_params;
  vector<FlannBasedMatcher*> matchers;
  vector<BFMatcher*> matchers_bruteforce;
  vector<BFMatcher*> matchers_crosscheck;
  vector<Ptr<FeatureDetector> > detectors;
  vector<Ptr<DescriptorExtractor> > extractors;

  jmagine_features_handle_t()
  {
    // Nothing (for now).
  }
};

// Defines a match candidate.
struct jmagine_candidate_match_t
{
  int algorithm_id;
  int trigger_id;
  int element_id;
  int nb_matches;
  double score;
  int nb_matches_used_for_score;

  // Homography & topology stuff.
  // This will stay empty unless the relevant options are enabled (see jmagine_parameters.hh).
  vector<cv::Point> shape_contour;  // Projected shape contour (can be approxed, see parameters).
  double shape_area;   // Area of the projected shape.
  int shape_is_convex;   // Wether or not the shape is convex.
  int shape_nb_faces;  // Number of faces.
  double shape_irregularity;   // Only if four faces: this is an indice of the irregularity of the rectangle.

  jmagine_candidate_match_t()
  {
    trigger_id=-1;
    algorithm_id=-1;
    element_id=-1;
    nb_matches=0;
    score=-1.;
    shape_area=-1.;
    shape_is_convex=-1;
    shape_nb_faces=-1;
    shape_irregularity=-1.;
    nb_matches_used_for_score=-1;
  }
};


// Initializes a features library handle. Returns 0 on success, something else on failure.
int jmagine_features_init(jmagine_features_handle_t& handle, const char* base_path, int db_id);

// Entry point for the features analysis. This will call the jmagine_perform_features_detection function for the specified query for each
// supported features detection algorithm. Those will return a list of likely matches. Then, this function will apply processing in
// order to present the results to the calling layer as if there was only one algorithm being used - in other words, it will combine
// the results of the various features detection algorithms.
// This is a hack due to the recent addition (post prototype branch) of the multi-detector support; it will eventually need to be
// replaced by something much cleaner than this.
// Returns 0 on success, an error code on failure.
int jmagine_features_detection(jmagine_features_handle_t& hnd, const Mat& query, vector<jmagine_candidate_match_t>& matches);

// The main features analysis function. This takes the specified image, generate its keypoints and descriptors for the specified algorithm,
// matches it against the library of known model, and then apply a large amount of filtering/other matching
// operations according to the algorithms params. Finally, it sorts through the filtered matches and attributes
// a score to each of those probable matches, putting them into the specified vector.
// If max_matches is >0, then no more than max_matches will be returned (the max_matches best results).
// Returns 0 on success, something else on failure.
int jmagine_perform_features_detection(jmagine_features_handle_t& hnd, const Mat& query, vector<jmagine_candidate_match_t>& matches, int algorithm_id);

// Creates the features detectors/descriptors extractors according to the params in the specified handle.
// Returns 0 on success, something else on failure.
int jmagine_features_create_detectors_extractors(jmagine_features_handle_t& hnd);

// Creates the descriptors matchers according to the params in the specified handle.
// Returns 0 on success, something else on failure.
int jmagine_features_create_matchers(jmagine_features_handle_t& hnd);

// Load all triggers into the specified handle. This function will also train all relevant matchers accordingly.
// Returns 0 on success, an error code on failure.
int jmagine_features_load_triggers(jmagine_features_handle_t& hnd, const char* base_path, int db_id);

// Updates the score and numbef of matches of the specified candidate based on the specified matches.
// The score is the mean value of the distance for the matches.
// Returns 0 on success, -1 on failure.
int jmagine_features_update_candidate(jmagine_candidate_match_t& candidate, const vector<DMatch>& matches, int algorithm_id);

// From the specified homography, try to pinpoint a shape for the matched model. Information about the shape
// (its points, its area, convexity, etc.) will be stored in the specified candidate.
// cols is the length of the query image, rows is its width.
// If approx is true, then the computed shape will be poly-approxed before extracting the infos.
// Returns 0 on success, something else on failure.
int jmagine_features_get_topology_homography(jmagine_candidate_match_t& candidate, const Mat& homography, int cols,
                                             int rows, bool approx);

// From an algorithm ID, this returns the position of the same in the global parameters table of algorithms to use.
// If the specified ID is not found, returns -1.
int jmagine_features_get_algorithm_pos_from_id(int algorithm_id);

// Returns 0 if any of the specified algorithms are not supported. This used the algorithms ID as defined in jmagine_parameters.hpp.
// If all of them are supported, returns !=0.
int jmagine_features_are_algorithms_supported(const int* algorithms, int length);



#endif //__JMAGINE_FEATURES_HPP
