APP_STL := gnustl_static
APP_CPPFLAGS := -frtti -fexceptions

# For release version, use ALL.
#APP_ABI := armeabi armeabi-v7a
APP_ABI := armeabi armeabi-v7a mips x86
