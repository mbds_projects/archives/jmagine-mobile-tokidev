/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_utils.hpp - Utils module - contains various useful functions.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_UTILS_HPP
#define __TOKIDEV_JMAGINE_UTILS_HPP


#ifdef  TKDV_ANDROID
#include "jmagine_android.hpp"
#else
#include "jmagine.hpp"
#endif
#include "jmagine_features.hpp"



// Read the specified file in its entirety and store the results into the specified string.
// Returns 0 on success, something else on failure.
int jmagine_utils_read_file(const char* path, string& contents);

// Read the specified binary file in its entirety and put the result into the specified matrix.
// The binary format is SNS-specific.
// The format is as follow:
//   [MAGIC VALUE|3B][MAJOR VERSION|1B][MINOR VERSION|1B][STR|null-terminated][ALGORITHM_ID]
//   [RESOLUTION_X|2B][RESOLUTION_Y|2B][KPS_LEN|2B][KPS]
//   [DESCRIPTORS_LEN|2B][DESCRIPTORS_SIZE|2B][DESCRIPTORS_VALUES]
// The type of algorithm dictates the format of the keypoints and descriptors. It describes both
// the kp detection algorithm and the feature descriptor used.
// The descriptors themselves are stored descriptor after descriptor, each with a size of
// DESCRIPTORS_SIZE.
// Returns 0 on success, something else on failure.
int jmagine_utils_load_trigger_binary(const char* path, jmagine_trigger_t& dest);

// From an opencv file memory format (see jmagine_data_* for examples), this extracts a usable
// matrix into dst.
// Returns 0 on success, something else on failure.
int jmagine_utils_mem2mat(const string& src, const string& name, Mat& dst);

// From an opencv file memory format (see jmagine_data_* for examples), this extracts a usable
// vector of KeyPoint into dst.
// Returns 0 on success, something else on failure.
int jmagine_utils_mem2vec_kps(const string& src, const string& name, vector<KeyPoint>& dst);

// From an opencv file memory format (see jmagine_data_* for examples), this extracts a usable
// size_t value into *dst.
// Returns 0 on success, something else on failure.
int jmagine_utils_mem2ulong(const string& src, const string& name, size_t* dst);

// Get a random color from the specified random number generator.
Scalar jmagine_utils_random_color(RNG& rng);

// Returns the length of the line between the two specified points.
double jmagine_utils_get_length_line(const cv::Point& a, const cv::Point& b);

// Returns the 'angle' of the line between the two specified points.
// That is, the angle between the horizon and the line.
// The angle is returned in degrees, in absolute value.
double jmagine_utils_get_angle_line(const cv::Point& a, const cv::Point& b);

// Returns an angle in radians from an angle in degrees.
double jmagine_utils_deg2rad(double deg);

// Returns an angle in degrees from an angle in radian.
double jmagine_utils_rad2deg(double radian);

// Returns the cosine of the angle between the two vectors
// pt0->pt1 and pt0->pt2.
double jmagine_utils_angle_vectors(cv::Point pt1, cv::Point pt2, cv::Point pt0);

// Returns the absolute difference between a and b, even if a is negative, b too, etc.
int jmagine_utils_get_absolute_diff(int a, int b);

// Returns the absolute difference between floats a and b, even if a is negative, b too, etc.
double jmagine_utils_get_absolute_diff_float(double a, double b);

// Normalize src and put the result into dst.
// For now, this performs a simple histogram equalization.
// Returns 0 on success, something else on failure.
int jmagine_utils_normalize_img(const Mat& src, Mat& dst);

// This goes through the specified list of matches and returns the minimum and maximum distance found respectively in
// *min_dist and *max_dist.
// min_dist and max_dist can be NULL.
// Returns 0 on success, -1 on failure. An empty matches list is a failure.
int jmagine_utils_get_min_max_dist_matches(const vector<DMatch>& matches, double* min_dist, double* max_dist);

// Put the n top matches (in terms of distance) found in the specified matches vector into top_matches.
// Returns 0 on success, -1 on failure.
// Having nb_top matches or less than that is a failure.
int jmagine_utils_get_top_matches(const vector<DMatch>& matches, vector<DMatch>& top_matches, unsigned int nb_top);

// This function will read a short value stored as little-endian from the specified buffer at the specified
// position, and put said value into the specified short (as either little endian or big endian depending on
// the current arch).
// Returns 0 on success, -1 on failure.
int jmagine_utils_read_short_little_endian(const char* buffer, int pos, short& value);

// This function will read an int value stored as little-endian from the specified buffer at the specified
// position, and put said value into the specified int (as either little endian or big endian depending on
// the current arch).
// Returns 0 on success, -1 on failure.
int jmagine_utils_read_int_little_endian(const char* buffer, int pos, int& value);

// This function will read a float value stored as little-endian from the specified buffer at the specified
// position, and put said value into the specified float (as either little endian or big endian depending on
// the current arch).
// Returns 0 on success, -1 on failure.
int jmagine_utils_read_float_little_endian(const char* buffer, int pos, float& value);

// Returns 1 if the current arch is big-endian, 0 if it is not.
// This makes use of the jmagine_order32.hpp header.
int jmagine_utils_is_big_endian();

// This function will check wether or not the length of the standard data types on the
// current platform are as expected. It will return 0 if that's the case.
// It should be noted that most (if not all) the binary loading code is already compliant
// with archs that have different values for those; however, this has not been tested
// at all. The aim of this function is to make sure we don't run on those archs (if there are
// actually any) until proper testing has been done.
int jmagine_utils_check_supported_len_datatypes();

// This function will check wether or not the current arch is supported in terms of endianness.
// It will return 0 if it is. Currently, little-endian and big-endian archs are supported (those
// should be the only ones encountered given the target platforms anyway).
int jmagine_utils_check_supported_endianness();



#endif //__TOKIDEV_JMAGINE_UTILS_HPP
