/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_order32.hpp - Header defining macros to pinpoint the endianness of the current arch.
  ---
  Benjamin Renaut <renaut.benjamin@snapnsee.fr>
  Copyright (C) 2015 Snap'n See SAS
*/
#ifndef __TOKIDEV_JMAGINE_ORDER32_HPP
#define __TOKIDEV_JMAGINE_ORDER32_HPP


#include <limits.h>
#include <stdint.h>

#if CHAR_BIT!=8
#error "Unsupported char size."
#endif

enum
{
  SNS_O32_LITTLE_ENDIAN=0x03020100ul,
  SNS_O32_BIG_ENDIAN=0x00010203ul,
  SNS_O32_PDP_ENDIAN=0x01000302ul
};

static const union { unsigned char bytes[4]; uint32_t value; } o32_host_order={ { 0, 1, 2, 3 } };

#define SNS_O32_HOST_ORDER (o32_host_order.value)


#endif //__TOKIDEV_JMAGINE_ORDER32_HPP
