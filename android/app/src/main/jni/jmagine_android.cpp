/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine.cpp - Main android module (JNI links, etc.)
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine_android.hpp"
#include "jmagine_recognition.hpp"
#include "jmagine_geoloc.hpp"
#include "jmagine_data_elements.hpp"
#include "jmagine_parameters.hpp"

static jmagine_handle_t hnd[JMAGINE_NB_DATABASES];
static int current_db_id=-1;
//static int nbb=0;


// Main recognition function, called by jmagine_recognition.
string jmagine_perform_recognition(const Mat& query, double gps_lat, double gps_lon);
JNIEXPORT jstring JNICALL jmagine_recognition(JNIEnv* env, jobject thisse, jdouble gps_lat, jdouble gps_lon, jlong img_in);
JNIEXPORT jint JNICALL jmagine_android_init(JNIEnv* env, jobject thisse, jstring base_path, jint db_id);


#define JMAGINE_JNI_NUM_METHODS  2
static const JNINativeMethod JMAGINE_JNI_METHODS[]={ {"jmagine_recognition",
                                                  "(DDJ)Ljava/lang/String;",
                                                  (void*)jmagine_recognition},
                                                 {"jmagine_android_init",
                                                  "(Ljava/lang/String;I)I",
                                                  (void*)jmagine_android_init} };


// Called when the native lib is loaded.
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
  jclass parent_class;
  JNIEnv* env=NULL;

  if(vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6)!=JNI_OK)
    return(-1);
  
  parent_class=env->FindClass(JAVA_JMAGINE_CLASSNAME);
  if(parent_class==NULL)
  {
    LOGE("Native registration error: unable to find class [%s].\n", JAVA_JMAGINE_CLASSNAME);
    return(-1);
  }
 
  if(env->RegisterNatives(parent_class, JMAGINE_JNI_METHODS, JMAGINE_JNI_NUM_METHODS)<0)
  {
    LOGE("Native registration error: unable to register methods in class [%s].\n", JAVA_JMAGINE_CLASSNAME);
    return(-1);
  }

  return(JNI_VERSION_1_6);
}




JNIEXPORT jstring JNICALL jmagine_recognition(JNIEnv* env, jobject thisse, jdouble gps_lat, jdouble gps_lon, jlong addr_in)
{
  const Mat& query=*((Mat*)addr_in);
  string res;

  /*  nbb=nbb+1;
  char tmp[500];
  snprintf(tmp, 500, "/sdcard/%lu_%03d_jmagine.png", tstamp, nbb);
  imwrite(tmp, query);*/
  res=jmagine_perform_recognition(query, gps_lat, gps_lon);

  jstring str_ret=env->NewStringUTF(res.c_str());
  return(str_ret);
}




JNIEXPORT jint JNICALL jmagine_android_init(JNIEnv* env, jobject thisse, jstring base_path, jint db_id)
{
  jint rv=0;
  string path=env->GetStringUTFChars(base_path, NULL);
  char fname[500];
  //  tstamp=time(NULL);

  if((db_id<0) || (db_id>=JMAGINE_NB_DATABASES))
    return(-1);
  if(!hnd[db_id].initialized)
  {
    if(jmagine_init(hnd[db_id], path.c_str(), db_id))
      return(-1);
  }
  current_db_id=db_id;

  return(0);
}




// Main recognition function, called by jmagine_recognition.
string jmagine_perform_recognition(const Mat& query, double gps_lat, double gps_lon)
{
  jmagine_query_t results;
  int rv;
  size_t i;
  vector<jmagine_reco_candidate_t> candidates;

 if((current_db_id<0) || (current_db_id>=JMAGINE_NB_DATABASES))
    return(string(""));

  rv=jmagine_analyze_image(hnd[current_db_id], query, results);
  if(rv)
    return(string(""));
  rv=jmagine_match_image(hnd[current_db_id], results, candidates);
  if(rv)
    return(string(""));

  if(candidates.size()>0)
  {
    vector<jmagine_reco_candidate_t> candidates_dist;
    LOGD("Candidates:");
    for(i=0; i<candidates.size(); ++i)
    {
      double dist=jmagine_get_dist_gps_coords_meters(gps_lat, gps_lon, JMAGINE_ELMT2GPS[candidates[i].element_id][0], JMAGINE_ELMT2GPS[candidates[i].element_id][1]);
      LOGD("  - #%lu: model %s, score %.02f, distance to candidate model: ~%.02f meters.", i+1, candidates[i].name.c_str(), candidates[i].score, dist);
      if(JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS<0.)
      {
        LOGD("    => Distance check disabled, pass the filter.");
      }
      else
      {
        if(((gps_lat<0.) || (gps_lon<0.)) && (JMAGINE_GPS_CHECK_ALLOW_NOCHECK))
        {
          LOGD("    => Pass distance check by default (at least one negative gps coord).");
          candidates_dist.push_back(candidates[i]);
        }
        else if((gps_lat<0.) || (gps_lon<0.))
        {
          LOGD("    => Doest NOT pass distance check (at least one negative gps coord).");
        }
        else if(dist<JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS)
        {
          LOGD("    => Pass distance check (max %.02f meters).", JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS);
          candidates_dist.push_back(candidates[i]);
        }
        else
        {
          LOGD("    => Does NOT pass distance check (max %.02f meter).", JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS);
        }
      }
    }
    if(JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS>0.)
    {
      candidates.clear();
      candidates.insert(candidates.end(), candidates_dist.begin(), candidates_dist.end());
    }
  }


  double max_score=-4242424;
  int max_score_idx=-1;
  for(i=0; i<candidates.size(); ++i)
  {
    if(candidates[i].score>max_score)
    {
      max_score=candidates[i].score;
      max_score_idx=i;
    }
  }

  string res="";
  if(max_score_idx!=-1)
  {
    res=candidates[max_score_idx].id;
  }
  else
    return(res);

  double limit=JMAGINE_MATCHING_GOOD_MATCH_MINSCORE;
  if(JMAGINE_MATCHING_ENABLE_GOOD_MATCH_FILTERING)
  {
    if((JMAGINE_MATCHING_F2F_GOOD_MATCH_TOLERANCE) && (hnd[current_db_id].previous_empty))
    {
      limit=JMAGINE_MATCHING_F2F_GOOD_MATCH_MINSCORE;
      hnd[current_db_id].previous_empty=false;
    }
    if(max_score>=limit)
    {
      // Do nothing.
    }
    else
    {
      res="";
      if(!hnd[current_db_id].previous_empty)
        hnd[current_db_id].previous_empty=true;
    }
  }
  else
  {
    // Do nothing.
  }

  return(res);
}
