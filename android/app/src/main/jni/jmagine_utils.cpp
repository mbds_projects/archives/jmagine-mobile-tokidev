/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_utils.cpp - Utils module - contains various useful functions.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine_utils.hpp"
#include "jmagine_order32.hpp"


#define SNS_BINFILE_LEN_MAGIC   4
const char SNS_BINFILE_MAGIC[SNS_BINFILE_LEN_MAGIC]={'S', 'N', 'S', 'T'};
#define SNS_BINFILE_MAJOR_VERSION  0x02
#define SNS_BINFILE_MINOR_VERSION  0x00
#define SNS_BINFILE_ALGORITHM_ID_SURF_SURF  0x20
#define SNS_BINFILE_ALGORITHM_ID_FAST_FREAK  0x21




// From an opencv file memory format (see jmagine_data_* for examples), this extracts a usable
// matrix into dst.
// Returns 0 on success, something else on failure.
int jmagine_utils_mem2mat(const string& src, const string& name, Mat& dst)
{
  FileStorage fstorage;

  if(!fstorage.open(src.c_str(), FileStorage::MEMORY+FileStorage::READ))
    return(1);
  fstorage[name]>>dst;
  if(!dst.data)
    return(2);
  fstorage.release();

  return(0);
}




// Read the specified file in its entirety and store the results into the specified string.
// Returns 0 on success, something else on failure.
int jmagine_utils_read_file(const char* path, string& contents)
{
  char buf[1024];
  FILE* fin;
  int nbread;

  fin=fopen(path, "r");
  if(fin==NULL)
    return(-1);
  while((nbread=fread(buf, sizeof(char), 1000, fin))>0)
  {
    buf[nbread]='\0';
    contents=contents+buf;
    if(feof(fin))
      break;
  }
  fclose(fin);
  return(0);
}




// Read the specified binary file in its entirety and put the result into the specified matrix.
// The binary format is SNS-specific.
// The format is as follow:
//   [MAGIC VALUE|3B][MAJOR VERSION|1B][MINOR VERSION|1B][STR|null-terminated][ALGORITHM_ID]
//   [RESOLUTION_X|2B][RESOLUTION_Y|2B][KPS_LEN|2B][KPS]
//   [DESCRIPTORS_LEN|2B][DESCRIPTORS_SIZE|2B][DESCRIPTORS_VALUES]
// The type of algorithm dictates the format of the keypoints and descriptors. It describes both
// the kp detection algorithm and the feature descriptor used.
// The descriptors themselves are stored descriptor after descriptor, each with a size of
// DESCRIPTORS_SIZE.
// Returns 0 on success, something else on failure.
int jmagine_utils_load_trigger_binary(const char* path, jmagine_trigger_t& dest)
{
  FILE* fin;
  char buf[16000];
  char* contents;
  int file_len;
  int nbread, cur;
  short data_short;
  char car;
  char name[251];
  int i, j;
  float x, y, size, angle, response, data_float;
  int octave, class_id, desc_nb, desc_len;

  fin=fopen(path, "rb");
  if(fin==NULL)
  {
    LOGE("Failed to open file '%s'.", path);
    return(-1);
  }
  fseek(fin, 0, SEEK_END);
  file_len=ftell(fin);
  fseek(fin, 0, SEEK_SET);

  contents=(char*)(malloc(sizeof(char)*file_len));
  if(contents==NULL)
  {
    LOGE("Could not load file: out of memory.\n");
    fclose(fin);
    return(-1);
  }

  // Read the entire file into the contents buffer.
  cur=0;
  while((nbread=fread(buf, sizeof(char), 16000, fin))>0)
  {
    if((cur+nbread)>file_len)
    {
      LOGE("Could not load file: buffer failure.\n");
      free(contents);
      fclose(fin);
      return(-1);
    }
    for(i=0; i<nbread; ++i)
      contents[cur+i]=buf[i];
    cur+=nbread;
  }
  fclose(fin);
  if(cur!=file_len)
  {
    LOGE("Could not load file: length sanity check failed.\n");
    free(contents);
    fclose(fin);
    return(-1);
  }

  cur=0;
  // Now, parse the contents.
  for(i=0; i<SNS_BINFILE_LEN_MAGIC; ++i)
  {
    if(contents[i]!=SNS_BINFILE_MAGIC[i])
    {
      LOGE("Invalid magic number in file '%s'.", path);
      free(contents);
      return(-1);
    }
  }
  // For now, only check that the version is the exact same.
  // Once new versions are created, backward compatibility will be needed here.
  if((contents[SNS_BINFILE_LEN_MAGIC]!=SNS_BINFILE_MAJOR_VERSION) &&
     (contents[SNS_BINFILE_LEN_MAGIC+1]!=SNS_BINFILE_MINOR_VERSION))
  {
    LOGE("Invalid version number in file '%s'.", path);
    free(contents);
    return(-1);
  }

  cur+=SNS_BINFILE_LEN_MAGIC+2;
  // Name has a max length of 250 (in version 1.0 of the format).
  memset(name, 0, 251);
  i=0;
  car=contents[cur];
  while(car!='\0')
  {
    name[i]=car;
    ++i;
    if(i>=251)
    {
      LOGE("Name field is too large in file '%s'.", path);
      free(contents);
      return(-1);
    }
    ++cur;
    car=contents[cur];
  }
  dest.name=name;
  ++cur;

  // Element ID.
  if(jmagine_utils_read_short_little_endian(contents, cur, data_short))
  {
    LOGE("Failed to read from file '%s'.", path);
    free(contents);
    return(-1);
  }
  dest.element_id=data_short;
  cur+=sizeof(short);

  // Algorithm ID.
  if(contents[cur]==SNS_BINFILE_ALGORITHM_ID_SURF_SURF)
    dest.algorithm_id=JMAGINE_ALGORITHM_SURF_SURF;
  else if(contents[cur]==SNS_BINFILE_ALGORITHM_ID_FAST_FREAK)
    dest.algorithm_id=JMAGINE_ALGORITHM_FAST_FREAK;
  else
  {
    LOGE("Invalid algorithm ID '%02X' in file '%s'.", car, path);
    free(contents);
    return(-1);
  }
  ++cur;

  // Checked the magic value, the version number, and retrieved the name & algorithm ID.
  // Now we start retrieving resolution information, keypoints and descriptors.

  // Resolution.
  if(jmagine_utils_read_short_little_endian(contents, cur, data_short))
  {
    LOGE("Failed to read from file '%s'.", path);
    free(contents);
    return(-1);
  }
  dest.len_x=data_short;
  cur+=sizeof(short);
  if(jmagine_utils_read_short_little_endian(contents, cur, data_short))
  {
    LOGE("Failed to read from file '%s'.", path);
    free(contents);
    return(-1);
  }
  dest.len_y=data_short;
  cur+=sizeof(short);

    // Keypoints.
  if(jmagine_utils_read_short_little_endian(contents, cur, data_short))
  {
    LOGE("Failed to read from file '%s'.", path);
    free(contents);
    return(-1);
  }
  cur+=sizeof(short);
  dest.keypoints.resize(data_short);
  for(i=0; i<data_short; ++i)
  {
    if(jmagine_utils_read_float_little_endian(contents, cur, x))
    {
      LOGE("Failed to read from file '%s'.", path);
      free(contents);
      return(-1);
    }
    cur+=sizeof(float);
    if(jmagine_utils_read_float_little_endian(contents, cur, y))
    {
      LOGE("Failed to read from file '%s'.", path);
      free(contents);
      return(-1);
    }
    cur+=sizeof(float);
    if(jmagine_utils_read_float_little_endian(contents, cur, size))
    {
      LOGE("Failed to read from file '%s'.", path);
      free(contents);
      return(-1);
    }
    cur+=sizeof(float);
    if(jmagine_utils_read_float_little_endian(contents, cur, angle))
    {
      LOGE("Failed to read from file '%s'.", path);
      free(contents);
      return(-1);
    }
    cur+=sizeof(float);
    if(jmagine_utils_read_float_little_endian(contents, cur, response))
    {
      LOGE("Failed to read from file '%s'.", path);
      free(contents);
      return(-1);
    }
    cur+=sizeof(float);
    if(jmagine_utils_read_int_little_endian(contents, cur, octave))
    {
      LOGE("Failed to read from file '%s'.", path);
      free(contents);
      return(-1);
    }
    cur+=sizeof(int);
    if(jmagine_utils_read_int_little_endian(contents, cur, class_id))
    {
      LOGE("Failed to read from file '%s'.", path);
      free(contents);
      return(-1);
    }
    cur+=sizeof(int);
    dest.keypoints[i]=KeyPoint(x, y, size, angle, response, octave, class_id);
  }

  // Descriptors.
  if(jmagine_utils_read_int_little_endian(contents, cur, desc_nb))
  {
    LOGE("Failed to read from file '%s'.", path);
    free(contents);
    return(-1);
  }
  cur+=sizeof(int);
  if(jmagine_utils_read_int_little_endian(contents, cur, desc_len))
  {
    LOGE("Failed to read from file '%s'.", path);
    free(contents);
    return(-1);
  }
  cur+=sizeof(int);
  Mat desc;
  if(dest.algorithm_id==JMAGINE_ALGORITHM_FAST_FREAK)
    desc=Mat(desc_nb, desc_len, CV_8U);
  else
    desc=Mat(desc_nb, desc_len, CV_32F);
  for(i=0; i<desc_nb; ++i)
  {
    for(j=0; j<desc_len; ++j)
    {
      if(dest.algorithm_id==JMAGINE_ALGORITHM_FAST_FREAK)
      {
        if(cur>=file_len)
        {
          LOGE("Failed to read from file '%s'.", path);
          free(contents);
          return(-1);
        }
        desc.at<unsigned char>(i,j)=contents[cur];
        ++cur;
      }
      else
      {
        if(jmagine_utils_read_float_little_endian(contents, cur, data_float))
        {
          LOGE("Failed to read from file '%s'.", path);
          free(contents);
          return(-1);
        }
        cur+=sizeof(float);
        desc.at<float>(i,j)=data_float;
      }
    }
  }
  dest.descriptors=desc.clone();
  desc.release();

  if(cur!=file_len)
  {
    LOGE("Could not load file: length sanity check failed.\n");
    free(contents);
    return(-1);
  }

#ifdef TKDV_DEBUG
  LOGD("Loaded trigger file '%s' successfully. Name: '%s'. Resolution: %lu*%lupx. Element correspondance ID: %d.", path, dest.name.c_str(), dest.len_x, dest.len_y, dest.element_id);
  LOGD("  :: Algorithm: %d. Keypoints: %lu. Descriptors: %d*%dB.", dest.algorithm_id, dest.keypoints.size(), dest.descriptors.rows, dest.descriptors.cols);
#endif

  free(contents);
  return(0);
}




// From an opencv file memory format (see jmagine_data_* for examples), this extracts a usable
// vector of KeyPoint into dst.
// Returns 0 on success, something else on failure.
int jmagine_utils_mem2vec_kps(const string& src, const string& name, vector<KeyPoint>& dst)
{
  FileStorage fstorage;
  FileNode fnode;

  if(!fstorage.open(src.c_str(), FileStorage::MEMORY+FileStorage::READ))
    return(1);
  fnode=fstorage[name];
  read(fnode, dst);
  if(dst.size()==0)
    return(2);
  fstorage.release();

  return(0);
}




// From an opencv file memory format (see jmagine_data_* for examples), this extracts a usable
// size_t value into *dst.
// Returns 0 on success, something else on failure.
int jmagine_utils_mem2ulong(const string& src, const string& name, size_t* dst)
{
  FileStorage fstorage;
  FileNode fnode;

  if(!fstorage.open(src.c_str(), FileStorage::MEMORY+FileStorage::READ))
    return(1);
  *dst=size_t((int)(fstorage[name]));
  fstorage.release();

  return(0);
}




// Get a random color from the specified random number generator.
Scalar jmagine_utils_random_color(RNG& rng)
{
  int rnum=(unsigned)(rng);
  return(Scalar(rnum&255, (rnum>>8)&255, (rnum>>16)&255));
}




// Returns the length of the line between the two specified points.
double jmagine_utils_get_length_line(const cv::Point& a, const cv::Point& b)
{
  int len_x, len_y;
  len_x=jmagine_utils_get_absolute_diff(a.x, b.x);
  len_y=jmagine_utils_get_absolute_diff(a.y, b.y);

  return(sqrt(double(len_x*len_x)+double(len_y*len_y)));
}





// Returns the 'angle' of the line between the two specified points.
// That is, the angle between the horizon and the line.
// The angle is returned in degrees, in absolute value.
double jmagine_utils_get_angle_line(const cv::Point& a, const cv::Point& b)
{
  int len_x, len_y;
  double len_line;
  double cos_angle, angle;

  len_x=jmagine_utils_get_absolute_diff(a.x, b.x);
  len_y=jmagine_utils_get_absolute_diff(a.y, b.y);
  len_line=sqrt(double(len_x*len_x)+double(len_y*len_y));

  cos_angle=double(len_x)/len_line;
  angle=abs(jmagine_utils_rad2deg(acos(cos_angle)));

  return(angle);
}




// Returns an angle in radians from an angle in degrees.
double jmagine_utils_deg2rad(double deg)
{
  return(deg/(180./CV_PI));
}




// Returns an angle in degrees from an angle in radian.
double jmagine_utils_rad2deg(double radian)
{
  return(radian*(180./CV_PI));
}




// Returns the cosine of the angle between the two vectors
// pt0->pt1 and pt0->pt2.
double jmagine_utils_angle_vectors(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
  double dx1=pt1.x-pt0.x;
  double dy1=pt1.y-pt0.y;
  double dx2=pt2.x-pt0.x;
  double dy2=pt2.y-pt0.y;

  return((dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2)+1e-10));
}




// Returns the absolute difference between a and b, even if a is negative, b too, etc.
int jmagine_utils_get_absolute_diff(int a, int b)
{
  int val1=a, val2=b;
  int delta1=0, delta2=0;

  if((val1<0) && (val2<0))
  {
    if(val1<val2)
      return(abs(val1+abs(val2)));
    if(val2<=val1)
      return(abs(val2+abs(val1)));
  }

  if(val1<0)
  {
    delta1=abs(val1);
    val1=0;
  }
  if(val2<0)
  {
    delta2=abs(val2);
    val2=0;
  }

  return(delta2+delta1+abs(val2-val1));
}




// Returns the absolute difference between floats a and b, even if a is negative, b too, etc.
double jmagine_utils_get_absolute_diff_float(double a, double b)
{
  double val1=a, val2=b;
  double delta1=0, delta2=0;

  if((val1<0) && (val2<0))
  {
    if(val1<val2)
      return(fabs(val1+fabs(val2)));
    if(val2<=val1)
      return(fabs(val2+fabs(val1)));
  }

  if(val1<0)
  {
    delta1=fabs(val1);
    val1=0.;
  }
  if(val2<0)
  {
    delta2=fabs(val2);
    val2=0.;
  }

  return(delta2+delta1+fabs(val2-val1));
}




// Normalize src and put the result into dst.
// For now, this performs a simple histogram equalization.
// Returns 0 on success, something else on failure.
int jmagine_utils_normalize_img(const Mat& src, Mat& dst)
{
  equalizeHist(src, dst);
  return(0);
}




// This goes through the specified list of matches and returns the minimum and maximum distance found respectively in
// *min_dist and *max_dist.
// min_dist and max_dist can be NULL.
// Returns 0 on success, -1 on failure. An empty matches list is a failure.
int jmagine_utils_get_min_max_dist_matches(const vector<DMatch>& matches, double* min_dist, double* max_dist)
{
  size_t i;
  double min=4242424;
  double max=-4242424;
  double dist;

  if((min_dist==NULL) && (max_dist==NULL))
    return(-1);
  if(matches.size()==0)
    return(-1);

  for(i=0; i<matches.size(); ++i)
  {
    dist=matches[i].distance;
    if(dist<min)
      min=dist;
    if(dist>max)
      max=dist;
  }

  if(min_dist!=NULL)
    *min_dist=min;
  if(max_dist!=NULL)
    *max_dist=max;

  return(0);
}




// Put the n top matches (in terms of distance) found in the specified matches vector into top_matches.
// Returns 0 on success, -1 on failure.
// Having nb_top matches or less than that is a failure.
int jmagine_utils_get_top_matches(const vector<DMatch>& matches, vector<DMatch>& top_matches, unsigned int nb_top)
{
  size_t i, j, k;
  vector<double> top_distances;
  vector<int> top_distances_idx;
  double dist;
  unsigned int nb_found=0;

  if((matches.size()<=nb_top) || (nb_top<=0))
    return(-1);

  top_distances.resize(nb_top);
  top_distances_idx.resize(nb_top);
  for(i=0; i<top_distances.size(); ++i)
  {
    top_distances[i]=4242424.;
    top_distances_idx[i]=-1;
  }
  for(i=0; i<matches.size(); ++i)
  {
    dist=matches[i].distance;
    for(j=0; j<top_distances.size(); ++j)
    {
      if(dist<top_distances[j])
      {
        for(k=(top_distances.size()-1); k>j; --k)
        {
          top_distances[k]=top_distances[k-1];
          top_distances_idx[k]=top_distances_idx[k-1];
        }
        top_distances[j]=dist;
        top_distances_idx[j]=i;
        ++nb_found;
        break;
      }
    }
  }

  if(nb_found<nb_top)   // Should be impossible.
    return(-1);

  for(i=0; i<top_distances_idx.size(); ++i)
    top_matches.push_back(matches[top_distances_idx[i]]);

  return(0);
}




// This function will read a short value stored as little-endian from the specified buffer at the specified
// position, and put said value into the specified short (as either little endian or big endian depending on
// the current arch).
// Returns 0 on success, -1 on failure.
int jmagine_utils_read_short_little_endian(const char* buffer, int pos, short& value)
{
  char data_short[sizeof(short)];
  size_t len=sizeof(short);
  short* res;
  int i;

  if(jmagine_utils_is_big_endian())
  {
    for(i=len-1; i>=0; --i)
      data_short[len-i-1]=buffer[pos+i];
  }
  else
  {
    for(i=0; i<int(len); ++i)
      data_short[i]=buffer[pos+i];
  }
  res=((short*)(data_short));
  value=*res;

  return(0);
}




// This function will read an int value stored as little-endian from the specified buffer at the specified
// position, and put said value into the specified int (as either little endian or big endian depending on
// the current arch).
// Returns 0 on success, -1 on failure.
int jmagine_utils_read_int_little_endian(const char* buffer, int pos, int& value)
{
  char data_int[sizeof(int)];
  size_t len=sizeof(int);
  int* res;
  int i;

  if(jmagine_utils_is_big_endian())
  {
    for(i=len-1; i>=0; --i)
      data_int[len-i-1]=buffer[pos+i];
  }
  else
  {
    for(i=0; i<int(len); ++i)
      data_int[i]=buffer[pos+i];
  }
  res=((int*)(data_int));
  value=*res;

  return(0);
}




// This function will read a float value stored as little-endian from the specified buffer at the specified
// position, and put said value into the specified float (as either little endian or big endian depending on
// the current arch).
// Returns 0 on success, -1 on failure.
int jmagine_utils_read_float_little_endian(const char* buffer, int pos, float& value)
{
  char data_float[sizeof(float)];
  size_t len=sizeof(float);
  float* res;
  int i;

  if(jmagine_utils_is_big_endian())
  {
    for(i=len-1; i>=0; --i)
      data_float[len-i-1]=buffer[pos+i];
  }
  else
  {
    for(i=0; i<int(len); ++i)
      data_float[i]=buffer[pos+i];
  }
  res=((float*)(data_float));
  value=*res;

  return(0);
}




// Returns 1 if the current arch is big-endian, 0 if it is not.
// This makes use of the jmagine_order32.hpp header.
int jmagine_utils_is_big_endian()
{
  if(SNS_O32_HOST_ORDER==SNS_O32_BIG_ENDIAN)
    return(1);
  return(0);
}




// This function will check wether or not the length of the standard data types on the
// current platform are as expected. It will return 0 if that's the case.
// It should be noted that most (if not all) the binary loading code is already compliant
// with archs that have different values for those; however, this has not been tested
// at all. The aim of this function is to make sure we don't run on those archs (if there are
// actually any) until proper testing has been done.
int jmagine_utils_check_supported_len_datatypes()
{
  if(sizeof(char)!=1)
    return(-1);
  if(sizeof(int)!=4)
    return(-1);
  if(sizeof(short)!=2)
    return(-1);
  if(sizeof(double)!=8)
    return(-1);
  if(sizeof(float)!=4)
    return(-1);
  return(0);
}




// This function will check wether or not the current arch is supported in terms of endianness.
// It will return 0 if it is. Currently, little-endian and big-endian archs are supported (those
// should be the only ones encountered given the target platforms anyway).
int jmagine_utils_check_supported_endianness()
{
  if(SNS_O32_HOST_ORDER==SNS_O32_LITTLE_ENDIAN)
    return(0);
  if(SNS_O32_HOST_ORDER==SNS_O32_BIG_ENDIAN)
    return(0);
  return(-1);
}
