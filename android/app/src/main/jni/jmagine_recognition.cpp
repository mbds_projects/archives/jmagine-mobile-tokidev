/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_recognition.hpp - Header recognition module.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine_recognition.hpp"
#include "jmagine_parameters.hpp"
#include "jmagine_utils.hpp"
#include "jmagine_data_elements.hpp"
#ifdef TKDV_DEBUG
#include "jmagine_debug.hpp"
#endif



// Initializes a query object once its img_color is filled.
// This will generate various images (grayscale, cannied, etc.) used throughout the analysis.
void jmagine_init_query(jmagine_query_t& query);




// Initializes various static data (triggers, etc...) into the specified handle.
// base_path needs to be set to the path where the triggers can be found.
// db_id should be a valid triggers database ID.
// This calls the various initialization routines in other modules.
// Returns 0 on success, -1 on failure.
// This should typically be called only once, at the start of the app.
int jmagine_init(jmagine_handle_t& handle, const char* base_path, int db_id)
{
  int rv=0;

  if(handle.initialized)
  {
#ifdef TKDV_DEBUG
    LOGE("Initialization failed: the handle is already initialized.");
#else
    LOGE("Error occured during analysis initialization (#EG00).");
#endif
    return(-1);
  }

  if((rv=jmagine_features_init(handle.features, base_path, db_id))!=0)
  {
#ifdef TKDV_DEBUG
    LOGE("Initialization failed: features module initialization failed (rv %d).", rv);
#else
    LOGE("Error occured during analysis initialization (#EG03-%d).", rv);
#endif
    return(rv);
  }

  handle.initialized=true;
#ifdef TKDV_DEBUG
  LOGD("Initialized recognition modules successfully.");
#endif

  return(0);
}




// Main recognition function: call with a *color* query image.
// This will fill the jmagine_query_t object with lots of information about the image (matched triggers, etc.).
// Returns 0 on success, -1 on failure.
int jmagine_analyze_image(jmagine_handle_t& hnd, const Mat& query_img, jmagine_query_t& result)
{
  jmagine_query_t query;
  query.img_color=&query_img;
  int rv=0;

  if(!hnd.initialized)
  {
    if(jmagine_init(hnd, "", 0))
      return(-1);
  }

  // Set up various images needed by all the algorithms.
#ifdef TKDV_DEBUG
  LOGD(" ");
  LOGD("Generating gray, blurred and cannied images...");
  double t=(double)getTickCount();
#endif
  jmagine_init_query(query);
#ifdef TKDV_DEBUG
  t=((double)getTickCount() - t)/getTickFrequency();
  LOGD("Done, time taken: %.04f seconds.", t);
#endif

  // Finally, perform features-based image matching.
#ifdef TKDV_DEBUG
  LOGD(" ");
  LOGD("Performing features-based analysis...");
  t=(double)getTickCount();
#endif
  rv=jmagine_features_detection(hnd.features, query.img_gray, query.probable_matches);
  if(rv)
  {
#ifdef TKDV_DEBUG
    LOGE("Error during features-based analysis, exiting.");
#else
    LOGE("Error occured during image analysis (#EF00).");
#endif
    return(-4);
  }
#ifdef TKDV_DEBUG
  t=((double)getTickCount() - t)/getTickFrequency();
  LOGD("Done, time taken: %.04f seconds.", t);
  int nb=query.probable_matches.size();
  LOGD("Got %d probable matches%c", nb, nb>0?':':'.');
  jmagine_debug_output_probable_matches(hnd, query.probable_matches, "  ");
#ifdef TKDV_DESKTOP
  LOGD(" ");
  LOGD("Displaying features match candidates. Legend:");
  const string lcolors[3]={ "green", "blue", "red" };
  for(size_t s=0; s<query.probable_matches.size(); ++s)
  {
    string lcolor=lcolors[s%3];
    LOGD("  - Candidate #%lu (%s/%d) is %s.", s+1, hnd.features.triggers[query.probable_matches[s].algorithm_id][query.probable_matches[s].trigger_id].name.c_str(),
         hnd.features.triggers[query.probable_matches[s].algorithm_id][query.probable_matches[s].trigger_id].element_id, lcolor.c_str());
  }
  jmagine_debug_show_probable_matches(query.img_color, hnd, query.probable_matches, "features match candidates", false);
#endif
#endif

  result=query;
  return(0);
}




// Initializes a query object once its img_color is filled.
// This will generate various images (grayscale, cannied, etc.) used throughout the analysis.
void jmagine_init_query(jmagine_query_t& query)
{
  Mat pyrscaled;

  cvtColor((*(query.img_color)), query.img_gray, CV_BGR2GRAY);
  if(JMAGINE_GENERAL_BLUR_MODE==1)  // Use upscale/downscale.
  {
    pyrDown(query.img_gray, pyrscaled, cv::Size(query.img_gray.cols/2, query.img_gray.rows/2));
    pyrUp(pyrscaled, query.img_blurred, query.img_gray.size());
    pyrscaled.release();
  }
  else // Use gaussian.
  {
    GaussianBlur(query.img_gray, query.img_blurred, cv::Size(JMAGINE_GENERAL_BLUR_KERNEL, JMAGINE_GENERAL_BLUR_KERNEL), 0, 0,
                 BORDER_DEFAULT);
  }

  Canny(query.img_blurred, query.img_cannied, JMAGINE_CANNY_LOW_THRESH, JMAGINE_CANNY_HIGH_THRESH, 3);
}




// Main matching function. From the query object obtained through jmagine_analyze_image, this will apply various
// rules (IF possible match of trigger there AND triangle detected of color red, THEN add score, etc.).
// It returns a list of likely match (hopefully only one).
int jmagine_match_image(jmagine_handle_t& hnd, const jmagine_query_t& result, vector<jmagine_reco_candidate_t>& candidates)
{
  size_t i, j;
  double score;
  vector<double> matches_scores(result.probable_matches.size());
  vector<vector<double> > candidates_matched(JMAGINE_NB_ELMTS);
  vector<double> candidates_scores(JMAGINE_NB_ELMTS);

  for(i=0; i<JMAGINE_NB_ELMTS; ++i)
    candidates_scores[i]=-1.;

  if(result.probable_matches.size()==0)
    return(0);
  matches_scores.resize(result.probable_matches.size());

  // For each probable trigger match, generate an adjusted score (between 1 and usually 3, the higher the better,
  // instead of the lower the better like up 'till now.
  // We'll also apply various steps to adjust the score if necessary.
  for(i=0; i<result.probable_matches.size(); ++i)
  {
    if(result.probable_matches[i].score==0)   // Theorically possibly, just very unlikely.
      score=3.;
    else
      score=1./(result.probable_matches[i].score/100.);
#ifdef TKDV_DEBUG
    LOGD("Candidate #%lu: algorithm 0x%02X, adjusted score before correction loop: %.02f.", i+1, result.probable_matches[i].algorithm_id, score);
#endif

    // First of all, if enabled, we'll increase the score of the matches according to their number of matched
    // features after filtering.
    if(JMAGINE_MATCHING_USE_NB_MATCHES)
      score+=(result.probable_matches[i].nb_matches*JMAGINE_MATCHING_NB_MATCHES_BONUS);
    //    LOGD("Candidate #%lu: adjusted score after nb_matches=%.02f.", i+1, score);
    // Then, if enabled and if homography filtering is enabled and we have the topology, we'll increase the score
    // of the matches that seems to produce a coherent large shape, because those are most likely to be strong
    // matches.
    if(JMAGINE_MATCHING_USE_TOPOLOGY)
    {
      // We use the homography filtering parameter of the first algorithm right now - because it's the "leading" one, that will
      // be influenced by the others. This is pending a better multi-detector implem (one that truly combines all algorithms even
      // at the current step).
      if((JMAGINE_FEATURES_HOMOGRAPHY_FILTERING[0]) && (JMAGINE_FEATURES_HOMOGRAPHY_STORE_TOPOLOGY))
      {
        if(result.probable_matches[i].shape_area>JMAGINE_MATCHING_TOPOLOGY_MIN_AREA)
        {
#ifdef TKDV_DEBUG
          LOGD("## Candidate %lu got a match topology bonus.", i+1);
#endif
          score+=JMAGINE_MATCHING_TOPOLOGY_BONUS;
        }
        if(JMAGINE_MATCHING_USE_TOPOLOGY_RECT)
        {
          // Four faces and rectangle score increase enabled: we'll increase the score slightly if the rectangle
          // is not too irregular.
          if((result.probable_matches[i].shape_nb_faces==4) &&
             (result.probable_matches[i].shape_irregularity<JMAGINE_MATCHING_TOPOLOGY_RECT_MAX_IRREGULARITY))
          {
#ifdef TKDV_DEBUG
            LOGD("## Candidate %lu got a match topology/rect bonus.", i+1);
#endif
            score+=JMAGINE_MATCHING_TOPOLOGY_RECT_BONUS;
          }
        }
      }
    }

#ifdef TKDV_DEBUG
    LOGD("Candidate #%lu: adjusted score after correction loop: %.02f.", i+1, score);
#endif

    matches_scores[i]=score;
  }


  // Now, if enabled, we'll go through the previous candidates at this stage. If there are common candidates, they
  // get a score increase.
  // We'll only do that if no more than n seconds have passed since the last image.
  if(JMAGINE_MATCHING_USE_PREVIOUS_MATCHES)
  {
    if(hnd.last_tick_probable_match==-1.)
    {
      hnd.last_tick_probable_match=(double)getTickCount();
      hnd.previous_empty=false;
    }
    else
    {
      double nb_seconds=((double)getTickCount()-hnd.last_tick_probable_match)/getTickFrequency();
      hnd.last_tick_probable_match=(double)getTickCount();
      if(nb_seconds<=JMAGINE_MATCHING_PREVIOUS_MAX_SECONDS)
      {
        for(i=0; i<hnd.previous_probable_matches.size(); ++i)
        {
          for(j=0; j<result.probable_matches.size(); ++j)
          {
            // TODO: FIX THIS BUG (USING TRIGGER ID INSTEAD OF ELEMENT ID). AS WELL, ALLOW TO OPTIONALLY USE TRIGGER ID INSTEAD ?
            // Done - no option yet.
            //            if(result.probable_matches[j].trigger_id==hnd.previous_probable_matches[i].trigger_id)
            if(result.probable_matches[j].element_id==hnd.previous_probable_matches[i].element_id)
            {
              matches_scores[j]+=JMAGINE_MATCHING_PREVIOUS_BONUS;
            }
          }
        }
      }
      else
      {
        hnd.previous_empty=false;
      }
    }
    hnd.previous_probable_matches.clear();
    hnd.previous_probable_matches=result.probable_matches;
  }

  // Now, there are a few elements that have two triggers. Accordingly, we'll now push the matches by their
  // real elements in a vector to detect this and provide score increases to those below.
  for(i=0; i<result.probable_matches.size(); ++i)
    candidates_matched[result.probable_matches[i].element_id].push_back(matches_scores[i]);

  // Check wether or not there are more than two matches for the same trigger.
  for(i=0; i<JMAGINE_NB_ELMTS; ++i)
  {
    if(candidates_matched[i].size()>1)
    {
      if(candidates_matched[i][0]>=candidates_matched[i][1])
        candidates_scores[i]=candidates_matched[i][0]+(candidates_matched[i][1]*JMAGINE_MATCHING_DOUBLETRIG_BONUS);
      else
        candidates_scores[i]=candidates_matched[i][1]+(candidates_matched[i][0]*JMAGINE_MATCHING_DOUBLETRIG_BONUS);
    }
    else if(candidates_matched[i].size()==1)
      candidates_scores[i]=candidates_matched[i][0];
  }

  // We finally have a list of all likely matched elements, and a score for each.
  for(i=0; i<JMAGINE_NB_ELMTS; ++i)
  {
    if(candidates_scores[i]==-1.)
      continue;
    jmagine_reco_candidate_t ncand;
    ncand.score=candidates_scores[i];
    ncand.name=JMAGINE_ELMT2RV[i][JMAGINE_NAME_FRIENDLY];
    ncand.element_id=i;
    ncand.id=JMAGINE_ELMT2RV[i][JMAGINE_NAME_ID];
    candidates.push_back(ncand);
  }

  return(0);
}
