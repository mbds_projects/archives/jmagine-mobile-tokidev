/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_data_elements.hpp - Data / Elements to be recognized.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_DATA_ELEMENTS_HPP
#define __TOKIDEV_JMAGINE_DATA_ELEMENTS_HPP

#include "jmagine_parameters.hpp"

#define JMAGINE_NAME_FRIENDLY   0
#define JMAGINE_NAME_ID  1

#define JMAGINE_NB_ELMTS   10

//                                                  DEBUG_NAME, APP_ELEMENT_CODE    |   ELMT_ID (IN TRIGGER BINARY FILE)
const string JMAGINE_ELMT2RV[JMAGINE_NB_ELMTS][2]={ // Old Nice ==>
                                                    {"11_lou_fran_calin", "11_lou_fran_calin"},          // 0x00 / 0 - Lou Fran Calin.
                                                    {"10_da_boutau", "10_da_boutau"},                    // 0x01 / 1 - Da Bouttau.
                                                    {"06_saleya_matisse", "06_saleya_matisse"},          // 0x02 / 2 - Palais Cais de Pierlas.
                                                    {"01_nietzsche", "01_nietzsche"},                    // 0x03 / 3 - Residence nietzsche ? pres de beaurivage.
                                                    {"02_hotel_beaurivage", "02_hotel_beaurivage"},      // 0x04 / 4 - Hotel Beaurivage.
                                                    {"03_biblio_apollinaire", "03_biblio_apollinaire"},  // 0x05 / 5 - Palais Hongran.
                                                    {"05_aragon_triolet", "05_aragon_triolet"},          // 0x06 / 6 - Demeure d'Aragon et Elsa Triolet.
                                                    {"08_paganini", "08_paganini"},                      // 0x07 / 7 - Demeure de Paganini.
                                                    {"09_alexandre_dumas", "09_alexandre_dumas"},        // 0x08 / 8 - Demeure d'Alexandre Dumas ? Place du Palais.
                                                    {"13_cafe_turin", "13_cafe_turin"} };                // 0x09 / 9 - Café Turin.
                                                    // <== Old Nice.


//                                                   LAT, LON    |   ELMT_ID (IN TRIGGER BINARY FILE)
const double JMAGINE_ELMT2GPS[JMAGINE_NB_ELMTS][2]={ // Old Nice ==>
                                                    {43.697327, 7.275812},            // 0x00 / 0 - Lou Fran Calin.
                                                    {43.697677, 7.276024},            // 0x01 / 1 - Da Bouttau.
                                                    {43.695369, 7.277011},            // 0x02 / 2 - Palais Cais de Pierlas.
                                                    {43.695733, 7.269229},            // 0x03 / 3 - Residence nietzsche ? pres de beaurivage.
                                                    {43.695663, 7.269975},            // 0x04 / 4 - Hotel Beaurivage.
                                                    {43.695680, 7.273182},            // 0x05 / 5 - Palais Hongran.
                                                    {43.695350, 7.274778},            // 0x06 / 6 - Demeure d'Aragon et Elsa Triolet.
                                                    {43.696437, 7.275734},            // 0x07 / 7 - Demeure de Paganini.
                                                    {43.696916, 7.273964},            // 0x08 / 8 - Demeure d'Alexandre Dumas ? Place du Palais.
                                                    {43.700603, 7.279433} };          // 0x09 / 9 - Café Turin.
                                                    // <== Old Nice.


#endif //__TOKIDEV_JMAGINE_DATA_ELEMENTS_HPP
