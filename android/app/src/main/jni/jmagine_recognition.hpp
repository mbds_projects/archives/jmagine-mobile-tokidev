/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_recognition.hpp - Header recognition module.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_RECOGNITION_HPP
#define __TOKIDEV_JMAGINE_RECOGNITION_HPP

#include <cstring>
#include <cstdio>


#ifdef TKDV_ANDROID
#include "jmagine_android.hpp"
#else
#include "jmagine.hpp"
#endif
#include "jmagine_features.hpp"



// Defines a library handle. This contains various static data (triggers, color histograms, etc.) that
// are used throughout the analysis. Ideally, the embedded app should only create one handle object,
// and initialize it only once (for example at the start of the app), because this structure is also
// used to keep track of frame-to-frame stuff.
struct jmagine_handle_t
{
  bool initialized;
  jmagine_features_handle_t features;

  vector<jmagine_candidate_match_t> previous_probable_matches;
  double last_tick_probable_match;
  bool previous_empty;

  jmagine_handle_t()
  {
    initialized=false;
    last_tick_probable_match=-1.;
    previous_empty=false;
  }
};



// Defines a query image.
struct jmagine_query_t
{
  const Mat* img_color;   // Original image, color.
  Mat img_gray;   // Original image, grayscaled.
  // The gray image either gaussian blurred with a 3,3 kernel or
  // blurred through downscale/upscale (see CONTOURS_GENERAL_BLUR_MODE).
  // Used for contours and histogram analysis.
  Mat img_blurred;
  Mat img_cannied;   // img_blurred, cannied.

  vector<jmagine_candidate_match_t> probable_matches;
};


// A possible last-step match (right after final matching stuff).
struct jmagine_reco_candidate_t
{
  string name;  // friendly name of the recognized element.
  string id;  // id of the recognized element.
  double score;   // score indicating the likelyhood of match.
  int element_id;
  int trigger_id;
};


// Initializes various static data (triggers, etc...) into the specified handle.
// base_path needs to be set to the path where the triggers can be found.
// db_id should be a valid triggers database ID.
// This calls the various initialization routines in other modules.
// Returns 0 on success, -1 on failure.
// This should typically be called only once, at the start of the app.
int jmagine_init(jmagine_handle_t& handle, const char* base_path, int db_id);

// Main recognition function: call with a *color* query image.
// This will fill the jmagine_query_t object with lots of information about the image (matched triggers, etc.).
// Returns 0 on success, -1 on failure.
int jmagine_analyze_image(jmagine_handle_t& hnd, const Mat& query_img, jmagine_query_t& result);

// Main matching function. From the query object obtained through jmagine_analyze_image, this will apply various
// rules (IF possible match of trigger there AND triangle detected of color red, THEN add score, etc.).
// It returns a list of likely match (hopefully only one).
int jmagine_match_image(jmagine_handle_t& hnd, const jmagine_query_t& result, vector<jmagine_reco_candidate_t>& candidates);

// Initializes a query object once its img_color is filled.
// This will generate various images (grayscale, cannied, etc.) used throughout the analysis.
void jmagine_init_query(jmagine_query_t& query);



#endif //__TOKIDEV_JMAGINE_RECOGNITION_HPP
