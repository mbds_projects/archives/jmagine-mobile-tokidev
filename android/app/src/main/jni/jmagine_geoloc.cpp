/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_geoloc.cpp - Geoloc module (right now, only coords distance calculations and so on).
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine_utils.hpp"
#include "jmagine_geoloc.hpp"



// Returns the distance, in meters, between the two specified points. Uses the haversine formula.
// Returns -1 on error.
double jmagine_get_dist_gps_coords_meters(double lat_1, double lon_1, double lat_2, double lon_2)
{
  double lat_1_rad, lat_2_rad;
  double sin_delta_lat, sin_delta_lon;
  double delta_lat, delta_lon;
  double hav_a;
  double res;

  if((lat_1>90.) || (lat_2>90.) || (lat_1<-90.) || (lat_2<-90.))
    return(-1.);
  if((lon_1>180.) || (lon_2>180.) || (lon_1<-180.) || (lon_2<-180.))
    return(-1.);

  lat_1_rad=jmagine_utils_deg2rad(lat_1);
  lat_2_rad=jmagine_utils_deg2rad(lat_2);
  delta_lat=jmagine_utils_deg2rad(lat_2-lat_1);
  delta_lon=jmagine_utils_deg2rad(lon_2-lon_1);
  sin_delta_lat=sin(delta_lat/2.);
  sin_delta_lon=sin(delta_lon/2.);
  
  hav_a=(sin_delta_lat*sin_delta_lat)+(sin_delta_lon*sin_delta_lon*cos(lat_1_rad)*cos(lat_2_rad));
  res=2.*atan2(sqrt(hav_a), sqrt(1-hav_a));
  res=JMAGINE_EARTH_RADIUS*res;

  if(res<0.)
    res=res*-1;

  return(res);
}
