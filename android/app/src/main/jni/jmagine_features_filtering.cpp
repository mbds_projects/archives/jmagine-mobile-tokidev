/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_features_filtering.hpp - Header features filtering module. Module providing filtering
                                   funtions for descriptors (features) matches.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine_features_filtering.hpp"
#include "jmagine_utils.hpp"
#include "jmagine_parameters.hpp"




// From a knn match to the library of triggers (for example the result of a general match on the triggers flann index),
// this provides nb_best (or less) best candidates from the triggers.
// This function simply select the n matches that have the most matches occurences.
// Returns 0 on success, -1 on failure.
int jmagine_features_filter_top_candidates_knn_occurences(jmagine_features_handle_t& hnd,
                                                          const vector<vector<DMatch> >& matches,
                                                          vector<jmagine_candidate_match_t>& candidates,
                                                          unsigned int nb_best, int algorithm_id)
{
  size_t i, j, k;
  unsigned int id;
  // nb_occurences[i] contains the number of times a keypoint from trigger[i] has been matched.
  vector<int> nb_occurences(hnd.triggers[algorithm_id].size());
  vector<vector<double> > distances(hnd.triggers[algorithm_id].size());
  // scores[i] contains the means of the distances for matched keypoints from trigger[i].
  vector<double> scores(hnd.triggers[algorithm_id].size());
  vector<int> best_candidates;
  vector<int> best_candidates_idx;
  unsigned int nb_best_found=0;
  double score;

  if(nb_best<=0)
    return(-1);
  best_candidates.resize(nb_best);
  best_candidates_idx.resize(nb_best);

  for(i=0; i<nb_best; ++i)
  {
    best_candidates[i]=-1;
    best_candidates_idx[i]=-1;
  }
  for(i=0; i<nb_occurences.size(); ++i)
    nb_occurences[i]=0;

  for(i=0; i<matches.size(); ++i)  // For each query keypoint group of matches.
  {
    for(j=0; j<matches[i].size(); ++j)  // For each match of the group.
    {
      id=matches[i][j].imgIdx;
      if(id>=hnd.triggers[algorithm_id].size()) // Should never happend.
      {
        LOGE("Filtering: triggers database size constraint failure.");
        return(-1);
      }
      nb_occurences[id]+=1;
      distances[id].push_back(matches[i][j].distance);
    }
  }

  // Now, get the n best.
  for(i=0; i<nb_occurences.size(); ++i)
  {
    for(j=0; j<nb_best; ++j)
    {
      if(nb_occurences[i]>best_candidates[j])
      {
        for(k=nb_best-1; k>j; --k)
        {
          best_candidates[k]=best_candidates[k-1];
          best_candidates_idx[k]=best_candidates_idx[k-1];
        }
        best_candidates[j]=nb_occurences[i];
        best_candidates_idx[j]=i;
        ++nb_best_found;
        break;
      }
    }
  }

  if(nb_best_found>nb_best)
    nb_best_found=nb_best;
  // And put them into the final vector.
  for(i=0; i<nb_best_found; ++i)
  {
    jmagine_candidate_match_t nmatch;
    if(best_candidates_idx[i]==-1)   // Should never happend.
      continue;
    nmatch.trigger_id=best_candidates_idx[i];
    nmatch.algorithm_id=algorithm_id;
    nmatch.element_id=hnd.triggers[algorithm_id][nmatch.trigger_id].element_id;
    nmatch.nb_matches=best_candidates[i];
    score=0;
    for(j=0; j<distances[best_candidates_idx[i]].size(); ++j)
      score+=distances[best_candidates_idx[i]][j];
    score=score/float(best_candidates[i]);
    nmatch.score=score;
    candidates.push_back(nmatch);
  }

  distances.clear();
  return(0);
}




// From a knn match to the library of triggers (for example the result of a general match on the triggers flann index),
// this provides nb_best (or less) best candidates from the triggers.
// This function starts by selecting the n top matches for each of the triggers. If there are enough of those, it will
// compute the mean of the distances to select the first t candidates.
// Then, it goes through the rest and simply filter by occurences.
// Returns 0 on success, -1 on failure.
int jmagine_features_filter_top_candidates_knn_score(jmagine_features_handle_t& hnd,
                                                     const vector<vector<DMatch> >& matches,
                                                     vector<jmagine_candidate_match_t>& candidates,
                                                     unsigned int nb_best, int algorithm_id)
{
  size_t i, j, k;
  size_t id;
  vector<vector<double> > distances_per_trigger(hnd.triggers[algorithm_id].size());
  vector<double> mean_dist_per_trigger(hnd.triggers[algorithm_id].size());
  vector<int> nb_matches_used_for_mean(hnd.triggers[algorithm_id].size());
  vector<bool> dist_selected(hnd.triggers[algorithm_id].size());
  vector<double> best_dist;
  vector<int> best_dist_idx;
  vector<int> best_occ;
  vector<int> best_occ_idx;
  size_t nb_best_dist=0, nb_best_occ=0;
  double score;
  size_t nb_candidates=0;
  double min_all_dists=4242424.;
  double max_all_dists=-4242424.;
  double total, dist_thr=-1.;

  if(nb_best<=0)
    return(-1);
  best_dist.resize(nb_best);
  best_occ.resize(nb_best);
  best_dist_idx.resize(nb_best);
  best_occ_idx.resize(nb_best);

  for(i=0; i<nb_best; ++i)
  {
    best_dist[i]=4242424.;
    best_dist_idx[i]=-1;
    best_occ[i]=-1;
    best_occ_idx[i]=-1;
  }
  for(i=0; i<hnd.triggers[algorithm_id].size(); ++i)
  {
    dist_selected[i]=false;
    mean_dist_per_trigger[i]=-1.;
    nb_matches_used_for_mean[i]=-1.;
  }

  // First, get all the distances for each trigger.
  for(i=0; i<matches.size(); ++i)  // For each query keypoint group of matches.
  {
    for(j=0; j<matches[i].size(); ++j)  // For each match of the group.
    {
      id=matches[i][j].imgIdx;
      if(id>=hnd.triggers[algorithm_id].size()) // Should never happend.
      {
        LOGE("Filtering: triggers database size constraint failure.");
        return(-1);
      }
      distances_per_trigger[id].push_back(matches[i][j].distance);
    }
  }

  // If enabled, we'll compute the min dist and max dist of all the triggers.
  // This will allow to use only part of the dists in the mean computation below.
  if(JMAGINE_FEATURES_MATCHING_FILTER_MEAN_DIST[algorithm_id])
  {
    for(i=0; i<hnd.triggers[algorithm_id].size(); ++i)
    {
      for(j=0; j<distances_per_trigger[i].size(); ++j)
      {
        if(distances_per_trigger[i][j]>max_all_dists)
          max_all_dists=distances_per_trigger[i][j];
        else if(distances_per_trigger[i][j]<min_all_dists)
          min_all_dists=distances_per_trigger[i][j];
      }
    }
    if((max_all_dists==-4242424) || (min_all_dists==4242424) || (min_all_dists==max_all_dists) ||
       (max_all_dists<0) || (min_all_dists<0))
      dist_thr=-1.;
    else
      dist_thr=(max_all_dists-min_all_dists)/JMAGINE_FEATURES_MATCHING_DIST_ATHRESHOLD[algorithm_id];
  }

  // Then, compute the mean of the distance for the same.
  for(i=0; i<hnd.triggers[algorithm_id].size(); ++i)
  {
    if(distances_per_trigger[i].size()<6)  // We want at least 6 matches to compute the mean.
      continue;
    total=0.;
    int nb_total=0;
    for(j=0; j<distances_per_trigger[i].size(); ++j)
    {
      // If enabled, take only the distances that pass an adjusted threshold.
      if((JMAGINE_FEATURES_MATCHING_FILTER_MEAN_DIST[algorithm_id]) && (dist_thr!=-1.))
      {
        if(distances_per_trigger[i][j]<(min_all_dists+dist_thr))
        {
          total+=distances_per_trigger[i][j];
          nb_total++;
        }
      }
      else
      {
        total+=distances_per_trigger[i][j];
        nb_total++;
      }
    }
    if(nb_total<6)   // After adjusted threshold, not enough distances.
      continue;
    total=total/double(nb_total);
    nb_matches_used_for_mean[i]=nb_total;
    mean_dist_per_trigger[i]=total;
  }

  // Now, try and find the n best distances means.
  for(i=0; i<hnd.triggers[algorithm_id].size(); ++i)
  {
    if(mean_dist_per_trigger[i]==-1)
      continue;
    for(j=0; j<nb_best; ++j)
    {
      if(mean_dist_per_trigger[i]<best_dist[j])
      {
        for(k=nb_best-1; k>j; --k)
        {
          best_dist[k]=best_dist[k-1];
          best_dist_idx[k]=best_dist_idx[k-1];
        }
        best_dist[k]=mean_dist_per_trigger[i];
        best_dist_idx[k]=i;
        ++nb_best_dist;
        break;
      }
    }
  }
  if(nb_best_dist>nb_best)
    nb_best_dist=nb_best;

  // Now, add the n best dist means to the candidates.
  for(i=0; i<nb_best_dist; ++i)
  {
    jmagine_candidate_match_t nmatch;
    if(best_dist_idx[i]==-1)   // Should never happend.
      continue;
    nmatch.trigger_id=best_dist_idx[i];
    nmatch.algorithm_id=algorithm_id;
    nmatch.element_id=hnd.triggers[algorithm_id][nmatch.trigger_id].element_id;
    nmatch.nb_matches=distances_per_trigger[best_dist_idx[i]].size();
    nmatch.score=best_dist[i];
    nmatch.nb_matches_used_for_score=nb_matches_used_for_mean[best_dist_idx[i]];
    candidates.push_back(nmatch);
    dist_selected[best_dist_idx[i]]=true;
    ++nb_candidates;
    if(nb_candidates>=nb_best)
      break;
  }

  if(nb_candidates>=nb_best)   // We already got enough.
    return(0);

  // We don't have enough. Now try to find the ones with the most matches to complete.
  // Find the n with the most occurences.
  for(i=0; i<hnd.triggers[algorithm_id].size(); ++i)
  {
    if(dist_selected[i])  // Already have that one.
      continue;
    for(j=0; j<nb_best; ++j)
    {
      if(int(distances_per_trigger[i].size())>best_occ[j])
      {
        for(k=nb_best-1; k>j; --k)
        {
          best_occ[k]=best_occ[k-1];
          best_occ_idx[k]=best_occ_idx[k-1];
        }
        best_occ[k]=distances_per_trigger[i].size();
        best_occ_idx[k]=i;
        ++nb_best_occ;
        break;
      }
    }
  }
  if(nb_best_occ>nb_best)
    nb_best_occ=nb_best;

  // And add them to the candidates.
  for(i=0; i<nb_best_occ; ++i)
  {
    jmagine_candidate_match_t nmatch;
    if(best_occ_idx[i]==-1)   // Should never happend.
      continue;
    nmatch.trigger_id=best_occ_idx[i];
    nmatch.algorithm_id=algorithm_id;
    nmatch.element_id=hnd.triggers[algorithm_id][nmatch.trigger_id].element_id;
    nmatch.nb_matches=best_occ[i];
    score=0.;
    // If enough matches, get the mean of the distances of the matches as the score.
    if(nmatch.nb_matches>=6)
    {
      for(j=0; j<distances_per_trigger[best_occ_idx[i]].size(); ++j)
        score+=distances_per_trigger[best_occ_idx[i]][j];
      score=score/double(distances_per_trigger[best_occ_idx[i]].size());
    }
    else   // If not, try to describe the score.
      score=100.-(5.*nmatch.nb_matches);
    nmatch.nb_matches_used_for_score=distances_per_trigger[best_occ_idx[i]].size();
    nmatch.score=score;
    candidates.push_back(nmatch);
    ++nb_candidates;
    if(nb_candidates>=nb_best)
      break;
  }

  return(0);
}



// Returns 0 on success, something else on failure.
int jmagine_features_filter_crosscheck(const vector<vector<DMatch> >& matches_q2m,
                                       const vector<vector<DMatch> >& matches_m2q, vector<DMatch>& filtered_matches)
{
  size_t i, j, k, l;
  DMatch current_match_q2m, current_match_m2q;
  bool found;

  for(i=0; i<matches_q2m.size(); ++i)   // For each q2m group of knn matches.
  {
    for(j=0; j<matches_q2m[i].size(); ++j)   // For each q2m match of the group.
    {
      current_match_q2m=matches_q2m[i][j];
      found=false;
      for(k=0; k<matches_m2q.size(); ++k)   // For each m2q group of knn matches.
      {
        for(l=0; l<matches_m2q[k].size(); ++l)   // For each m2q match of the group.
        {
          current_match_m2q=matches_m2q[k][l];
          // Exact match found.
          if((current_match_q2m.trainIdx==current_match_m2q.queryIdx) &&
             (current_match_q2m.queryIdx==current_match_m2q.trainIdx))
          {
            filtered_matches.push_back(current_match_q2m);
            found=true;
            break;
          }
        }
        if(found)
          break;
      }
    }
  }

  return(0);
}




// Simple threshold filtering. This extract matches using a simple threshold on the distance (dist<min_dist*2).
// If dist_min==-1, then the minimum distance is computed from the specified matches. If not, the specified
// value is used.
// Returns 0 on success, something else on failure.
int jmagine_features_filter_simple_threshold(const vector<DMatch>& matches, vector<DMatch>& filtered_matches,
                                             double threshold, int dist_min)
{
  double max_dist, min_dist;
  size_t i;
  int rv;

  if(dist_min!=-1)
    min_dist=dist_min;
  else
  {
    if((rv=jmagine_utils_get_min_max_dist_matches(matches, &min_dist, &max_dist))!=0)
      return(rv);
  }

  for(i=0; i<matches.size(); i++)
  {
    if((min_dist>0) && (threshold>0))
    {
      if(matches[i].distance<(threshold*min_dist))
        filtered_matches.push_back(matches[i]);
    }
    else  // No threshold or min dist 0: do not filter anything.
      filtered_matches.push_back(matches[i]);
  }

  return(0);
}




// Adjusted threshold filtering. This does basically the same thing as simple_threshold, but this function tries to
// pinpoint a good value for a filtering threshold by taking into account min_dist *and* max_dist.
// Thus, matches pass if they have: dist<min_dist+((max_dist-min_dist)/threshold).
// If dist_min==-1 and dist_max==-1, the function will pinpoint the min/max values from matches. If not, it takes the
// provided values instead.
// Returns 0 on success, something else on failure.
int jmagine_features_filter_adjusted_threshold(const vector<DMatch>& matches, vector<DMatch>& filtered_matches,
                                               double threshold, int dist_min, int dist_max)
{
  int rv;
  size_t i;
  double max_dist, min_dist, dist_thr;

  if(threshold<=0)
    return(-1);
  if(matches.size()<=1)   // No filtering in that case.
  {
    for(i=0; i<matches.size(); ++i)
      filtered_matches.push_back(matches[i]);
    return(0);
  }

  if((dist_min!=-1) && (dist_max!=-1))
  {
    min_dist=dist_min;
    max_dist=dist_max;
  }
  else
  {
    if((rv=jmagine_utils_get_min_max_dist_matches(matches, &min_dist, &max_dist))!=0)
      return(rv);
    if(dist_min!=-1)
      min_dist=dist_min;
    if(dist_max!=-1)
      max_dist=dist_max;
  }

  if(min_dist==max_dist)   // No filtering in that case.
  {
    for(i=0; i<matches.size(); ++i)
      filtered_matches.push_back(matches[i]);
    return(0);
  }
  
  dist_thr=(max_dist-min_dist)/threshold;
  for(i=0; i<matches.size(); i++)
  {

    if((min_dist>0) && (max_dist>0))
    {
      if(matches[i].distance<(min_dist+dist_thr))
        filtered_matches.push_back(matches[i]);
    }
    else  // min_dist 0 or max_dist 0: no filtering.
      filtered_matches.push_back(matches[i]);
  }

  return(0);
}




// Ratio filtering. This uses Lowe's ratio method as described in his paper.
// The function takes knn matches. The knn value must be two (or more, but higher dimensions will be ignored).
// Basically, a knn match passes if knn[0].dist/knn[1].dist is less than ratio.
// In other words, a knn match that has another keypoint match with a close distance is to be ignored, because it means
// that this match is likely to be a false positive.
// Returns 0 on success, something else on failure.
int filter_matches_ratio(const vector<vector<DMatch> >& knnmatches, vector<DMatch>& filtered_matches, double ratio)
{
  size_t i;

  for(i=0; i<knnmatches.size(); i++)
  {
    if(knnmatches[i].size()==1)   // No other candidates, this match passes the filter.
      filtered_matches.push_back(knnmatches[i][0]);
    else if(knnmatches[i].size()>=2)
    {
      if((knnmatches[i][0].distance/knnmatches[i][1].distance)<ratio)
        filtered_matches.push_back(knnmatches[i][0]);
    }
  }

  return(0);
}




// Ratio filtering, but will return all knn candidates in the filtered matches, not just the first one.
// In other words, if (knn[0].dist/knn[1].dist)<ratio, then knn[0] *and* knn[1] go through the filter.
// Also, if there are more than two knn matches, they also go through.
// Returns 0 on success, something else on failure.
int filter_matches_ratio_knn(const vector<vector<DMatch> >& knnmatches, vector<vector<DMatch> >& filtered_matches,
                             double ratio)
{
  size_t i;
  bool passes;

  for(i=0; i<knnmatches.size(); i++)
  {
    passes=false;
    if(knnmatches[i].size()==1)   // No other candidates, this match passes the filter.
      passes=true;
    else if(knnmatches[i].size()>=2)
    {
      if((knnmatches[i][0].distance/knnmatches[i][1].distance)<ratio)
        passes=true;
    }
    if(passes)   // Goes through the filter.
      filtered_matches.push_back(knnmatches[i]);
  }

  return(0);
}




// Homography-based filtering. Will try to find a coherent homography from the specified matches and features, and
// discard outliers using either RANSAC or LMEDS (method).
// This can take a long time. The found homography is stored in homography.
// Returns 0 on success, -1 on failure.
int jmagine_features_filter_homography(const vector<DMatch>& matches, const vector<KeyPoint>& query_features,
                                       const vector<KeyPoint>& model_features, int method,
                                       vector<DMatch>& filtered_matches, Mat& homography)
{
  size_t i;
  vector<Point2f> model_pts(matches.size());
  vector<Point2f> query_pts(matches.size());
  Mat H;
  vector<unsigned char> mask_inliers;

  if((method!=CV_RANSAC) && (method!=CV_LMEDS))
    return(-1);

  // Convert the matched keypoints to points.
  for(i=0; i<matches.size(); ++i)
  {
    model_pts[i]=model_features[matches[i].trainIdx].pt;
    query_pts[i]=query_features[matches[i].queryIdx].pt;
  }
  mask_inliers.resize(model_pts.size());
  // Find a coherent homography from the points.
  H=findHomography(model_pts, query_pts, method, 3, mask_inliers);

  // Fill in the inliers (our filtered matches).
  for(size_t i=0; i<mask_inliers.size(); i++)
  {
    if(mask_inliers[i])
      filtered_matches.push_back(matches[i]);
  }

  homography=H.clone();
  H.release();
  return(0);
}

