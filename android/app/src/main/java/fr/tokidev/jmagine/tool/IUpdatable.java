package fr.tokidev.jmagine.tool;

/**
 * Created by max on 10/04/15.
 */
public interface IUpdatable
{

    public void update();

}
