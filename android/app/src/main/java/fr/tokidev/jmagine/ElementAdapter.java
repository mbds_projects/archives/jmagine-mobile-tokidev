package fr.tokidev.jmagine;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.tokidev.jmagine.model.ElementInfo;
import fr.tokidev.jmagine.tool.Tool;
import fr.tokidev.jmagine.tools.Globals;
import fr.tokidev.jmagine.tools.PictureTools;
import fr.tokidev.jmagine.ui.AproposActivity;
import fr.tokidev.jmagine.ui.ContenuHistoriqueActivity;
import fr.tokidev.jmagine.ui.ContenuPedagogiqueActivity;
import fr.tokidev.jmagine.ui.DetailActivity;
import fr.tokidev.jmagine.ui.ParcoursActivity;
import fr.tokidev.jmagine.ui.CameraActivity;
import fr.tokidev.jmagine.ui.ScanPoiActivity;
import fr.tokidev.jmagine.ui.WebViewActivity;

/**
 * Created by Gav on 20/01/2015.
 */

public class ElementAdapter extends RecyclerView.Adapter<ElementAdapter.ElementViewHolder> {

    private List<ElementInfo> elementInfoList;
    private Context context;
    Typeface font;

    public ElementAdapter(List<ElementInfo> elementInfoList, Context context) {
        this.elementInfoList = elementInfoList;
        this.context = context;
        this.font = Typeface.createFromAsset( context.getAssets(), "fonts/Muli-Regular.ttf");
    }

    @Override
    public int getItemCount() {
        return elementInfoList.size();
    }


    @Override
    public ElementViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);

        ((TextView)itemView.findViewById(R.id.cardTitle)).setTypeface(font);

        return new ElementViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(ElementViewHolder elementViewHolder, int i) {

        ElementInfo elementInfo = elementInfoList.get(i);
        elementViewHolder.vTitle.setText(elementInfo.title);

        if ( elementInfo.background != null && !elementInfo.background.contains("//") )
        {
            Resources resources = context.getResources();
            int resId = resources.getIdentifier(elementInfo.background, "drawable", context.getPackageName());
            int pxHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, resources.getDisplayMetrics());
            Bitmap backgroundPic = PictureTools.decodeSampledBitmapFromResource(resources, resId, Globals.WIDTH, pxHeight);
            Drawable drawable = new BitmapDrawable(resources, backgroundPic);
            elementViewHolder.vBackground.setImageDrawable(drawable);
        }
        else
        {
            Tool.addToLazyLoadingPool(elementViewHolder.vBackground,elementInfo.background);
        }
    }

    public class ElementViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView vTitle;
        protected ImageView vBackground;

        public ElementViewHolder(View v) {
            super(v);
            vTitle =  (TextView) v.findViewById(R.id.cardTitle);
            vBackground = (ImageView) v.findViewById(R.id.card_background);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            Intent myIntent;
            ElementInfo el = elementInfoList.get(getPosition());
            switch (el.type)
            {
                // Scan
                case ElementInfo.TYPE_SCAN:
                    myIntent = new Intent(context,ScanPoiActivity.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(myIntent);
                break;

                case ElementInfo.TYPE_VISIT:
                    myIntent = new Intent(context,DetailActivity.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    myIntent.putExtra(DetailActivity.EXTRA_IDPOI,el.extra);
                    context.startActivity(myIntent);
                break;

                case ElementInfo.TYPE_HTML:
                    myIntent = new Intent(context,WebViewActivity.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    myIntent.putExtra(WebViewActivity.HTML,el.extra);
                    context.startActivity(myIntent);
                break;


            }
        }
    }
}