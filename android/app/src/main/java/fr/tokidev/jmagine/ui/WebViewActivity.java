package fr.tokidev.jmagine.ui;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import fr.tokidev.jmagine.tools.JmagineApplication;

import fr.tokidev.jmagine.R;

public class WebViewActivity extends ActionBarActivity
{
    public static final String HTML = "HTML";

    WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        wv = (WebView)findViewById(R.id.webview);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setDisplayZoomControls(false);
        wv.getSettings().setJavaScriptEnabled(true);

        String html = getIntent().getStringExtra(HTML);
        wv.loadDataWithBaseURL("", html, "text/html", "UTF-8", "");

    }



    @Override
    protected void onResume() {
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
    }


}
