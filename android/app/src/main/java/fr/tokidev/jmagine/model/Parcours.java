package fr.tokidev.jmagine.model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import fr.tokidev.jmagine.tool.Tool;

/**
 * Created by max on 09/04/15.
 */
public class Parcours
{
    private String id;
    private String title;
    private String backgroundPic;
    private int components_count;
    private int pois_count;
    private String first_poi;
    private List<Component> components;

    public Parcours()
    {

    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBackgroundPic() {
        return backgroundPic;
    }

    public int getComponents_count() {
        return components_count;
    }

    public int getPois_count() {
        return pois_count;
    }

    public String getFirst_poi() {
        return first_poi;
    }

    public List<Component> getComponents() {
        return components;
    }


    public class Component
    {
        private String title;
        private String backgroundPic;
        private String content;


        public String getTitle() {
            return title;
        }

        public String getBackgroundPic() {
            return backgroundPic;
        }

        public String getContent() {
            return content;
        }
    }

    public void fillParcoursFromXml(Document xml)
    {
        NodeList nl = xml.getElementsByTagName("parcours");
        for (int i = 0; i < nl.getLength(); i++)
        {
            Element e = (Element) nl.item(i);
            this.id                = Tool.getXmlValue(e,"id");
            this.title             = Tool.getXmlValue(e,"title");
            this.backgroundPic     = Tool.getXmlValue(e,"backgroundPic");
            this.first_poi         = Tool.getXmlValue(e,"first_poi");
            this.components = new ArrayList<Component>();
            NodeList NodeComponents = e.getElementsByTagName("component");
            this.components_count = NodeComponents.getLength();
            for(int j = 0 ; j< this.getComponents_count(); j++)
            {
                Parcours.Component component = new Parcours.Component();
                Element el              = (Element)NodeComponents.item(j);
                component.title         = Tool.getXmlValue(el,"title");
                component.backgroundPic = Tool.getXmlValue(el,"backgroundPic");
                component.content       = Tool.getXmlValue(el,"content");
                components.add(component);

            }
        }

    }


}
