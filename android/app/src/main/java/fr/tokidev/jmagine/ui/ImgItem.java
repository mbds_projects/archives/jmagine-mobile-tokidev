package fr.tokidev.jmagine.ui;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.tools.Globals;
import fr.tokidev.jmagine.tools.PictureTools;


public class ImgItem extends Fragment {

    public ImgItem(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_img_item, container, false);

        Bundle bundle = getArguments();
        String drawableStr = bundle.getString("drawable");


        Resources resources = getResources();
        int resId = resources.getIdentifier(drawableStr, "drawable", Globals.PACKAGE_NAME);
        int pxHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, resources.getDisplayMetrics());
        Bitmap picture = PictureTools.decodeSampledBitmapFromResource(resources, resId, Globals.WIDTH, pxHeight);
        Drawable drawable = new BitmapDrawable(resources, picture);

        ((ImageView)mView.findViewById(R.id.img_item)).setImageDrawable(drawable);

        return mView;
    }
}