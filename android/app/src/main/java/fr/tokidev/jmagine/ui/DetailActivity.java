package fr.tokidev.jmagine.ui;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.MarkerOptionsCreator;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.model.ElementInfo;
import fr.tokidev.jmagine.model.Poi;
import fr.tokidev.jmagine.tool.IUpdatable;
import fr.tokidev.jmagine.tool.Tool;
import fr.tokidev.jmagine.tools.Globals;
import fr.tokidev.jmagine.tools.JmagineApplication;
import fr.tokidev.jmagine.tools.PictureTools;

public class DetailActivity extends ActionBarActivity
{
    public final static String EXTRA_IDPOI = "IDPOI";
    String idpoi;
    TextView tvtitre;
    Poi poi;
    LatLng latLngPoi;
    MapFragment mapfragment;
    ImageView recopoi;
    Handler mainHandler;
    boolean isMapActive = false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        mainHandler= new Handler(this.getMainLooper());

        idpoi = getIntent().getStringExtra(EXTRA_IDPOI);

        mapfragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if(mapfragment!=null && mapfragment.getMap()!=null)
        {
            isMapActive=true;
            mapfragment.getMap().setMyLocationEnabled(true);
            mapfragment.getMap().setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng)
                {
                    if(latLngPoi!=null)
                    {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q="+poi.getLat()+","+poi.getLng()+"&mode=w");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    }
                }
            });
        }

        tvtitre = (TextView)findViewById(R.id.detail_title);
        tvtitre.setTypeface(((JmagineApplication)getApplication()).font);
        recopoi = (ImageView)findViewById(R.id.imgreco);



       /* mapfragment.getMap().setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng latLng)
            {
                if(latLngPoi!=null)
                {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q="+poi.getLat()+","+poi.getLng()+"&mode=w");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            }
        });*/

        recopoi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(DetailActivity.this,ScanPoiActivity.class);
                i.putExtra(ScanPoiActivity.isNFCEnabled,poi.isNFCEnabled());
                i.putExtra(ScanPoiActivity.isQREnabled,poi.isQREnabled());
                i.putExtra(ScanPoiActivity.isSNSEnabled,poi.isSNSEnabled());
                i.putExtra(ScanPoiActivity.isGeolocEnabled,poi.isGeolocEnabled());
                i.putExtra(ScanPoiActivity.geoLocCoordinates,latLngPoi);
                i.putExtra(POIActivity.EXTRA_IDPOI,idpoi);

                DetailActivity.this.startActivity(i);
            }
        });

        loadInfoFromPoi();





    }

    final IUpdatable iup = new IUpdatable() {
        @Override
        public void update() {
            loadInfoFromPoi();
        }
    };

    public void loadInfoFromPoi()
    {


        Runnable r = new Runnable()
        {
            @Override
            public void run()
            {
                String str = Tool.getXmlFromUrl("http://jmagine.tokidev.fr/api/parcours/4/pois/"+idpoi);
                if(str==null) //erreur de réseau afficher la boite de dialog et retry
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Tool.popItUp("Erreur de Réseau","Cette application nécessite une connection internet pour fonctionner, merci de ré-essayer.",DetailActivity.this,iup);
                        }
                    });

                    return;
                }

                 poi = new Poi();

                poi.fillPoiFromXml(Tool.getXmlDomElement(str));


                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run()
                    {
                        if ( poi.getBackgroundPic() != null )
                        {
                            Tool.addToLazyLoadingPool((ImageView)findViewById(R.id.detail_background), poi.getBackgroundPic());
                        }
                        tvtitre.setText(poi.getTitle());


                        latLngPoi = new LatLng(Double.parseDouble(poi.getLat()),Double.parseDouble(poi.getLng()));



                        if(isMapActive)
                        {
                            Marker poimarker = mapfragment.getMap().addMarker(new MarkerOptions().position(latLngPoi).title(poi.getTitle()));
                            mapfragment.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(latLngPoi, 15));
                        }
                    }
                }; // This is your code
                mainHandler.post(myRunnable);





            }
        };
        new Thread(r).start();




    }

    @Override
    protected void onResume() {
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
    }

}
