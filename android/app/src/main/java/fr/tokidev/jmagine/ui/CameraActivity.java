package fr.tokidev.jmagine.ui;

import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.tools.JmagineApplication;
import fr.tokidev.jmagine.tool.Tool;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import fr.tokidev.jmagine.CameraLivePreview;


public class CameraActivity extends Activity
{
    CameraLivePreview clp;
    public RelativeLayout rl;
    ImageView flipcam;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        FrameLayout fl = new FrameLayout(this);
        setContentView(fl);



        clp = new CameraLivePreview(this);

        rl = new RelativeLayout(this);
        RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
        int pix = (int)Tool.dipToPixels(this, 15);
        rllp.setMargins(pix,pix, pix, pix);
        fl.addView(rl,rllp);

        rllp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
        rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
        rl.addView(clp,0,rllp);


        flipcam = new ImageView(this);
        flipcam.setImageResource(R.drawable.flip_camera);
        rllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        rllp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        rllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        rllp.setMargins(0, pix, pix,0);
        rl.addView(flipcam,rllp);


        if(!clp.hasFrontCam())
            flipcam.setVisibility(View.GONE);
        flipcam.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                clp.switchCam();
            }
        });

        ImageView viseur = new ImageView(this);
        viseur.setImageResource(R.drawable.viseur);
        viseur.setScaleType(ScaleType.FIT_START);
        viseur.setAdjustViewBounds(true);
        viseur.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
				/*int id = DataPackage.dal.generateFakePic();
				Intent i = new Intent(CameraActivity.this,ItemDisplayActivity.class);
				Item item = DataPackage.dal.getItemById(String.valueOf(id));
				i.putExtra("id",Integer.parseInt(item.idequip));
				CameraActivity.this.startActivity(i);*/

            }
        });
        clp.viseur = viseur;

        rllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
        rl.addView(viseur,rllp);
    }


    public void updateFlipCamWithMargins(int w, int h)
    {
        int pix = (int)Tool.dipToPixels(this, 15);
        RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        rllp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        rllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        rllp.setMargins(0, h+pix, w+pix,0);
        flipcam.setLayoutParams(rllp);

    }



    @Override
    protected void onResume()
    {
        JmagineApplication.JmagineInstance.onResume();
        clp.onResume();
        super.onResume();
    }
    @Override
    protected void onPause()
    {
        JmagineApplication.JmagineInstance.onPause();
        clp.onPause();
        super.onPause();
    }

}
