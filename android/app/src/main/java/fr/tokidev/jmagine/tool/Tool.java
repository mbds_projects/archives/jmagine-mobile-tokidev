package fr.tokidev.jmagine.tool;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ImageView;
import android.app.Activity;
import android.net.ConnectivityManager;
import android.os.Looper;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import fr.tokidev.jmagine.RecognitionService;

public class Tool {

    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }


    public void AsyncLoadImageFromCache(String path, ImageView imgv) {

    }


    public static float spToPixels(Context context, int spValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue, metrics);
    }


    //DOWNLOAD XML FROM WEBSERVICE WITH URL url

    public static String getXmlFromUrl(String url) {
        String xml = null;

        try {
            // defaultHttpCli@ent
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            xml = EntityUtils.toString(httpEntity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return XML
        return xml;
    }

    //PARSING XML

    public static Document getXmlDomElement(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        // return DOM
        return doc;
    }

    public static String getXmlValue(Element item, String str) {
        NodeList n = item.getElementsByTagName(str);
        return getXmlElementValue(n.item(0));
    }

    public static String getXmlElementValue(Node elem) {
        Node child;
        if (elem != null) {
            if (elem.hasChildNodes()) {
                for (child = elem.getFirstChild(); child != null; child = child.getNextSibling()) {
                    if (child.getNodeType() == Node.TEXT_NODE) {
                        return child.getNodeValue();
                    }
                }
            }
        }
        return "";
    }


    //LAZY LOADING CACHE
    public static HashMap<String, Bitmap> cache = new HashMap<String, Bitmap>();

    public static void addToLazyLoadingPool(ImageView vBackground, String background) {
        if (cache.containsKey(background)) {
            vBackground.setImageBitmap(cache.get(background));
        } else {
            new DownloadImageTask(vBackground).execute(background);
        }
    }

    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        String urldisplay;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            cache.put(urldisplay, result);
        }
    }


    //UNZIP ASSETS

    public static boolean unzipAssets(Context ctx/*,IProgressable progressable*/, int percent) throws IOException {
        String path = ctx.getApplicationInfo().dataDir + "//";
        File file = new File(path + "/extracted.ifo");
        if (file.exists())
            return false;

        InputStream zipFileStream = ctx.getAssets().open("triggers.zip");
        File f = new File(path);
        String filename;
        int count;
        ZipInputStream zis = new ZipInputStream(zipFileStream);

        ZipEntry ze = null;
        AssetFileDescriptor mydisc = ctx.getAssets().openFd("triggers.zip");
        long entry = mydisc.getLength();
        mydisc.close();
        byte[] buffer = new byte[2048];
        while ((ze = zis.getNextEntry()) != null) {
            Log.w("SNS", "extracting file: '" + ze.getName() + "'...");
            filename = ze.getName();
            if (ze.isDirectory()) {
                File fmd = new File(path + filename);
                fmd.mkdirs();
                continue;
            }
            FileOutputStream fout = null;
            fout = new FileOutputStream(path + filename);


            while ((count = zis.read(buffer)) != -1) {
                fout.write(buffer, 0, count);
            }
            fout.close();
            zis.closeEntry();
            //progressable.onProgressStep(((float)ze.getSize()/(float)entry)*(float)percent);
        }


        file.createNewFile();

        return true;
    }


    // RECOGNITION SERVICE


    public static RecognitionService rs = new RecognitionService();


    //DIALOG BOXES

    public static AlertDialog popItUp(String title, String message, Activity ctx,final IUpdatable iup)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id)
            {
                if(iup!=null)
                {
                    iup.update();
                    dialog.dismiss();
                    Log.d("JIMAGINE","DISMISS");
                }
                else
                {
                    Log.d("JIMAGINE","NO CALLBACK");
                }
            }
        });

        // Create the AlertDialog object and return it
        final AlertDialog ad = builder.create();
        ad.show();
        return ad;

    }

    // TEST RESEAU

    public static boolean isNetworkAvailable(Context context)
    {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
}