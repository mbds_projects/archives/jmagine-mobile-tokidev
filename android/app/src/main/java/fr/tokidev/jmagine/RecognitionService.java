
package fr.tokidev.jmagine;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

import android.graphics.Bitmap;




public class RecognitionService
{


	public static final int OLD_NICE_DB 	=  0;
	private static final String TAG="JMAGINE::RecognitionWrapper";

	static
	{
		if(!OpenCVLoader.initDebug())
		{
		}
		System.loadLibrary("ocvnf");
		System.loadLibrary("tkdv_jmagine_jni");
	}

	// Native JNI methods.
  private native String jmagine_recognition(double lat, double lon, long addr_in);
  private native int jmagine_android_init(String base_path, int db_id);


	public RecognitionService()
	{
	}


	public String recognition(Bitmap bmp, double lat, double lon)
	{
		String rv;
		Mat img=new Mat();

		Utils.bitmapToMat(bmp, img);
		rv=jmagine_recognition(lat, lon, img.nativeObj);
		img.release();

		return(rv);
	}

  public int init(String base_path, int db_id)
	{
    return(jmagine_android_init(base_path, db_id));
	}
}
