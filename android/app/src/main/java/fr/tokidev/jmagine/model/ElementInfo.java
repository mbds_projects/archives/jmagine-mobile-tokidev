package fr.tokidev.jmagine.model;

import java.io.Serializable;

/**
 * Created by Gav on 20/01/2015.
 */
public class ElementInfo implements Serializable
{
    public static final int TYPE_SCAN  = 0;
    public static final int TYPE_VISIT = 1;
    public static final int TYPE_HTML  = 2;

    public String title;
    public String background;
    public String description;
    public String img;
    public int type;
    public String extra;
}