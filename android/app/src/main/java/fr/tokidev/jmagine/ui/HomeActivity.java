package fr.tokidev.jmagine.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import java.lang.Override;
import java.lang.Runnable;
import java.util.ArrayList;
import java.util.List;

import fr.tokidev.jmagine.ElementAdapter;
import fr.tokidev.jmagine.model.ElementInfo;
import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.model.Parcours;
import fr.tokidev.jmagine.tool.IUpdatable;
import fr.tokidev.jmagine.tool.Tool;
import fr.tokidev.jmagine.tools.JmagineApplication;

public class HomeActivity extends Activity
{
    private Parcours parcours;
    private List<ElementInfo> list;
    RecyclerView recList;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        createContent();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    final IUpdatable iup = new IUpdatable() {
        @Override
        public void update() {
            createContent();
        }
    };

    private void createContent()
    {
        list = new ArrayList<ElementInfo>();
        Runnable r = new Runnable()
        {
            @Override
            public void run()
            {

                String str = Tool.getXmlFromUrl("http://jmagine.tokidev.fr/api/parcours/4");
                if(str==null) //erreur de réseau afficher la boite de dialog et retry
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Tool.popItUp("Erreur de Réseau","Cette application nécessite une connection internet pour fonctionner, merci de ré-essayer.",HomeActivity.this,iup);

                        }
                    });

                    return;
                }
                parcours = new Parcours();
                parcours.fillParcoursFromXml(Tool.getXmlDomElement(str));



                ElementInfo elementInfo = new ElementInfo();
                elementInfo.title       = parcours.getTitle();
                elementInfo.background  = parcours.getBackgroundPic();
                elementInfo.type        = ElementInfo.TYPE_VISIT;
                elementInfo.extra       = parcours.getFirst_poi();
                list.add(elementInfo);

                ElementInfo elementInfoEdifice = new ElementInfo();
                elementInfoEdifice.title = "Scanner un POI";
                elementInfoEdifice.background = "img1";
                elementInfoEdifice.type = ElementInfo.TYPE_SCAN;
                list.add(elementInfoEdifice);

                for(Parcours.Component p : parcours.getComponents())
                {
                    ElementInfo el = new ElementInfo();
                    el.title = p.getTitle();
                    el.background = p.getBackgroundPic();
                    el.type = ElementInfo.TYPE_HTML;
                    el.extra = p.getContent().replace("<![CDATA[","").replace("]]>","");
                    list.add(el);
                }

                HomeActivity.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ElementAdapter elementAdapter = new ElementAdapter(list, getApplicationContext());
                        recList.setAdapter(elementAdapter);
                    }
                });
            }
        };

        Thread mythread = new Thread(r);
        mythread.start();

    }


    @Override
    protected void onResume() {
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
    }
}
