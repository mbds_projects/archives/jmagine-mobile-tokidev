package fr.tokidev.jmagine.tools;

/**
 * Created by Gav on 21/01/2015.
 */
public class Globals
{
    public static int WIDTH;
    public static int HEIGHT;
    public static String PACKAGE_NAME;

    public static int CARD_WIDTH;
    public static int CARD_HEIGHT;
}
