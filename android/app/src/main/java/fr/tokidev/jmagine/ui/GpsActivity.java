package fr.tokidev.jmagine.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;

import fr.tokidev.jmagine.tools.JmagineApplication;

import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Text;

import android.widget.ImageView;

import fr.tokidev.jmagine.R;

public class GpsActivity extends Activity implements LocationListener
{

    LatLng latLngPoi;
    TextView tv;
    ImageView imgv;

    // Animation
    Animation anim;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);
        latLngPoi = getIntent().getParcelableExtra(ScanPoiActivity.geoLocCoordinates);

        tv = (TextView)findViewById(R.id.gps_text);
        imgv = (ImageView) findViewById(R.id.gpsimage);
        anim =  AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink);

        imgv.startAnimation(anim);

        tv.setText("Veuillez patienter pendant la géolocalisation...");
        CheckLocation();

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        JmagineApplication.JmagineInstance.setForwardListener(this);
        JmagineApplication.JmagineInstance.onResume();
        CheckLocation();

    }

    @Override
    protected void onPause()
    {
        super.onPause();
        JmagineApplication.JmagineInstance.removeListener();
        JmagineApplication.JmagineInstance.onPause();
    }



    public void CheckLocation()
    {
        if(JmagineApplication.location==null)
        {
            tv.setText("Veuillez patienter pendant la géolocalisation...");
            return;
        }
        Location dlocation = new Location("");
        dlocation.setLatitude(latLngPoi.latitude);
        dlocation.setLongitude(latLngPoi.longitude);
        float distance = JmagineApplication.location.distanceTo(dlocation);

        String str = getString(R.string.resteAparcourir);
        tv.setText(String.format(str,distance));

        if(distance<30)
        {
            Intent i = new Intent(GpsActivity.this,POIActivity.class);
            i.putExtra(POIActivity.EXTRA_IDPOI,getIntent().getStringExtra(POIActivity.EXTRA_IDPOI));
            startActivity(i);
        }

    }

    @Override
    public void onLocationChanged(Location location)
    {
        CheckLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
