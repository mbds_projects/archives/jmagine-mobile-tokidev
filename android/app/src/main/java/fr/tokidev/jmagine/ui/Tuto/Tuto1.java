package fr.tokidev.jmagine.ui.Tuto;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.util.Log;

import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.tools.JmagineApplication;

/**
 * Created by Gav on 19/01/2015.
*/
public class Tuto1 extends Fragment {

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mView = inflater.inflate(R.layout.tuto_1, container, false);

        ((TextView)mView.findViewById(R.id.tuto_text)).setTypeface(((JmagineApplication)this.getActivity().getApplication()).font);
        
        return mView;
    }


}