package fr.tokidev.jmagine.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.lang.Exception;
import java.lang.Override;
import java.lang.Runnable;

import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.model.Poi;
import fr.tokidev.jmagine.tool.IUpdatable;
import fr.tokidev.jmagine.tool.Tool;
import fr.tokidev.jmagine.tools.JmagineApplication;

/**
 * Created by max on 10/04/15.
 */
public class POIActivity extends ActionBarActivity
{

        public final static String EXTRA_IDPOI = "IDPOI";
        String idpoi;
        TextView tvtitre;
        Poi poi;
        LatLng latLngPoi;
        ImageView pointSuivant;
        Handler mainHandler;
        WebView wv;
        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_poi);
            mainHandler= new Handler(this.getMainLooper());

            idpoi = getIntent().getStringExtra(EXTRA_IDPOI);

            tvtitre = (TextView)findViewById(R.id.detail_title);
            tvtitre.setTypeface(((JmagineApplication)getApplication()).font);
            wv = (WebView)findViewById(R.id.webview);
            wv.getSettings().setBuiltInZoomControls(true);
            wv.getSettings().setDisplayZoomControls(false);
            wv.getSettings().setJavaScriptEnabled(true);
            pointSuivant = (ImageView)findViewById(R.id.suivantimage);

            pointSuivant.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(poi.getNext_poi()!=null && poi.getNext_poi().length()>0)
                    {
                        Intent i = new Intent(POIActivity.this, DetailActivity.class);
                        i.putExtra(DetailActivity.EXTRA_IDPOI, poi.getNext_poi());
                        POIActivity.this.startActivity(i);
                    }
                }
            });

            loadInfoFromPoi();





        }

    final IUpdatable iup = new IUpdatable() {
        @Override
        public void update() {
            loadInfoFromPoi();
        }
    };

        public void loadInfoFromPoi()
        {


            Runnable r = new Runnable()
            {
                @Override
                public void run()
                {
                    String str = Tool.getXmlFromUrl("http://jmagine.tokidev.fr/api/parcours/4/pois/" + idpoi);
                    if(str==null) //erreur de réseau afficher la boite de dialog et retry
                    {
                        POIActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                Tool.popItUp("Erreur de Réseau","Cette application nécessite une connection internet pour fonctionner, merci de ré-essayer.",POIActivity.this,iup);
                            }
                        }
                        );
                        return;
                    }
                    poi = new Poi();

                    poi.fillPoiFromXml(Tool.getXmlDomElement(str));



                    Runnable myRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            wv.loadDataWithBaseURL("", poi.getContent().replace("<![CDATA[", "").replace("]]>", ""), "text/html", "UTF-8", "");
                            if ( poi.getBackgroundPic() != null )
                            {
                                Tool.addToLazyLoadingPool((ImageView)findViewById(R.id.detail_background), poi.getBackgroundPic());
                            }
                            tvtitre.setText(poi.getTitle());
                            latLngPoi = new LatLng(Double.parseDouble(poi.getLat()),Double.parseDouble(poi.getLng()));




                        }
                    }; // This is your code
                    mainHandler.post(myRunnable);





                }
            };
            new Thread(r).start();




        }




    @Override
    protected void onResume() {
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
        try
        {
            Class.forName("android.webkit.WebView").getMethod("onResume", (Class[]) null).invoke(wv, (Object[]) null);
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
        try
        {
            Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null).invoke(wv, (Object[]) null);
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
    }

    }

