package fr.tokidev.jmagine;


import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import fr.tokidev.jmagine.tools.JmagineApplication;
import fr.tokidev.jmagine.tool.Tool;
import fr.tokidev.jmagine.ui.CameraActivity;
import fr.tokidev.jmagine.ui.POIActivity;


@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public final class CameraLivePreview extends SurfaceView implements SurfaceHolder.Callback
{
    public static int currentTicket =  0;
    public static enum STATE{STATE_OFF,STATE_INIT,STATE_SWITCH_CAM,STATE_INIT_DONE,STATE_SCAN,STATE_FOCUS_SUCCESS,STATE_FOCUS_FAILED,STATE_WAITING_RESULT,STATE_FOUND,STATE_NOTFOUND,STATE_WAITING_BEFORE_FOCUS,DOC_NOT_FOUND};


    private SurfaceHolder holder;
    private Camera camera;
    Context ctx;
    int lastW=-1,lastH=-1;
    public ImageView viseur;
    boolean frontCam=false;
    Size optimalSize=null;

    AtomicReference<Enum<STATE>> currentState;
    private int recoCount=0;



    public CameraLivePreview(Context context)
    {
        super(context);
        this.ctx = context;
        init();
    }

    public CameraLivePreview(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.ctx = context;
        init();
    }

    public CameraLivePreview(final Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        this.ctx = context;
        init();
    }


    @SuppressWarnings("deprecation")
    public void applyState(STATE state, Object stateObject)
    {
        currentState.set(state);
        Log.d("PSA", "ETAT CHANGE A : " + state.name());
        switch (state)
        {
            case STATE_OFF:
                stopAnim();
                if(camera!=null)
                {
                    camera.stopPreview();
                    camera.setPreviewCallback(null);
                    camera.lock();
                    camera.release();
                    camera=null;
                }
                break;

            case STATE_INIT:
                holder = getHolder();
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
                    getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                holder.addCallback(this);
                break;

            case STATE_SWITCH_CAM:
                stopAnim();

                if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.GINGERBREAD)
                {
                    return;
                }
                frontCam = !frontCam;
                applyState(STATE.STATE_OFF, null);
                setUpCamera(lastW, lastH);

                break;

            case STATE_INIT_DONE:
                startPreview();
                applyState(STATE.STATE_WAITING_BEFORE_FOCUS, null);
                break;

            case STATE_WAITING_BEFORE_FOCUS:
                currentTicket++;
                stopAnim();
                waitLongAndFocus(6000,currentTicket);

                break;

            case STATE_SCAN:
                if(camera!=null)
                {
                    Handler mainHandler = new Handler(ctx.getMainLooper());
                    Runnable myRunnable = new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if(camera!=null)
                            {
                                startAnim();
                                camera.autoFocus(autoFocusCallback);
                            }
                        }
                    };
                    mainHandler.post(myRunnable);
                }
                break;

            case STATE_FOCUS_SUCCESS:
                try
                {
                    Camera.Parameters camParams = camera.getParameters();
                    Size optimalSize = getPictureSize(camParams.getSupportedPictureSizes()/*, this.optimalSize.width, this.optimalSize.height*/);
                    camParams.setPictureSize(optimalSize.width, optimalSize.height);
                    camParams.setJpegQuality(100);
                    camera.setParameters(camParams);
                    camera.takePicture(shutterCallback, rawCallback, jpegCallback);
                }
                catch (Exception e)
                {
                    Log.e("PSA", e.getLocalizedMessage());
                }
                camera.cancelAutoFocus();
                break;

            case STATE_FOCUS_FAILED:
                camera.cancelAutoFocus();
                applyState(STATE.STATE_WAITING_BEFORE_FOCUS, null);
                break;

            case STATE_WAITING_RESULT:
                Log.d("PSA","taille w ="+this.getWidth()+ " , h=" + this.getHeight());
                break;

            case STATE_FOUND:

                String res = (String)stateObject;

                // Get instance of Vibrator from current Context
                Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);

                // Vibrate for 300 milliseconds
                v.vibrate(new long[]{400,400,400},-1);

                String id = "-1";
                switch(res)
                {
                    case "01_nietzsche"         :id="6"; break;
                    case "02_hotel_beaurivage"  :id="7"; break;
                    case "03_biblio_apollinaire":id="8"; break;
                    case "05_aragon_triolet"    :id="9"; break;
                    case "06_saleya_matisse"    :id="10";break;
                    case "08_paganini"          :id="11";break;
                    case "09_alexandre_dumas"   :id="12";break;
                    case "10_da_boutau"         :id="13";break;
                    case "11_lou_fran_calin"    :id="14";break;
                    case "13_cafe_turin"        :id="15";break;

                }
                if(id.equals("-1"))
                {
                    applyState(STATE.STATE_NOTFOUND, null);
                }
                else
                {
                    Log.d("MBDS","ID TROUVE : " + id);
                    Intent i = new Intent(ctx, POIActivity.class);
                    i.putExtra(POIActivity.EXTRA_IDPOI,id);
                    ctx.startActivity(i);
                }



                break;

            case STATE_NOTFOUND:


                recoCount++;
                applyState(STATE.STATE_WAITING_BEFORE_FOCUS, null);


                break;

            case DOC_NOT_FOUND:
                camera.stopPreview();
                recoCount=0;
                stopAnim();
                Handler mainHandler = new Handler(ctx.getMainLooper());
                Runnable myRunnable = new Runnable()
                {
                    @Override
                    public void run() {
                        displayDocNotFoundAlert() ;

                    }

                };
                mainHandler.post(myRunnable);
                break;

        }
    }

    private void init()
    {
        currentState = new AtomicReference<Enum<STATE>>();
        applyState(STATE.STATE_INIT,null);
    }

    /**
     * Camera init stuff
     */
    @SuppressLint("NewApi")
    public void setUpCamera(int scW,int scH)
    {

        boolean firstLoad = false;
        if(lastW==-1)
        {
            firstLoad =true;
            lastH = scH;
            lastW = scW;
        }

        if(currentState.get() != STATE.STATE_INIT && currentState.get() !=STATE.STATE_OFF)
        {
            Log.d("PSA_SCAN", "CAMERA IS ALREADY INITIALIZED");
            //return;
        }

        System.out.println("SETUP CAMERA");
        if(camera!=null)
        {
            camera.stopPreview();
            camera.release();
            camera =  null;
        }
        int cameraCount = 0;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for ( int camIdx = 0; camIdx < cameraCount; camIdx++ )
        {
            Camera.getCameraInfo( camIdx, cameraInfo );
            if ( cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT && frontCam)
            {
                try
                {
                    camera = Camera.open( camIdx );
                    break;
                }
                catch (RuntimeException e)
                {
                    Log.e("PSA", "Camera failed to open: " + e.getLocalizedMessage());
                }

            }
            else if ( cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK && !frontCam)
            {
                try
                {
                    camera = Camera.open( camIdx );
                    break;
                }
                catch (RuntimeException e)
                {
                    Log.e("PSA", "Camera failed to open: " + e.getLocalizedMessage());
                }
            }

        }

        try
        {
            camera.setDisplayOrientation(90);//portrait
            camera.setPreviewDisplay(holder);
        }
        catch (IOException exception)
        {
            camera.release();
            camera = null;
        }

        try
        {
            Camera.Parameters parameters = camera.getParameters();

            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);

            if(firstLoad || optimalSize==null)
            {
                List<Size> sizes = parameters.getSupportedPreviewSizes();
                optimalSize = getPreviewSize(sizes,scH,scW);

   	 			/*RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

              int left  = (int)((scW-optimalSize.height)/2.0f);
              int top = (int)((scH-optimalSize.width)/2.0f);


	 	       if (left < top)
                {
                    top = Math.round(top - ((float)(left) * ((float)(optimalSize.height)/(float)(optimalSize.width))));
                    left = 0;
                }
                else if ( left > top )
                {
                    left = Math.round(left - ((float)(top) * ((float)(optimalSize.width)/(float)(optimalSize.height))));
                    top = 0;
                }

           //           Log.i("PSA", "Left: "+left+" / top: "+top+" / optimalSize.height: "+optimalSize.height+" / optimalSize.width: "+optimalSize.width+" / scW: "+scW+" / scH: "+scH);

   	 			rllp.setMargins(left, top, left, top);
   	 			CameraActivity ca = (CameraActivity)ctx;
   	 			ca.updateFlipCamWithMargins(left, top);
   	 			rllp.addRule(RelativeLayout.CENTER_IN_PARENT);
   	 			this.setLayoutParams(rllp);*/
            }

            parameters.setPreviewSize(optimalSize.width, optimalSize.height);
            camera.setParameters(parameters);
        }
        catch(Exception ex)
        {

        }

        applyState(STATE.STATE_INIT_DONE,null);
    }



    public boolean hasFrontCam()
    {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.GINGERBREAD)
        {
            return false;
        }

        int cameraCount = 0;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for ( int camIdx = 0; camIdx < cameraCount; camIdx++ )
        {
            Camera.getCameraInfo( camIdx, cameraInfo );
            if ( cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT  )
            {
                return true;
            }
        }
        return false;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        //setUpCamera();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        applyState(STATE.STATE_OFF,null);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {


        applyState(STATE.STATE_OFF,null);
        Log.d("PSA","surfaceChanged: " + format + " , w="+w+" , h=" + h);
        setUpCamera(w,h);
    }

    private Size getPreviewSize(List<Size> sizes, int w, int h)
    {

        Log.d("PSA-ratio","taille a matcher : (" + w + " , " + h + ")" );
        // requirement
        final double ASPECT_TOLERANCE = 0.05;
        double targetRatio = (double) w / h;
        if (sizes == null)
        {
            return null;
        }

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        int targetWidth = w;
        // find a size that match aspect ratio and size
        for (Size size : sizes)
        {
            Log.d("PSA-ratio","taille possible : (" + size.width + " , " + size.height + ")" );

            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff)
            {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        // it's not possible
        // ignore the requirement
        if (optimalSize == null)
        {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes)
            {
                if (Math.abs(size.width - targetWidth) < minDiff && size.height<=targetHeight)
                {
                    optimalSize = size;
                    minDiff = Math.abs(size.width - targetWidth);
                }
            }
        }
        Log.d("PSA-ratio","taille trouvée : (" + optimalSize.width + " , " + optimalSize.height + ")" );
        return optimalSize;
    }


    private Size getPictureSize(List<Size> sizes)
    {

        if (sizes == null)
        {
            return null;
        }

        Size optimalSize = null;
        double maxSize = 0;

        // find a size that match aspect ratio and size
        for (Size size : sizes)
        {
            Log.d("PSA-ratio","taille possible : (" + size.width + " , " + size.height + ")" );
            if(size.width>maxSize)
            {
                maxSize = size.width;
                optimalSize = size;
            }

        }

        Log.d("PSA-ratio","taille trouvée : (" + optimalSize.width + " , " + optimalSize.height + ")" );
        return optimalSize;
    }

    /**
     * Start the preview display
     */
    public void startPreview()
    {
        camera.startPreview();
        Log.d("PSA_SCAN","Starting PREVIEW");
    }



    private Camera.PictureCallback rawCallback = new Camera.PictureCallback()
    {
        @Override
        public void onPictureTaken(final byte[] data, Camera c)
        {
	        	/*Handler mainHandler = new Handler(ctx.getMainLooper());
	    		Runnable myRunnable = new Runnable() 
	    		{
	    			@Override
	    			public void run() 
	    			{
	    				startPreview();
	    				
	    			}
	    		};
	    		mainHandler.post(myRunnable);
	      	
	        	new Thread(new Runnable()
	        	{
	        		public void run() 
	        		{
	        			applyState(STATE.STATE_WAITING_RESULT,null);
	        			Camera.Parameters parameters = camera.getParameters();
	                    int w = parameters.getPictureSize().width;
	                    int h = parameters.getPictureSize().height;              
	                    float ratio = (float)h/(float)w;
	        		    Bitmap sbm = BitmapFactory.decodeByteArray(data, 0, data.length); 
	        		    MediaStore.Images.Media.insertImage(ctx.getContentResolver(), sbm, "test" , "test");
	        		    int orientation;
	        		    
	        		    
	        	        if(sbm.getHeight() < sbm.getWidth())
	        	        {
	        	        	sbm = Bitmap.createScaledBitmap(sbm,650,(int)(650*ratio), true);
	        	            orientation = 90;
	        	            //float scaleFactor = 650.0f/w;
	        	            Matrix matrix = new Matrix();
	        	            //matrix.postScale(scaleFactor, scaleFactor);  
	        	            matrix.postRotate(orientation);
	        	            
	        	            sbm = Bitmap.createBitmap(sbm, 0, 0,sbm.getWidth(),sbm.getHeight(), matrix, true);
	        	        } 
	        	        else 
	        	        {
	        	            orientation = 0;
	        	            sbm = Bitmap.createScaledBitmap(sbm,650,(int)(650*ratio), true);
	        	        }
	 
	        	        
	        	        Log.d("PSA-ratio","image scalled to : (" + 650 + " , " + (int)(650*ratio) + ")" );
	        	        
	        	        MediaStore.Images.Media.insertImage(ctx.getContentResolver(), sbm, "test" , "test");
	        			String str = DataPackage.SMPRS.recognition(sbm);
		        		sbm.recycle();
		        		gotResultFromReco(str);
	        		    
	        		};
	        	}).start();*/
        }
    };

    private Camera.PictureCallback jpegCallback = new Camera.PictureCallback()
    {
        @Override
        public void onPictureTaken(final byte[] data, Camera c)
        {
            Handler mainHandler = new Handler(ctx.getMainLooper());
            Runnable myRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    startPreview();

                }
            };
            mainHandler.post(myRunnable);

            new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    applyState(STATE.STATE_WAITING_RESULT,null);

                    Bitmap sbm = BitmapFactory.decodeByteArray(data, 0, data.length);

                    //SBM
                    int w = sbm.getWidth();
                    int h = sbm.getHeight();

                    float ratio = (float)w/(float)h;

                    Mat tempsbm = new Mat(new org.opencv.core.Size(sbm.getWidth(),sbm.getHeight()), CvType.CV_8UC4); // MAT

                    // BMP(sbm) to MAT(tempsbm)
                    org.opencv.android.Utils.bitmapToMat(sbm, tempsbm);
                    Imgproc.cvtColor(tempsbm, tempsbm, Imgproc.COLOR_BGR2RGB);

                    float ftargetSize = 500.0f;
                    int itargetSize= (int)ftargetSize;

                    // Resized MAT container
                    Mat dest = new Mat(new org.opencv.core.Size((double)itargetSize, (double)(ftargetSize*ratio)), CvType.CV_8UC4); //after resize

                    if(sbm.getHeight() < sbm.getWidth())
                    {
                        // MAT container for the rotated picture
                        Mat temp = new Mat(new org.opencv.core.Size(sbm.getHeight(),sbm.getWidth()), CvType.CV_8UC4); //after rotate
                        org.opencv.core.Core.flip(tempsbm.t(), temp, 1);
                        // Resize the MAT to the defined size
                        org.opencv.imgproc.Imgproc.resize(temp, dest, new org.opencv.core.Size((double)itargetSize, (double)(ftargetSize*ratio)),0,0,org.opencv.imgproc.Imgproc.INTER_AREA);
                        temp.release();
                    }
                    else
                    {
                        // Resize the MAT to the defined size
                        org.opencv.imgproc.Imgproc.resize(tempsbm, dest, new org.opencv.core.Size((double)itargetSize, (double)(ftargetSize*ratio)),0,0,org.opencv.imgproc.Imgproc.INTER_AREA);
                    }

                    tempsbm.release();
                    sbm.recycle();
                    try
                    {
                        sbm = Bitmap.createBitmap((int)itargetSize, (int)(ftargetSize*ratio), Bitmap.Config.ARGB_8888);
                        org.opencv.android.Utils.matToBitmap(dest,sbm);
                        dest.release();
                    }
                    catch (CvException e)
                    {
                        Log.d("MBDS-JMAGINE","Exception during bitmap creation :"+e.getMessage());
                    }

                    Log.d("PSA-ratio","image scalled to : (" + itargetSize + " , " + (int)(ftargetSize*ratio) + ")" );

                    String str;

                    if(JmagineApplication.location==null)
                    {
                        str = Tool.rs.recognition(sbm,-1,-1);
                    }
                    else
                    {
                        Log.d("JIMAGINE","latitude:"+JmagineApplication.location.getLatitude()+", longitude:"+JmagineApplication.location.getLongitude());
                        if(JmagineApplication.location.getLatitude()!=0 && JmagineApplication.location.getLongitude()!=0)
                        {
                            Log.d("JIMAGINE","latitude:"+JmagineApplication.location.getLatitude()+", longitude:"+JmagineApplication.location.getLongitude());
                            str = Tool.rs.recognition(sbm,JmagineApplication.location.getLatitude(),JmagineApplication.location.getLongitude());
                        }
                        else
                            str = Tool.rs.recognition(sbm,-1,-1);

                    }
                    gotResultFromReco(str);
                    sbm.recycle();
                };
            }).start();
        }
    };



    public void gotResultFromReco(String res)
    {

        if(res.equals(""))
        {
            applyState(STATE.STATE_NOTFOUND,null);
        }
        else
        {
            applyState(STATE.STATE_FOUND,res);
        }

    }

    public void onPause()
    {
        applyState(STATE.STATE_OFF, null);

    }

    public void onResume()
    {
        //setUpCamera();
    }



    public void waitLongAndFocus(final int time, final int ticket)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {

                try
                {
                    synchronized (this)
                    {
                        this.wait(time);
                        if(ticket== CameraLivePreview.currentTicket)
                            applyState(STATE.STATE_SCAN, null);
                        else
                            Log.d("PSA","this focus was cancelled");
                    }
                }
                catch (InterruptedException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }



            };
        }).start();

    }


    int count=0;
    private Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback()
    {
        @Override
        public void onShutter()
        {
        }
    };

    private Camera.AutoFocusCallback autoFocusCallback = new Camera.AutoFocusCallback()
    {
        @Override
        public void onAutoFocus(boolean success, Camera camera)
        {
            if(currentState.get()==STATE.STATE_SCAN)
            {
                if(success|| count==1)
                {
                    count=0;
                    applyState(STATE.STATE_FOCUS_SUCCESS,null);

                }
                else
                {
                    count++;
                    applyState(STATE.STATE_FOCUS_FAILED,null);

                }
            }
        }
    };

    boolean isAnimated =false;
    Animation animation;
    public void startAnim()
    {
        Handler mainHandler = new Handler(ctx.getMainLooper());

        Runnable myRunnable = new Runnable()
        {

            @Override
            public void run()
            {
                if(!isAnimated)
                {
                    isAnimated = true;
                    viseur.setImageResource(R.drawable.cercle1);
                    animation = AnimationUtils.loadAnimation(ctx, R.anim.rotateanim);
                    viseur.startAnimation(animation);
                }

            }
        };
        mainHandler.post(myRunnable);



    }
    public void stopAnim()
    {
        Handler mainHandler = new Handler(ctx.getMainLooper());

        Runnable myRunnable = new Runnable() {

            @Override
            public void run() {
                if(isAnimated)
                {
                    viseur.clearAnimation();
                    viseur.setImageResource(R.drawable.viseur);
                    isAnimated = false;
                }

            }
        };
        mainHandler.post(myRunnable);



    }

    public void switchCam()
    {
        applyState(STATE.STATE_SWITCH_CAM, null);
    }

    private void displayDocNotFoundAlert()
    {
        stopAnim();
        DialogInterface.OnClickListener fichepapListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                dialog.dismiss();
                applyState(STATE.STATE_INIT_DONE,null);
            }
        };

        DialogInterface.OnClickListener retry = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                applyState(STATE.STATE_INIT_DONE,null);
            }
        };

        AlertDialog.Builder alertbDialog = new AlertDialog.Builder(this.ctx);
        alertbDialog.setTitle("Scanner");
        alertbDialog.setMessage("Non reconnu");
        alertbDialog.setNegativeButton("Re-essayer", retry);
        alertbDialog.setIcon(R.drawable.info_icon_alert);
        alertbDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog)
            {
                applyState(STATE.STATE_INIT_DONE,null);

            }
        });
        alertbDialog.show();

    }



}