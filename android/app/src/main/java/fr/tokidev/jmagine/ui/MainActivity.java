package fr.tokidev.jmagine.ui;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.location.LocationManager;
import android.content.Context;
import android.content.Intent;
import android.app.AlertDialog;
import android.content.DialogInterface;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.RecognitionService;
import fr.tokidev.jmagine.tool.Tool;
import fr.tokidev.jmagine.tools.Globals;
import fr.tokidev.jmagine.ui.Tuto.*;
import fr.tokidev.jmagine.tools.PictureTools;
import fr.tokidev.jmagine.tools.JmagineApplication;

public class MainActivity extends FragmentActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get screen dimensions
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Globals.WIDTH = size.x;
        Globals.HEIGHT = size.y;




        Globals.PACKAGE_NAME = getApplicationContext().getPackageName();

        // Loading background
        Bitmap backgroundPic = PictureTools.decodeSampledBitmapFromResource(getResources(), R.drawable.fond_vieux_nice, Globals.WIDTH, Globals.HEIGHT);
        ViewPager layout = (ViewPager) findViewById(R.id.home);
        Drawable drawable = new BitmapDrawable(getResources(), backgroundPic);
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            layout.setBackgroundDrawable(drawable);
        } else {
            layout.setBackground(drawable);
        }

        // Création de la liste de Fragments que fera défiler le PagerAdapter
        List fragments = new Vector();

        // Ajout des Fragments dans la liste
        fragments.add(Fragment.instantiate(this, Tuto1.class.getName()));
        fragments.add(Fragment.instantiate(this, Tuto2.class.getName()));
        fragments.add(Fragment.instantiate(this,Tuto3.class.getName()));

        // Création de l'adapter qui s'occupera de l'affichage de la liste de
        // Fragments
        PagerAdapter mPagerAdapter = new MyPagerAdapter(super.getSupportFragmentManager(), fragments, getApplicationContext());

        ViewPager pager = (ViewPager) super.findViewById(R.id.home);
        // Affectation de l'adapter au ViewPager
        pager.setAdapter(mPagerAdapter);

        Runnable r = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    Tool.unzipAssets(MainActivity.this,100);
                    String path = MainActivity.this.getApplicationInfo().dataDir+"//triggers//";
                    int ret;
                    ret = Tool.rs.init(path, RecognitionService.OLD_NICE_DB);
                    Log.d("MBDS-JMAGINE","INIT RECO : "+ret);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        };

        new Thread(r).start();

        Tool.popItUp("VERSION BETA","Cette application est en cours de développement et peut présenter des dysfonctionnements.",MainActivity.this,null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void checkGps()
    {
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
        {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("L'application Jimagine nécessite d'avoir le gps actif, souhaitez vous l'activer maintenant ?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Quitter", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    protected void onResume() {
        checkGps();
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
    }

}
