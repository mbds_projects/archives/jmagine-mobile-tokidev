package fr.tokidev.jmagine.ui;

import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import fr.tokidev.jmagine.tools.JmagineApplication;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import fr.tokidev.jmagine.R;

public class ContenuPedagogiqueActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenu_pedagogique);

        // Création de la liste de Fragments que fera défiler le PagerAdapter
        List fragments = new Vector();
        Fragment fragment;

        Field[] drawables = fr.tokidev.jmagine.R.drawable.class.getFields();
        for (Field f : drawables) {
            try {
                if (f.getName().startsWith("patallaci"))
                {
                    fragment = new ImgItem();
                    Bundle bundle = new Bundle();
                    bundle.putString("drawable",f.getName());
                    fragment.setArguments(bundle);
                    fragments.add(fragment);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Création de l'adapter qui s'occupera de l'affichage de la liste de fragments
        PagerAdapter mPagerAdapter = new MyPagerAdapter(super.getSupportFragmentManager(), fragments, getApplicationContext());

        ViewPager pager = (ViewPager) super.findViewById(R.id.pedagogique_pager);
        // Affectation de l'adapter au ViewPager
        pager.setAdapter(mPagerAdapter);

        // Bis repetita pour les autres images

        // Création de la liste de Fragments que fera défiler le PagerAdapter
        List fragmentsBis = new Vector();
        Fragment fragmentBis;

        drawables = fr.tokidev.jmagine.R.drawable.class.getFields();
        for (Field f : drawables) {
            try {
                if (f.getName().startsWith("patin_troiani"))
                {
                    fragmentBis = new ImgItem();
                    Bundle bundle = new Bundle();
                    bundle.putString("drawable",f.getName());
                    fragmentBis.setArguments(bundle);
                    fragmentsBis.add(fragmentBis);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Création de l'adapter qui s'occupera de l'affichage de la liste de fragments
        PagerAdapter mPagerAdapterBis = new MyPagerAdapter(super.getSupportFragmentManager(), fragmentsBis, getApplicationContext());

        ViewPager pagerBis = (ViewPager) super.findViewById(R.id.pedagogique_pager_2);
        // Affectation de l'adapter au ViewPager
        pagerBis.setAdapter(mPagerAdapterBis);
    }



    @Override
    protected void onResume() {
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
    }
}
