package fr.tokidev.jmagine.ui;


import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.model.LatLng;

import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.model.Poi;
import fr.tokidev.jmagine.tool.Tool;

import android.location.LocationManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import fr.tokidev.jmagine.tools.JmagineApplication;

public class ScanPoiActivity extends ActionBarActivity
{
    ImageView imgReco;
    ImageView imgNfc;
    ImageView imgQr;
    ImageView imgGeo;

    RelativeLayout rlReco,rlNfc,rlQr,rlGeo;

    public static final String isNFCEnabled      = "isNFCEnabled";
    public static final String isQREnabled       = "isQREnabled";
    public static final String isSNSEnabled      = "isSNSEnabled";
    public static final String isGeolocEnabled   = "isGeolocEnabled";
    public static final String geoLocCoordinates = "geoLocCoordinates";

    private NfcAdapter mNfcAdapter;
    private LatLng latLngPoi;
    String poi;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_poi);

        imgReco=(ImageView)findViewById(R.id.imgrecovisuel);
        imgNfc =(ImageView)findViewById(R.id.imgnfc);
        imgQr  =(ImageView)findViewById(R.id.imgQR);
        imgGeo =(ImageView)findViewById(R.id.imggeo);

        rlReco=(RelativeLayout)findViewById(R.id.rlreco);
        rlNfc=(RelativeLayout)findViewById(R.id.rlnfc);
        rlQr=(RelativeLayout)findViewById(R.id.rlqr);
        rlGeo=(RelativeLayout)findViewById(R.id.rlgeo);

        boolean recoEnabled     = getIntent().getBooleanExtra(isSNSEnabled,false);
        boolean nfcEnabled      = getIntent().getBooleanExtra(isNFCEnabled,false);
        boolean qrenabled       = getIntent().getBooleanExtra(isQREnabled,false);
        boolean geolocenabled   = getIntent().getBooleanExtra(isGeolocEnabled,false);

        //get the next poi gps coordinate if set
        if(geolocenabled)
        {
            latLngPoi = getIntent().getParcelableExtra(geoLocCoordinates);
        }

        //if nothing is active, we activate all scan methods
        if(!nfcEnabled&&!qrenabled&&!recoEnabled&&!geolocenabled)
        {
            nfcEnabled = qrenabled=recoEnabled=geolocenabled=true;
        }
        //if the location to check for is null , we disable the geoloc scan
        if(latLngPoi==null)
            geolocenabled = false;

        //if nfc is down we disable the nfc scan
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if(mNfcAdapter==null)
        {
            nfcEnabled = false;
        }

        //show / hide scan methods accordingly
        if(!recoEnabled)
            rlReco.setVisibility(View.GONE);
        if(!nfcEnabled)
            rlNfc.setVisibility(View.GONE);
        if(!qrenabled)
            rlQr.setVisibility(View.GONE);
        if(!geolocenabled)
            rlGeo.setVisibility(View.GONE);


        imgReco.setOnClickListener(

                new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Vibrator vib = (Vibrator) ScanPoiActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vib.vibrate(500);
                if(checkGps())
                {
                    Intent i = new Intent(ScanPoiActivity.this, CameraActivity.class);
                    startActivity(i);
                }

            }
        });

        imgNfc.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Vibrator vib = (Vibrator) ScanPoiActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vib.vibrate(500);
                Intent i = new Intent(ScanPoiActivity.this,ScanNfcActivity.class);
                startActivity(i);

            }
        });

        imgQr.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Vibrator vib = (Vibrator) ScanPoiActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vib.vibrate(500);

                try
                {
                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes
                    startActivityForResult(intent, 0);
                }
                catch (Exception e)
                {
                    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
                    startActivity(marketIntent);
                }


            }
        });

        imgGeo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(ScanPoiActivity.this,GpsActivity.class);
                i.putExtra(ScanPoiActivity.geoLocCoordinates,latLngPoi);
                i.putExtra(POIActivity.EXTRA_IDPOI,getIntent().getStringExtra(POIActivity.EXTRA_IDPOI));
                startActivity(i);
            }
        });



    }



    boolean checkGps()
    {
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
        {
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }

    private void buildAlertMessageNoGps()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("L'application Jimagine nécessite d'avoir le gps actif, souhaitez vous l'activer maintenant ?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Quitter", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0)
        {
            if (resultCode == RESULT_OK)
            {
                String contents = data.getStringExtra("SCAN_RESULT");

                if(contents.contains("JMAGINE")||contents.contains("jmagine"))
                {
                    String[] strs = contents.split("::");
                    if(strs.length==3)
                    {
                        contents = strs[2];
                    }
                    else
                    {
                        strs = contents.split("-");
                        if(strs.length==3)
                        {
                            contents = strs[2];
                        }
                    }
                }

                if(android.text.TextUtils.isDigitsOnly(contents))
                {
                    Intent i = new Intent(ScanPoiActivity.this,POIActivity.class);
                    i.putExtra(POIActivity.EXTRA_IDPOI,contents);
                    ScanPoiActivity.this.startActivity(i);
                }
            }
            else
            {
                Tool.popItUp("Erreur","L'élément scanné n'a pas été reconnu.",ScanPoiActivity.this,null);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
    }

}
