package fr.tokidev.jmagine.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

public class GifDecoderView extends ImageView 
{
	private boolean mIsPlayingGif = false;    
	private GifDecoder mGifDecoder;    
	private Bitmap mTmpBitmap;    
	final Handler mHandler = new Handler(); 
	public boolean playable=false;
	Thread t;
	File curfile;
	
	public GifDecoderView(Context context, File f) throws FileNotFoundException 
	{    
		super(context);       
		playGif(f);
	}
	
	public GifDecoderView(Context ctx)
	{
		super(ctx);
	}
	
	public GifDecoderView(Context ctx, AttributeSet attrs)
	{
		super(ctx,attrs);
	}
	
	public GifDecoderView(Context ctx, AttributeSet attrs,int defStyle)
	{
		super(ctx,attrs,defStyle);
	}
	
	
	@Override
	protected void finalize() throws Throwable 
	{
		super.finalize();
		mIsPlayingGif=false;
		
	}
	
	public void stop()
	{
		mIsPlayingGif = false;
		Drawable drawable = this.getDrawable();
		if (drawable instanceof BitmapDrawable) 
		{
		    BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
		    Bitmap bitmap = bitmapDrawable.getBitmap();
		    if(bitmap!=null)
		    	bitmap.recycle();
		}
		
	}
	
	public void restart()
	{
		if(!mIsPlayingGif)
		{
			try {
				playGif(curfile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	final Runnable mUpdateResults = new Runnable() 
	{ 
			@Override
			public void run() 
			{ 
				if (mTmpBitmap != null && !mTmpBitmap.isRecycled()) 
				{ 
					GifDecoderView.this.setImageBitmap(mTmpBitmap); 
				} 
			} 
	};
	
	public void playGif(File f) throws FileNotFoundException 
	{ 
		curfile = f;
		FileInputStream stream = new FileInputStream(f);
		
		playable=true;
		mGifDecoder = new GifDecoder(); 
		mGifDecoder.read(stream); 
		mIsPlayingGif = true;
	
		t = new Thread(new Runnable() 
			{ 
				@Override
				public void run() 
				{ 
					try
					{
					
					final int n = mGifDecoder.getFrameCount(); 
					final int ntimes = mGifDecoder.getLoopCount(); 
					int repetitionCounter = 0; 
					do 
					{ 
						for (int i = 0; i < n; i++) 
						{ 
							mTmpBitmap = mGifDecoder.getFrame(i); 
							final int t = mGifDecoder.getDelay(i); 
							mHandler.post(mUpdateResults); 
							try 
							{ 
								Thread.sleep(t); 
							} catch (InterruptedException e) { e.printStackTrace(); } 
							if(!mIsPlayingGif)
							{
								i = n;
							}
						} 
						
						if(ntimes != 0) 
						{ 
							repetitionCounter ++; 
						} 
					 } 
					 while (mIsPlayingGif && (repetitionCounter <= ntimes));
					}
					catch (Exception ex) 
					{
						// TODO: handle exception
					}
					
					System.out.println("THREAD STOPPED !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
					if(mTmpBitmap!=null)
						mTmpBitmap.recycle();
					mGifDecoder = null;
				   }
			  }); 
			t.start();
	
	}
	
}
