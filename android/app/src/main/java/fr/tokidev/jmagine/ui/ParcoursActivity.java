package fr.tokidev.jmagine.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import fr.tokidev.jmagine.model.ElementInfo;
import fr.tokidev.jmagine.ElementParcoursAdapter;
import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.tools.JmagineApplication;

public class ParcoursActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcours);

        RecyclerView recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        ElementParcoursAdapter elementAdapter = new ElementParcoursAdapter(createParcours(), getApplicationContext());
        recList.setAdapter(elementAdapter);
    }



    @Override
    protected void onResume() {
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
    }

    public List<ElementInfo> createParcours()
    {
        List<ElementInfo> result = new ArrayList<ElementInfo>();

        ElementInfo elementInfoDaBouttau = new ElementInfo();
        elementInfoDaBouttau.title = "Restaurant Da Bouttau";
        elementInfoDaBouttau.background = "dabouttau";
        elementInfoDaBouttau.img = "dabouttau";
        elementInfoDaBouttau.description = "Apollinaire, ce sensuel, ce buveur, ce fumeur, cet amoureux de la vie, deux semaines avant sa mort, écrit encore  à son ami André Rouveyre venu sur la Côte en convalescence de guerre, « va manger des pâtes aux Ponchettes dans le vieux Nice, des sanguins à la cave de Falicon, du gibier chez Bouttau rue Colonna d’Istria, les raviolis à la blette chez la Bicon, au fin fond de la Promenade des Anglais. N’oublie pas de goûter à la pissaladière au marché ».";
        result.add(elementInfoDaBouttau);

        ElementInfo elementInfoFrancalin = new ElementInfo();
        elementInfoFrancalin.title = "Lou Fran Calin";
        elementInfoFrancalin.background = "fran_calin";
        elementInfoFrancalin.img = "fran_calin";
        elementInfoFrancalin.description= "Apollinaire, ce sensuel, ce buveur, ce fumeur, cet amoureux de la vie, deux semaines avant sa mort, écrit encore  à son ami André Rouveyre venu sur la Côte en convalescence de guerre, « va manger des pâtes aux Ponchettes dans le vieux Nice, des sanguins à la cave de Falicon, du gibier chez Bouttau rue Colonna d’Istria, les raviolis à la blette chez la Bicon, au fin fond de la Promenade des Anglais. N’oublie pas de goûter à la pissaladière au marché ».";
        result.add(elementInfoFrancalin);

        ElementInfo elementInfoCais = new ElementInfo();
        elementInfoCais.title = "Palais Caïs de Pierlas";
        elementInfoCais.background = "palais_cais";
        elementInfoCais.img = "palais_cais";
        elementInfoCais.description = "La famille Caïs (Cais ou Chais ou Cays ou Ciais ou Chiais), comtes de (ou « di ») Pierlas, est l’une des plus vieilles familles de Nice et de la Provence\n" +
                "\n" +
                "Leurs premiers faits documentés remontent à 1200. Elle vient de La Roche (Valdeblore), le village où se trouve la vieille maison familiale des Chiaïs (voir photo), et a été investie du fief de Pierlas et du titre comital en 1764.\n" +
                "\n" +
                "Depuis la fin xviiie siècle, elle était propriétaire de l’un des plus beaux palais du Vieux-Nice qui porte toujours son nom. Au cours des siècles, elle a fourni de nombreuses autorités civiles et militaires à la ville elle-même ainsi qu’à l'armée du duc de Savoie, incluant, entre 1795 et 1800, le comte Joseph-Marie qui s'est distingué au cours de la guerre contre les Français. Son fils, Hippolyte (1787-1868), officier et premier consul de Nice en 1820, était un sculpteur et un peintre d'une valeur certaine. Il conçut, entre autres choses, la façade de l'église de Cimiez où le tombeau familial se situe. Son neveu Eugène Caïs de Pierlas, (1842-1900), initialement peintre lui-même et auquel a été consacré une rue de Nice, était un historien important du comté de Nice.\n" +
                "\n" +
                "Le comte Hippolyte Caïs de Pierlas, amateur de plantes exotiques et le premier propagateur de palmiers à Nice, avait planté dans sa propriété du Ray, la villa Pierlas, dès 1837, des Chamaedorea elegans, C. sartorii, Phoenix sylvestris et Trachycarpus martianus. Il est à l'origine des activités d'acclimatation à Nice. Son domaine, racheté par la famille Chambrun, fut ensuite divisé et morcelé. L'actuel parc public Chambrun est une parcelle de l'ancienne propriété.\n" +
                "\n" +
                "Au xxe siècle, la famille Caïs s’est déplacée au Piémont. Du mariage de Joseph Caïs de Pierlas et Laura Mocenigo (descendante directe de l'ancienne famille ducale vénitienne) sont nés trois enfants : Maritza, Anna Maria (qui a épousé Eugenio Casagrande di Villaviera et qui a eu une fille -Maria Gregoria- mariée Jagher) et Alberto (Parme 1891-Nice 1973), dernier descendant direct et pianiste-compositeur d'un certain succès11.\n" +
                "\n" +
                "Aujourd'hui, ses branches directes vivent entre Gênes et Venise.\n" +
                "\n" +
                "Le nom de Caïs reste hautement symbolique dans le Comté Niçois car il représente la grande histoire avec toutes ses traditions et valeurs.";
        result.add(elementInfoCais);

        return result;
    }
}
