
package fr.tokidev.jmagine.ui;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.lang.Override;
import java.util.Arrays;
import fr.tokidev.jmagine.tools.JmagineApplication;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

import fr.tokidev.jmagine.R;

public class ScanNfcActivity extends Activity
{
    NfcAdapter adapter;
    PendingIntent nfcPendingIntent;
    IntentFilter[] writeTagFilter;
    ImageView imgcontact;

    // Animation
    Animation anim;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);
        adapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this,0, new Intent(this,getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP), 0);

        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilter = new IntentFilter[]{tagDetected,ndefDetected};


        imgcontact = (ImageView)findViewById(R.id.imgcontact);
        anim =  AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink);

        imgcontact.startAnimation(anim);

    }

    public void enableForegroundMode()
    {


        // foreground mode gives the current active application priority for reading scanned tags
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED); // filter for tags
        IntentFilter[] writeTagFilters = new IntentFilter[] {tagDetected};
        adapter.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null);
    }


    public void disableForegroundMode()
    {

        adapter.disableForegroundDispatch(this);
    }


    @Override
    public void onNewIntent(Intent intent)
    { // this method is called when an NFC tag is scanned

        String result="";
        // check for NFC related actions
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()))
        {

            String type = intent.getType();

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                result = readTag(tag);


        }
        else if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction()))
        {


            NdefMessage[] messages = null;
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsgs != null)
            {
                messages = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++)
                {
                    messages[i] = (NdefMessage) rawMsgs[i];
                }
            }
            if(messages!=null && messages[0] != null)
            {

                byte[] payload = messages[0].getRecords()[0].getPayload();
                for (int b = 0; b<payload.length; b++)
                    result += (char) payload[b];


            }
        }
        else
        {
            return;
        }

        if(result!=null && result.length()>0)
        {
            try
            {
                int res = Integer.parseInt(result);
                Intent i = new Intent(ScanNfcActivity.this,POIActivity.class);
                i.putExtra(POIActivity.EXTRA_IDPOI,result);
                startActivity(i);
                return;
            }
            catch(Exception ex)
            {

            }
        }

       this.finish();

    }


    public String readTag(Tag... params)
    {
        Tag tag = params[0];

        Ndef ndef = Ndef.get(tag);
        if (ndef == null)
        {
            // NDEF is not supported by this Tag.
            return null;
        }

        NdefMessage ndefMessage = ndef.getCachedNdefMessage();

        NdefRecord[] records = ndefMessage.getRecords();
        for (NdefRecord ndefRecord : records)
        {
            if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT))
            {
                try
                {
                    return readText(ndefRecord);
                }
                catch (UnsupportedEncodingException e)
                {
                    Log.e("JIMAGINE", "Unsupported Encoding", e);
                }
            }
        }

        return null;
    }

    private String readText(NdefRecord record) throws UnsupportedEncodingException
    {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */

        byte[] payload = record.getPayload();

        // Get the Text Encoding
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

        // Get the Language Code
        int languageCodeLength = payload[0] & 0063;

        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
        // e.g. "en"

        // Get the Text
        return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableForegroundMode();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableForegroundMode();
        JmagineApplication.JmagineInstance.onPause();
    }


}