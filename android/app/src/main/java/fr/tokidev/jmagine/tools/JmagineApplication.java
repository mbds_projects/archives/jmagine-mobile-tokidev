package fr.tokidev.jmagine.tools;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.Timer;
import java.util.TimerTask;

import fr.tokidev.jmagine.R;

/**
 * Created by Gav on 21/01/2015.
 */
public class JmagineApplication extends Application
{

    public static JmagineApplication JmagineInstance;

    public Typeface font;
    LocationManager lm;
    public static Location location;
    LatLng latLngPoi;
    LocationListener forwardListener=null;
    Timer t;

    public boolean shouldStopGps=false;

    @Override
    public void onCreate()
    {
        t = new Timer();
        JmagineInstance = this;
        super.onCreate();
        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        font = Typeface.createFromAsset(getAssets(), "fonts/Muli-Regular.ttf");
    }

    private final LocationListener locationListener = new LocationListener()
    {
        public void onLocationChanged(Location location)
        {
            if(location!=null)
            {
                JmagineApplication.location = location;

                if(forwardListener!=null)
                    forwardListener.onLocationChanged(location);

            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {

        }

        @Override
        public void onProviderEnabled(String provider)
        {

        }

        @Override
        public void onProviderDisabled(String provider)
        {

        }
    };

    public void setForwardListener(LocationListener loclist )
    {
        forwardListener = loclist;
    }

    public void removeListener()
    {
        forwardListener = null;
    }


    public void onResume()
    {
        if(t!=null) {
            t.cancel();
            t.purge();
        }
        shouldStopGps=false;
        Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_FINE);
        String provider = lm.getBestProvider(c,true);
        lm.requestLocationUpdates(provider, 2000, 10, locationListener);
    }


    public void onPause()
    {
        if(t!=null)
        {
            t.cancel();
            t.purge();
        }
        shouldStopGps=true;
        t=new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run()
            {
                if(shouldStopGps)
                {
                    lm.removeUpdates(locationListener);
                }
            }
        },5000);


    }


}
