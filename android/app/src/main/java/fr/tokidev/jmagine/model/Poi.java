package fr.tokidev.jmagine.model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

import fr.tokidev.jmagine.tool.Tool;

/**
 * Created by max on 10/04/15.
 */
public class Poi
{
    private String title;
    private String backgroundPic;
    private String lat;
    private String lng;
    private String address;
    private String content;
    private String next_poi;

    private boolean isNFCEnabled;
    private boolean isQREnabled;
    private boolean isSNSEnabled;
    private boolean isGeolocEnabled;

    public String getTitle()
    {
        return title;
    }

    public String getBackgroundPic()
    {
        return backgroundPic;
    }

    public String getLat()
    {
        return lat;
    }

    public String getLng()
    {
        return lng;
    }

    public String getAddress()
    {
        return address;
    }

    public String getContent()
    {
        return content;
    }

    public String getNext_poi()
    {
        return next_poi;
    }


    public boolean isNFCEnabled() {
        return isNFCEnabled;
    }

    public void setNFCEnabled(boolean isNFCEnabled) {
        this.isNFCEnabled = isNFCEnabled;
    }

    public boolean isQREnabled() {
        return isQREnabled;
    }

    public void setQREnabled(boolean isQREnabled) {
        this.isQREnabled = isQREnabled;
    }

    public boolean isSNSEnabled() {
        return isSNSEnabled;
    }

    public void setSNSEnabled(boolean isSNSEnabled) {
        this.isSNSEnabled = isSNSEnabled;
    }

    public boolean isGeolocEnabled() {
        return isGeolocEnabled;
    }

    public void setGeolocEnabled(boolean isGeolocEnabled) {
        this.isGeolocEnabled = isGeolocEnabled;
    }


    public void fillPoiFromXml(Document xml)
    {
        NodeList nl = xml.getElementsByTagName("poi");

        Element e = (Element) nl.item(0);

        this.title             = Tool.getXmlValue(e,"title");
        this.backgroundPic     = Tool.getXmlValue(e,"backgroundPic");
        this.lat               = Tool.getXmlValue(e,"lat");
        this.lng               = Tool.getXmlValue(e,"lng");
        this.address           = Tool.getXmlValue(e,"address");
        this.content           = Tool.getXmlValue(e,"content");
        this.next_poi          = Tool.getXmlValue(e,"next_poi");


        this.isGeolocEnabled    = Tool.getXmlValue(e,"isGeolocEnabled").equals("true");
        this.isSNSEnabled       = Tool.getXmlValue(e,"isSNSEnabled").equals("true");
        this.isQREnabled        = Tool.getXmlValue(e,"isQREnabled").equals("true");
        this.isNFCEnabled       = Tool.getXmlValue(e,"isNFCEnabled").equals("true");

    }

}
