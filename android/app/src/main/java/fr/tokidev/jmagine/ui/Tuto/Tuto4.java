package fr.tokidev.jmagine.ui.Tuto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import fr.tokidev.jmagine.R;
import fr.tokidev.jmagine.tools.JmagineApplication;
import fr.tokidev.jmagine.ui.HomeActivity;

/**
 * Created by Gav on 19/01/2015.
*/
public class Tuto4 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mView = inflater.inflate(R.layout.tuto_4, container, false);

        Button mButton = (Button) mView.findViewById(R.id.start);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tuto4.this.getActivity(), HomeActivity.class);
                startActivity(intent);
            }
        });

        ((TextView)mView.findViewById(R.id.tuto_text)).setTypeface(((JmagineApplication)this.getActivity().getApplication()).font);
        ((TextView)mView.findViewById(R.id.start)).setTypeface(((JmagineApplication)this.getActivity().getApplication()).font);

        return mView;
    }
}