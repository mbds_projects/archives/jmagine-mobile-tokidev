package fr.tokidev.jmagine.ui;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import fr.tokidev.jmagine.tools.JmagineApplication;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import fr.tokidev.jmagine.R;

public class ContenuHistoriqueActivity extends ActionBarActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenu_historique);


        // Création de la liste de Fragments que fera défiler le PagerAdapter
        List fragments = new Vector();
        Fragment fragment;

        Field[] drawables = fr.tokidev.jmagine.R.drawable.class.getFields();
        for (Field f : drawables) {
            try
            {
                if (f.getName().startsWith("historique_"))
                {
                    fragment = new ImgItem();
                    Bundle bundle = new Bundle();
                    bundle.putString("drawable",f.getName());
                    fragment.setArguments(bundle);
                    fragments.add(fragment);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        JmagineApplication.JmagineInstance.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JmagineApplication.JmagineInstance.onPause();
    }
}
