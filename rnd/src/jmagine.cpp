/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine.cpp - Main desktop module.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine.hpp"
#include "jmagine_geoloc.hpp"
#include "jmagine_recognition.hpp"
#include "jmagine_parameters.hpp"
#include "jmagine_data_elements.hpp"


static jmagine_handle_t hnd;


int main(int argc, char** argv)
{
  Mat query;
  int rv;
  double t, t_total;
  jmagine_query_t results;
  vector<jmagine_reco_candidate_t> candidates;
  string res_str;
  float gps_lon, gps_lat;
  size_t i;
  int dbid;

  if(argc!=5)
  {
    LOGI("Usage: %s [query image] [gps lat] [gps lon] [db id]", argv[0]);
    return(-1);
  }

  dbid=atoi(argv[4]);
  gps_lat=atof(argv[2]);
  gps_lon=atof(argv[3]);
  query=imread(argv[1], CV_LOAD_IMAGE_COLOR);
  if(!query.data)
  {
    LOGE("Error: unable to load image '%s'.", argv[1]);
    return(-1);
  }

  LOGI("Input GPS coords: [%.08f|%.08f]", gps_lat, gps_lon);
#ifdef TKDV_DEBUG
  for(i=0; i<JMAGINE_NB_ELMTS; ++i)
  {
    double dist=jmagine_get_dist_gps_coords_meters(gps_lat, gps_lon, JMAGINE_ELMT2GPS[i][0], JMAGINE_ELMT2GPS[i][1]);
    LOGD("  - Dist from target #%lu ('%s'): ~%.04f meters.", i, JMAGINE_ELMT2RV[i][0].c_str(), dist);
  }
#endif

  string path_triggers="";
  if(jmagine_init(hnd, path_triggers.c_str(), dbid))
  {
    LOGE("Couldn't initialize library, exiting.");
    return(-1);
  }

  t_total=(double)getTickCount();
  t=(double)getTickCount();
  // Perform analysis.
  rv=jmagine_analyze_image(hnd, query, results);
  t=((double)getTickCount() - t)/getTickFrequency();
#ifdef TKDV_DEBUG
  LOGD(" ");
#endif
  LOGI("Analysis, return value: %d.", rv);
  LOGI("Time taken: %.04f seconds.", t);
  LOGI(" ");

  t=(double)getTickCount();
  // Perform matching from analysis results.
  rv=jmagine_match_image(hnd, results, candidates);
  t=((double)getTickCount() - t)/getTickFrequency();
  LOGI("Matching, return value: %d.", rv);
  LOGI("Time taken: %.04f seconds.", t);
  LOGI("Number of candidates: %lu.", candidates.size());

  if(candidates.size()>0)
  {
    vector<jmagine_reco_candidate_t> candidates_dist;
    LOGI("Candidates:");
    for(i=0; i<candidates.size(); ++i)
    {
      double dist=jmagine_get_dist_gps_coords_meters(gps_lat, gps_lon, JMAGINE_ELMT2GPS[candidates[i].element_id][0], JMAGINE_ELMT2GPS[candidates[i].element_id][1]);
      LOGI("  - #%lu: model %s, score %.02f, distance to candidate model: ~%.02f meters.", i+1, candidates[i].name.c_str(), candidates[i].score, dist);
      if(JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS<0.)
      {
        LOGD("    => Distance check disabled, pass the filter.");
      }
      else
      {
        if(((gps_lat<0.) || (gps_lon<0.)) && (JMAGINE_GPS_CHECK_ALLOW_NOCHECK))
        {
          LOGD("    => Pass distance check by default (at least one negative gps coord).");
          candidates_dist.push_back(candidates[i]);
        }
        else if((gps_lat<0.) || (gps_lon<0.))
        {
          LOGD("    => Doest NOT pass distance check (at least one negative gps coord).");
        }
        else if(dist<JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS)
        {
          LOGD("    => Pass distance check (max %.02f meters).", JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS);
          candidates_dist.push_back(candidates[i]);
        }
        else
        {
          LOGD("    => Does NOT pass distance check (max %.02f meter).", JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS);
        }
      }
    }
    if(JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS>0.)
    {
      candidates.clear();
      candidates.insert(candidates.end(), candidates_dist.begin(), candidates_dist.end());
    }
  }

  double max_score=-4242424;
  int max_score_idx=-1;
  for(i=0; i<candidates.size(); ++i)
  {
    if(candidates[i].score>max_score)
    {
      max_score=candidates[i].score;
      max_score_idx=i;
    }
  }

  string res="";
  if(max_score_idx!=-1)
  {
#ifdef TKDV_DESKTOP
    res=candidates[max_score_idx].name;
#else
    res=candidates[max_score_idx].id;
#endif
  }

  t_total=((double)getTickCount() - t_total)/getTickFrequency();
  LOGI("TIME: [%.04f].", t_total);
  LOGI("SCORE: [%.04f].", max_score);
  double limit=JMAGINE_MATCHING_GOOD_MATCH_MINSCORE;
  if(JMAGINE_MATCHING_ENABLE_GOOD_MATCH_FILTERING)
  {
    if((JMAGINE_MATCHING_F2F_GOOD_MATCH_TOLERANCE) && (hnd.previous_empty))
    {
      limit=JMAGINE_MATCHING_F2F_GOOD_MATCH_MINSCORE;
      hnd.previous_empty=false;
    }
    if(max_score>=limit)
    {
      LOGI("RES: [%s].", res.c_str());
    }
    else
    {
      LOGI("RES: []");
      if(!hnd.previous_empty)
        hnd.previous_empty=true;
    }
  }
  else
  {
    LOGI("RES: [%s].", res.c_str());
  }

#ifdef TKDV_DEBUG
  waitKey(0);
#endif

  return(0);
}
