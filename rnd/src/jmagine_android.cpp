/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine.cpp - Main android module (JNI links, etc.)
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine_android.hpp"
#include "jmagine_recognition.hpp"

static jmagine_handle_t hnd[JMAGINE_NB_DATABASES];
static int current_db_id=-1;
//static int nbb=0;

// Main recognition function, called by jmagine_recognition.
string jmagine_perform_recognition(const Mat& query, double gps_lat, double gps_lon);
JNIEXPORT jstring JNICALL jmagine_recognition(JNIEnv* env, jobject thisse, jdouble gps_lat, jdouble gps_lon, jlong img_in);
JNIEXPORT jint JNICALL jmagine_android_init(JNIEnv* env, jobject thisse, jstring base_path, jint db_id);


#define JMAGINE_JNI_NUM_METHODS  2
static const JNINativeMethod JMAGINE_JNI_METHODS[]={ {"jmagine_recognition",
                                                  "(J)Ljava/lang/String;",
                                                  (void*)jmagine_recognition},
                                                 {"jmagine_android_init",
                                                  "(Ljava/lang/String;I)I",
                                                  (void*)jmagine_android_init} };


// Called when the native lib is loaded.
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
  jclass parent_class;
  JNIEnv* env=NULL;

  if(vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6)!=JNI_OK)
    return(-1);
  
  parent_class=env->FindClass(JAVA_JMAGINE_CLASSNAME);
  if(parent_class==NULL)
  {
    LOGE("Native registration error: unable to find class [%s].\n", JAVA_JMAGINE_CLASSNAME);
    return(-1);
  }
 
  if(env->RegisterNatives(parent_class, JMAGINE_JNI_METHODS, JMAGINE_JNI_NUM_METHODS)<0)
  {
    LOGE("Native registration error: unable to register methods in class [%s].\n", JAVA_JMAGINE_CLASSNAME);
    return(-1);
  }

  return(JNI_VERSION_1_6);
}




JNIEXPORT jstring JNICALL jmagine_recognition(JNIEnv* env, jobject thisse, jdouble gps_lat, jdouble gps_lon, jlong addr_in)
{
  const Mat& query=*((Mat*)addr_in);
  string res;

  //  nbb=nbb+1;
  //  char tmp[500];
  //  snprintf(tmp, 500, "/sdcard/%03d_smc.png", nbb);
  //  imwrite(tmp, query);
  res=jmagine_perform_recognition(query, gps_lat, gps_lon);

  jstring str_ret=env->NewStringUTF(res.c_str());
  return(str_ret);
}




JNIEXPORT jint JNICALL jmagine_android_init(JNIEnv* env, jobject thisse, jstring base_path, jint db_id)
{
  jint rv=0;
  string path=env->GetStringUTFChars(base_path, NULL);

  if((db_id<0) || (db_id>=JMAGINE_NB_DATABASES))
    return(-1);
  if(!hnd[db_id].initialized)
  {
    if(jmagine_init(hnd[db_id], path.c_str(), db_id))
      return(-1);
  }
  current_db_id=db_id;

  return(0);
}




// Main recognition function, called by jmagine_recognition.
string jmagine_perform_recognition(const Mat& query, double gps_lat, double gps_lon)
{
  jmagine_query_t results;
  int rv;
  size_t i;
  vector<jmagine_reco_candidate_t> candidates;

 if((current_db_id<0) || (current_db_id>=JMAGINE_NB_DATABASES))
    return(string(""));

  rv=jmagine_analyze_image(hnd[current_db_id], query, results);
  if(rv)
    return(string(""));
  rv=jmagine_match_image(hnd[current_db_id], results, candidates);
  if(rv)
    return(string(""));

  double max_score=-4242424;
  int max_score_idx=-1;
  for(i=0; i<candidates.size(); ++i)
  {
    if(candidates[i].score>max_score)
    {
      max_score=candidates[i].score;
      max_score_idx=i;
    }
  }

  string res="";
  if(max_score_idx!=-1)
  {
    res=candidates[max_score_idx].id;
  }
  else
    return(res);

  double limit=JMAGINE_MATCHING_GOOD_MATCH_MINSCORE;
  if(JMAGINE_MATCHING_ENABLE_GOOD_MATCH_FILTERING)
  {
    if((JMAGINE_MATCHING_F2F_GOOD_MATCH_TOLERANCE) && (hnd[current_db_id].previous_empty))
    {
      limit=JMAGINE_MATCHING_F2F_GOOD_MATCH_MINSCORE;
      hnd[current_db_id].previous_empty=false;
    }
    if(max_score>=limit)
    {
      // Do nothing.
    }
    else
    {
      res="";
      if(!hnd[current_db_id].previous_empty)
        hnd[current_db_id].previous_empty=true;
    }
  }
  else
  {
    // Do nothing.
  }

  return(res);
}
