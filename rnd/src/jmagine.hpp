/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine.hpp - Header main desktop module.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_HPP
#define __TOKIDEV_JMAGINE_HPP

#include <cstring>
#include <cstdio>
using namespace std;

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>

#define TKDV_SURF   1
#ifdef TKDV_SURF
#include <opencv2/nonfree/nonfree.hpp>
#endif
using namespace cv;


// Android defines TKDV_ANDROID, IOS defines TKDV_IOS, and both define TKDV_EMBEDDED
#ifndef TKDV_DESKTOP
#define TKDV_DESKTOP
#endif
#ifdef TKDV_EMBEDDED
#undef TKDV_EMBEDDED
#endif

#ifndef CV_PI
#define CV_PI   3.1415926
#endif


#define  LOGD(...)  fprintf(stdout, "[DEBUG] ");fprintf(stdout, __VA_ARGS__);fprintf(stdout, "\n")
#define  LOGI(...)  fprintf(stdout, "[INFO] ");fprintf(stdout, __VA_ARGS__);fprintf(stdout, "\n")
#define  LOGW(...)  fprintf(stdout, "[WARNING] ");fprintf(stdout, __VA_ARGS__);fprintf(stdout, "\n")
#define  LOGE(...)  fprintf(stdout, "[ERROR] ");fprintf(stdout, __VA_ARGS__);fprintf(stdout, "\n")


#endif //__TOKIDEV_JMAGINE_HPP
