/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_features_filtering.hpp - Header features filtering module. Module providing filtering
                                   funtions for descriptors (features) matches.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_FEATURES_FILTERING_HPP
#define __TOKIDEV_JMAGINE_FEATURES_FILTERING_HPP

#ifdef TKDV_ANDROID
#include "jmagine_android.hpp"
#else
#include "jmagine.hpp"
#endif
#include "jmagine_features.hpp"
#include "jmagine_parameters.hpp"


// From a knn match to the library of triggers (for example the result of a general match on the triggers flann index),
// this provides nb_best (or less) best candidates from the triggers.
// This function simply select the n matches that have the most matches occurences.
// Returns 0 on success, -1 on failure.
int jmagine_features_filter_top_candidates_knn_occurences(jmagine_features_handle_t& hnd,
                                                          const vector<vector<DMatch> >& matches,
                                                          vector<jmagine_candidate_match_t>& candidates,
                                                          unsigned int nb_best, int algorithm_id);

// From a knn match to the library of triggers (for example the result of a general match on the triggers flann index),
// this provides nb_best (or less) best candidates from the triggers.
// This function starts by selecting the n top matches for each of the triggers. If there are enough of those, it will
// compute the mean of the distances to select the first t candidates.
// Then, it goes through the rest and simply filter by occurences.
// Returns 0 on success, -1 on failure.
int jmagine_features_filter_top_candidates_knn_score(jmagine_features_handle_t& hnd,
                                                     const vector<vector<DMatch> >& matches,
                                                     vector<jmagine_candidate_match_t>& candidates,
                                                     unsigned int nb_best, int algorithm_id);

// Simple threshold filtering. This extract matches using a simple threshold on the distance (dist<min_dist*2).
// If dist_min==-1, then the minimum distance is computed from the specified matches. If not, the specified
// value is used.
// Returns 0 on success, something else on failure.
int jmagine_features_filter_simple_threshold(const vector<DMatch>& matches, vector<DMatch>& filtered_matches,
                                             double threshold, int dist_min=-1);

// Adjusted threshold filtering. This does basically the same thing as simple_threshold, but this function tries to
// pinpoint a good value for a filtering threshold by taking into account min_dist *and* max_dist.
// Thus, matches pass if they have: dist<min_dist+((max_dist-min_dist)/threshold).
// If dist_min==-1 and dist_max==-1, the function will pinpoint the min/max values from matches. If not, it takes the
// provided values instead.
// Returns 0 on success, something else on failure.
int jmagine_features_filter_adjusted_threshold(const vector<DMatch>& matches, vector<DMatch>& filtered_matches,
                                               double threshold, int dist_min=-1, int dist_max=-1);

// Ratio filtering. This uses Lowe's ratio method as described in his paper.
// The function takes knn matches. The knn value must be two (or more, but higher dimensions will be ignored).
// Basically, a knn match passes if knn[0].dist/knn[1].dist is less than ratio (knn[0] passes).
// In other words, a knn match that has another keypoint match with a close distance is to be ignored, because it means
// that this match is likely to be a false positive.
// Returns 0 on success, something else on failure.
int filter_matches_ratio(const vector<vector<DMatch> >& knnmatches, vector<DMatch>& filtered_matches, double ratio);

// Ratio filtering, but will return all knn candidates in the filtered matches, not just the first one.
// In other words, if (knn[0].dist/knn[1].dist)<ratio, then knn[0] *and* knn[1] go through the filter.
// Also, if there are more than two knn matches, they also go through.
// Returns 0 on success, something else on failure.
int filter_matches_ratio_knn(const vector<vector<DMatch> >& knnmatches, vector<vector<DMatch> >& filtered_matches,
                             double ratio);

// Crosscheck filtering. Will make sure that a knn match from the query to the model has the same knn match between
// the model and the query. knn is the knn value - it can be as low as 1 and as high as needed (the higher the slower, and
// also there should be a value for which any increase produce negligible improvements).
// Returns 0 on success, something else on failure.
int jmagine_features_filter_crosscheck(const vector<vector<DMatch> >& matches_q2m,
                                       const vector<vector<DMatch> >& matches_m2q, vector<DMatch>& filtered_matches);

// Homography-based filtering. Will try to find a coherent homography from the specified matches and features, and
// discard outliers using either RANSAC or LMEDS (method).
// This can take a long time. The found homography is stored in homography.
// Returns 0 on success, -1 on failure.
int jmagine_features_filter_homography(const vector<DMatch>& matches, const vector<KeyPoint>& query_features,
                                       const vector<KeyPoint>& model_features, int method,
                                       vector<DMatch>& filtered_matches, Mat& homography);


#endif //__TOKIDEV_JMAGINE_FEATURES_FILTERING_HPP
