/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_debug.cpp - Debug module header.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_DEBUG_HPP
#define __TOKIDEV_JMAGINE_DEBUG_HPP

#ifdef TKDV_ANDROID
#include "jmagine_android.hpp"
#else
#include "jmagine.hpp"
#endif
#include "jmagine_recognition.hpp"


// Output a listing of the specified features match candidates.
// If padding is not empty, the specified string will be added at the beginning of each line.
void jmagine_debug_output_probable_matches(const jmagine_handle_t& hnd, const vector<jmagine_candidate_match_t>& candidates,
                                           const string& padding);

// Display a view where the probable features match are drawed.
// They are drawn alternatively in green, blue and red.
// If wait_key is true, the function will wait for the user to press a key.
void jmagine_debug_show_probable_matches(const Mat* query, const jmagine_handle_t& hnd,
                                         const vector<jmagine_candidate_match_t>& candidates, const string& window_name,
                                         bool wait_key);

// Outputs details on all triggers for all algorithms for debug.
void jmagine_debug_output_triggers(const jmagine_features_handle_t& hnd);



#endif //__TOKIDEV_JMAGINE_DEBUG_HPP
