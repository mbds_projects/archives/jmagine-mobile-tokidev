/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_debug.cpp - Debug module.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#include "jmagine_debug.hpp"
#include "jmagine_utils.hpp"




// Output a listing of the specified features match candidates.
// If padding is not empty, the specified string will be added at the beginning of each line.
void jmagine_debug_output_probable_matches(const jmagine_handle_t& hnd, const vector<jmagine_candidate_match_t>& candidates,
                                           const string& padding)
{
  size_t i;
  string pad;
  string name_model;

  if(padding.length()>0)
    pad=padding;
  for(i=0; i<candidates.size(); ++i)
  {
    name_model=hnd.features.triggers[candidates[i].algorithm_id][candidates[i].trigger_id].name;
    LOGD("%s- Candidate #%lu: model '%s', %d matches, score %.02f.", pad.c_str(), i+1, name_model.c_str(),
         candidates[i].nb_matches, candidates[i].score);
    if(candidates[i].shape_nb_faces!=-1)
    {
      LOGD("%s  Topology information: %d faces, area %.02f, convexity %d.", pad.c_str(), candidates[i].shape_nb_faces,
           candidates[i].shape_area, candidates[i].shape_is_convex);
      if(candidates[i].shape_nb_faces==4)
      {
        LOGD("%s  This is a rectangle, irregularity: %.02f.", pad.c_str(), candidates[i].shape_irregularity);
      }
    }
  }
}




// Display a view where the probable features match are drawed.
// They are drawn alternatively in green, blue and red.
// If wait_key is true, the function will wait for the user to press a key.
void jmagine_debug_show_probable_matches(const Mat* query, const jmagine_handle_t& hnd,
                                         const vector<jmagine_candidate_match_t>& candidates, const string& window_name,
                                         bool wait_key)
{
  Mat display=query->clone();
  size_t i, j;
  cv::Point pt1, pt2;
  Scalar colors[3]={ Scalar(0, 255, 0), Scalar(255, 0, 0), Scalar(0, 0, 255) };
  Scalar color;

  for(i=0; i<candidates.size(); ++i)
  {
    color=colors[i%3];
    for(j=0; j<candidates[i].shape_contour.size(); ++j)
    {
      pt1=candidates[i].shape_contour[j];
      pt2=candidates[i].shape_contour[(j+1)%(candidates[i].shape_contour.size())];
      line(display, pt1, pt2, color, 1, CV_AA);
    }
  }

  imshow(window_name, display);
  if(wait_key)
    waitKey(0);
}




// Outputs details on all triggers for all algorithms for debug.
void jmagine_debug_output_triggers(const jmagine_features_handle_t& hnd)
{
  int i;
  unsigned int j;

  LOGD("Triggers loaded:");
  for(i=0; i<JMAGINE_NB_ALGORITHMS_TO_USE; ++i)
  {
    LOGD("  Algorithm %02X:", JMAGINE_ALGORITHMS_TO_USE[i]);
    for(j=0; j<hnd.triggers[i].size(); ++j)
    {
      LOGD("    Trigger #%d: name '%s', element correspondance id '%d', %lu keypoints, %d descriptors of size %dB.", j+1, hnd.triggers[i][j].name.c_str(),
           hnd.triggers[i][j].element_id, hnd.triggers[i][j].keypoints.size(), hnd.triggers[i][j].descriptors.rows, hnd.triggers[i][j].descriptors.cols);
    }
  }
}
