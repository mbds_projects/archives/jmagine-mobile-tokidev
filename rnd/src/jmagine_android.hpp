/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine.cpp - Main android module (JNI links, etc.)
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_ANDROID_HPP
#define __TOKIDEV_JMAGINE_ANDROID_HPP


#include <cstring>
#include <cstdio>
#include <jni.h>
#include <android/log.h>

#define JMAGINE_SURF 1

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>
#ifdef JMAGINE_SURF
#include <opencv2/nonfree/nonfree.hpp>
#endif

using namespace cv;
using namespace std;

// Android defines JMAGINE_ANDROID and JMAGINE_EMBEDDED.
// Desktop defines JMAGINE_DESKTOP.
#ifndef JMAGINE_ANDROID
#define JMAGINE_ANDROID
#endif
#ifndef JMAGINE_EMBEDDED
#define JMAGINE_EMBEDDED
#endif
#ifdef JMAGINE_DESKTOP
#undef JMAGINE_DESKTOP
#endif

#ifndef CV_PI
#define CV_PI   3.1415926
#endif

// This *needs* to be set to the complete class path of the calling class (the one that
// actually loads the native library).
#define JAVA_JMAGINE_CLASSNAME  "com/tokidev/jmagine/RecognitionService"

#define  LOG_TAG    "JMagine::RecognitionService"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)


// Main recognition function, called by the objective C side.
extern "C"
{
  JNIEXPORT jstring JNICALL psa_smp_recognition(JNIEnv* env, jobject thisse, jdouble gps_lat, jdouble gps_lon, jlong img_in);
  JNIEXPORT jint JNICALL psa_smp_android_init(JNIEnv* env, jobject thisse, jstring base_path, jint db_id);
}


#endif //__TOKIDEV_JMAGINE_ANDROID_HPP
