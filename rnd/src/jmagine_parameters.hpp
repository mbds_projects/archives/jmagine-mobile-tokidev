/*
  Projet JMagine - Application touristique avec reconnaissance de batiments
  PROTOTYPE
  Tokidev SAS
  ---
  Module de reconnaissance visuelle.
  ---
  jmagine_parameters.hpp - Parameters header.
  ---
  Benjamin Renaut <renaut.benjamin@tokidev.fr>
  Copyright (C) 2015 Tokidev SAS
*/
#ifndef __TOKIDEV_JMAGINE_PARAMETERS_HPP
#define __TOKIDEV_JMAGINE_PARAMETERS_HPP

#define JMAGINE_NB_DATABASES   1

#ifdef JMAGINE_TESTING
#define JMAGINE_NB_TRIGGERS_OLD_NICE  102
#else
#define JMAGINE_NB_TRIGGERS_OLD_NICE  102
#endif


const int JMAGINE_GENERAL_BLUR_KERNEL=3;
// Use 0 for gaussian with a JMAGINE_GENERAL_BLUR_KERNEL kernel, 1 for upscale/downscale based blur.
const int JMAGINE_GENERAL_BLUR_MODE=0;
const int JMAGINE_CANNY_LOW_THRESH=60;
const int JMAGINE_CANNY_HIGH_THRESH=160;

// If set to -1: disabled.
const double JMAGINE_GPS_CHECK_MAX_DISTANCE_METERS=200.;
// If set to true: if the input GPS coords are set to -1., then ignore the distance check.
const bool JMAGINE_GPS_CHECK_ALLOW_NOCHECK=true;

// FEATURES
#define JMAGINE_ALGORITHM_FAST_FREAK  0
#define JMAGINE_ALGORITHM_SURF_SURF  1
#define JMAGINE_FLANN_TYPE_KDTREE  0
#define JMAGINE_FLANN_TYPE_LSH  1
// Detection/extraction.
#define JMAGINE_NB_ALGORITHMS_TO_USE  2
const int JMAGINE_ALGORITHMS_TO_USE[JMAGINE_NB_ALGORITHMS_TO_USE]={JMAGINE_ALGORITHM_SURF_SURF, JMAGINE_ALGORITHM_FAST_FREAK};
const bool JMAGINE_FEATURES_NORMALIZE_QUERY=true;   // Wether or not to normalize the query images.
const bool JMAGINE_FEATURES_BLUR_QUERY=true;   // Wether or not to blur the query images.
const int JMAGINE_FEATURES_BLUR_QUERY_KERNEL=1.;   // Value of the gaussian kernel if blur enabled.
const double JMAGINE_FEATURES_SURF_HTHRESHOLD=800.;   // SURF: hessian threshold.
const int JMAGINE_FEATURES_SURF_NB_OCTAVES=6;    // SURF: number of octaves.
const int JMAGINE_FEATURES_SURF_NB_OCTAVE_LAYERS=4;    // SURF: number of octave layers.
const bool JMAGINE_FEATURES_SURF_UPRIGHT=false;   // SURF: omit orientation for features.
const bool JMAGINE_FEATURES_SURF_EXTENDED=true;   // SURF: wether or not to use 128-based descriptors.
const int JMAGINE_FEATURES_FAST_THRESHOLD=35;   // Threshold for FAST.
const bool JMAGINE_FEATURES_FREAK_NORM_ORIENTATION=true;  // FREAK: enable orientation normalization.
const bool JMAGINE_FEATURES_FREAK_NORM_SCALE=true;  // FREAK: enable scale normalization.
const double JMAGINE_FEATURES_FREAK_PSCALE=15.;  // FREAK: pattern scale.
const int JMAGINE_FEATURES_FREAK_N_OCTAVES=6;    // FREAK: number of octaves.
const bool JMAGINE_FEATURES_DYNAMIC_DETECTION[JMAGINE_NB_ALGORITHMS_TO_USE]={ false, false };  // Wether or not to enable dynamic detection.
const size_t JMAGINE_FEATURES_DETECTION_MAX_FEATURES[JMAGINE_NB_ALGORITHMS_TO_USE]={ 400, 400 };   // Max features for dynamic detection.
const size_t JMAGINE_FEATURES_DETECTION_MIN_FEATURES[JMAGINE_NB_ALGORITHMS_TO_USE]={ 80, 80 };   // Min features for dynamic detection.
const int JMAGINE_FEATURES_DETECTION_STEP_THRESH_FAST=2;   // FAST: delta to apply to the threshold at each step.
const int JMAGINE_FEATURES_DETECTION_MAX_STEPS_FAST=5;    // FAST: max number of steps for dynamic detection.
const int JMAGINE_FEATURES_DETECTION_STEP_THRESH_SURF=20;   // SURF: delta to apply to the hessian threshold at each step.
const int JMAGINE_FEATURES_DETECTION_MAX_STEPS_SURF=5;    // SURF: max number of steps for dynamic detection.
// Matching.
const bool JMAGINE_FEATURES_MATCHING_USE_FLANN[JMAGINE_NB_ALGORITHMS_TO_USE]={ true, true };    // Wether or not to use FLANN for matching (if not: bruteforce).
const int JMAGINE_FEATURES_MATCHING_BRUTEFORCE_NORM[JMAGINE_NB_ALGORITHMS_TO_USE]={ NORM_L2, NORM_HAMMING };   // Dist norm to use when bruteforcing.
const int JMAGINE_FEATURES_FLANN_TYPE[JMAGINE_NB_ALGORITHMS_TO_USE]={ JMAGINE_FLANN_TYPE_KDTREE, JMAGINE_FLANN_TYPE_LSH };   // Type of flann model to use.
const int JMAGINE_FEATURES_MATCHING_KNN_TRIGGERS[JMAGINE_NB_ALGORITHMS_TO_USE]={ 2, 2 };   // Number of matches to get from the general library matching.
const int JMAGINE_FEATURES_MATCHING_MODE_SCORE[JMAGINE_NB_ALGORITHMS_TO_USE]={ 1, 1 };   // 0 to get the initial candidates from nb_occ, 1 for the lower mean dist.
const bool JMAGINE_FEATURES_MATCHING_FILTER_MEAN_DIST[JMAGINE_NB_ALGORITHMS_TO_USE]={ true, true };   // Wether or not to consider only part of the dist for mean.
const double JMAGINE_FEATURES_MATCHING_DIST_ATHRESHOLD[JMAGINE_NB_ALGORITHMS_TO_USE]={ 7., 7. };  // Threshold for dist filtering before computing the mean.
const unsigned int JMAGINE_FEATURES_LSH_NB_TABLES[JMAGINE_NB_ALGORITHMS_TO_USE]={ 20, 20 };   // LSH: number of hash tables to use (10-30 usually).
const unsigned int JMAGINE_FEATURES_LSH_KEY_SIZE[JMAGINE_NB_ALGORITHMS_TO_USE]={ 15, 15 };    // LSH: key size (10-20 usually).
const unsigned int JMAGINE_FEATURES_LSH_MULTI_PROBE_LEVEL[JMAGINE_NB_ALGORITHMS_TO_USE]={ 2, 2 };   // Number of bits to shiftfor neighboring buckets.
const int JMAGINE_FEATURES_FLANN_SEARCH_NB_CHECKS[JMAGINE_NB_ALGORITHMS_TO_USE]={ 32, 32 };  // FLANN: Number of time the trees in the index should be traversed.
const int JMAGINE_FEATURES_KDTREE_NB_TREES[JMAGINE_NB_ALGORITHMS_TO_USE]={ 4, 4 };   // KD-Trees: number of trees.
const int JMAGINE_FEATURES_FLANN_FILTERING_NB_BEST[JMAGINE_NB_ALGORITHMS_TO_USE]={ 3, 3 };  // Number of best trigger matches to keep from general flann matching.
// Filtering - ratio.
const bool JMAGINE_FEATURES_RATIO_FILTERING[JMAGINE_NB_ALGORITHMS_TO_USE]={ true, true };   // Wether or not to perform ratio filtering.
const double JMAGINE_FEATURES_RATIO[JMAGINE_NB_ALGORITHMS_TO_USE]={ 0.8, 0.8 };  // Ratio value.
// Filtering - crosscheck.
const int JMAGINE_FEATURES_CROSSCHECK_NORM[JMAGINE_NB_ALGORITHMS_TO_USE]={ NORM_L1, NORM_HAMMING }; /*NORM_HAMMING;*/   // Type of distance for the BF matcher (crosscheck).
const bool JMAGINE_FEATURES_CROSSCHECK_FILTERING=true;   // Perform crosscheck filtering. MUST be true right now.
const int JMAGINE_FEATURES_MATCHING_KNN_CROSSCHECK=2;   // knn value for the crosscheck matching.
// Filtering - simple threshold.
const bool JMAGINE_FEATURES_STHRESHOLD_FILTERING[JMAGINE_NB_ALGORITHMS_TO_USE]={ false, false };   // Enable simple threshold filtering.
const double JMAGINE_FEATURES_STHRESHOLD[JMAGINE_NB_ALGORITHMS_TO_USE]={ 3., 3. };   // Simple threshold value.
const bool JMAGINE_FEATURES_STHRESHOLD_LOCAL[JMAGINE_NB_ALGORITHMS_TO_USE]={ false, false };   // If true: use local min/max, not the all-candidates one.
// Filtering - adjusted threshold.
const bool JMAGINE_FEATURES_ATHRESHOLD_FILTERING[JMAGINE_NB_ALGORITHMS_TO_USE]={ true, true };   // Enable adjusted threshold filtering.
const double JMAGINE_FEATURES_ATHRESHOLD[JMAGINE_NB_ALGORITHMS_TO_USE]={ 5., 5. };   // Adjusted threshold value. Passes if dist<((max_dist-min_dist)/thresh).
const bool JMAGINE_FEATURES_ATHRESHOLD_LOCAL[JMAGINE_NB_ALGORITHMS_TO_USE]={ true, true };   // If true: use local min/max, not the all-candidates one.
// Filtering - homography (ransac/lmeds).
const bool JMAGINE_FEATURES_HOMOGRAPHY_FILTERING[JMAGINE_NB_ALGORITHMS_TO_USE]={ true, true };   // Enable homography-based filtering.
const unsigned int JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_NBMIN[JMAGINE_NB_ALGORITHMS_TO_USE]={ 6, 6 };   // If less than this, do not apply filtering.
const unsigned int JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_NBMAX[JMAGINE_NB_ALGORITHMS_TO_USE]={ 25, 25 };  // Use the top n matches (or -1 for all - slow !).
const int JMAGINE_FEATURES_HOMOGRAPHY_FILTERING_METHOD[JMAGINE_NB_ALGORITHMS_TO_USE]={ CV_LMEDS, CV_LMEDS };   // Can use CV_RANSAC or CV_LMEDS.
// Topology (needs homography filtering enabled to work).
const bool JMAGINE_FEATURES_HOMOGRAPHY_STORE_TOPOLOGY=true;   // Wether or not to store shape information from homography.
const bool JMAGINE_FEATURES_HOMOGRAPHY_APPROX_TOPOLOGY=true;   // Wether or not to approx shape from homography.
const bool JMAGINE_FEATURES_TOPOLOGY_APPROX_ARCLENGTH=false;  // If true: use arclength*ponderation (below) for epsilon.
const double JMAGINE_FEATURES_TOPOLOGY_APPROX_PONDERATION=0.02;   // Ponderation for the epsilon value (if using arclength).
const double JMAGINE_FEATURES_TOPOLOGY_APPROX_EPSILON=5.;   // Epsilon value for the poly approx of the projected shape.
const bool JMAGINE_FEATURES_TOPOLOGY_COMPUTE_CONVEXITY=false;   // Wether or not to compute convexity for projected shapes.

// MATCHING
const bool JMAGINE_MATCHING_USE_NB_MATCHES=true;   // Enable using the number of filtered matches for score increases.
const double JMAGINE_MATCHING_NB_MATCHES_BONUS=0.1;   // Bonus per match.
const bool JMAGINE_MATCHING_USE_TOPOLOGY=true;   // Enable using the computed shape area for score increases.
const double JMAGINE_MATCHING_TOPOLOGY_MIN_AREA=2000.;    // Min area to obtain score bonus.
const double JMAGINE_MATCHING_TOPOLOGY_BONUS=0.5;    // Topology score bonus.
const bool JMAGINE_MATCHING_USE_TOPOLOGY_RECT=true;   // Enable score increases for regular rectangles.
const double JMAGINE_MATCHING_TOPOLOGY_RECT_MAX_IRREGULARITY=0.2;   // Max irregularity of the rectangle for score increase.
const double JMAGINE_MATCHING_TOPOLOGY_RECT_BONUS=0.25;    // Topology score bonus.
const bool JMAGINE_MATCHING_USE_HOMOGRAPHY=false;   // Wether or not to use homography information for score increases.
const bool JMAGINE_MATCHING_USE_PREVIOUS_MATCHES=true;   // Wether or not to consider previous matches for score increases.
const double JMAGINE_MATCHING_PREVIOUS_MAX_SECONDS=5;   // Max amount of seconds elapsed since last image for bonus.
const double JMAGINE_MATCHING_PREVIOUS_BONUS=0.5;  // Value of the previous matches bonus.
const double JMAGINE_MATCHING_DOUBLETRIG_BONUS=0.08;   // Score increase when double trigger for same element (* this value).
const bool JMAGINE_MATCHING_ENABLE_GOOD_MATCH_FILTERING=true;   // Wether or not to discard a match if score is too low.
const double JMAGINE_MATCHING_GOOD_MATCH_MINSCORE=55.; // Min score to consider a good match (not discard the match).
const bool JMAGINE_MATCHING_F2F_GOOD_MATCH_TOLERANCE=true;   // true: lower good match threshold if we have empty results.
const int JMAGINE_MATCHING_F2F_GOOD_MATCH_MINSCORE=45.;    // min score if we have a series of empty results.
// Temporary stuff (until the homography/topology score increases are rewritten with a flexible rules engine).
const double JMAGINE_MATCHING_HOMOGRAPHY_RT_MINAREA=350.;   // Red triangle min area.
const double JMAGINE_MATCHING_HOMOGRAPHY_RT_BONUS=0.25;
const double JMAGINE_MATCHING_HOMOGRAPHY_HEADLIGHT_MINAREA=1000.;   // Min area of headlights reddish blobs.
const double JMAGINE_MATCHING_HOMOGRAPHY_HEADLIGHT_BONUS=0.15;    // Score increase for headlight.
const double JMAGINE_MATCHING_CIRCLES_WHEEL_MINAREA=6000.;
const double JMAGINE_MATCHING_CIRCLES_WHEEL_BONUS=0.1;
const double JMAGINE_MATCHING_CIRCLES_SIDES_MINAREA=2500.;
const double JMAGINE_MATCHING_CIRCLES_SIDES_MAXAREA=15000.;
const double JMAGINE_MATCHING_CIRCLES_SIDES_BONUS=0.1;
const double JMAGINE_MATCHING_CIRCLES_VENTILATION_MINAREA=1000.;
const double JMAGINE_MATCHING_CIRCLES_VENTILATION_MAXAREA=3700.;
const double JMAGINE_MATCHING_CIRCLES_VENTILATION_BONUS=0.1;
const double JMAGINE_MATCHING_CIRCLES_PRISE_MINAREA=1200.;
const double JMAGINE_MATCHING_CIRCLES_PRISE_MAXAREA=6000.;
const double JMAGINE_MATCHING_CIRCLES_PRISE_BONUS=0.1;
const double JMAGINE_MATCHING_SHAPES_VENTILATION_MINAREA=1500.;
const double JMAGINE_MATCHING_SHAPES_VENTILATION_BONUS=0.2;
const double JMAGINE_MATCHING_SHAPES_FRONT_BONUS=0.1;
const double JMAGINE_MATCHING_SHAPES_TABLEAU_MINAREA=5000.;
const double JMAGINE_MATCHING_SHAPES_TABLEAU_BONUS=0.2;



#endif //__TOKIDEV_JMAGINE_PARAMETERS_HPP
